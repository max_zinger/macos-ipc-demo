package integration;

import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.ipc.IPCException;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.AbstractContextHandler;
import com.proofpoint.ecd.ipc.context.JsonContextHandler;
import com.proofpoint.ecd.ipc.platform.mac.xpc.XPCConnectionConfiguration;
import com.proofpoint.ecd.ipc.platform.mac.xpc.XPCPeerConnection;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.HashMap;

import static org.junit.Assert.assertTrue;
import static com.proofpoint.ecd.ipc.platform.macos.xpc.XPCTestUtils.*;

public class ITXPCListenerDaemon {
    static final String service_name = "com.proofpoint.srv.test";

    @Test
    public void aIntegrationTest() throws IOException, InterruptedException, IPCException.IPCConnectionException {
        System.out.println("integration test is running");
        String testWorkDirectory = Utils.getAppPath(ITXPCListenerDaemon.class);
        String plistOutputPath = Paths.get(testWorkDirectory, "com.proofpoint.srv.test.plist").toFile().getAbsolutePath();
        assertTrue("assert service started", restartService(plistOutputPath, service_name));
        startPeer();
        assertTrue("assert xpc service stopped", stopService(plistOutputPath, service_name));
    }

    private void startPeer() throws IPCException.IPCConnectionException, InterruptedException {
        var conf = new XPCConnectionConfiguration();
        conf.setServiceName(service_name);
        XPCPeerConnection conn = new XPCPeerConnection(conf);
        final var messages = new HashMap<String, Boolean>();
        messages.put("hello", false);
        messages.put("world", false);
        messages.put("goodbyte", false);
        var handler = new AbstractContextHandler() {
            @Override
            public void handleRead(InterProcessCommunicable ipc, byte[] data) {
                String key = new String(data);
                if (messages.containsKey(key)) {
                    messages.put(key, true);
                }
            }
        };
        handler.setIPCOperationsHandler(new JsonContextHandler());
        conn.setIPCOperationsHandler(handler);
        conn.start();

        Thread.sleep(1000);

        for (var key : messages.keySet()) {
            conn.write(key.getBytes(StandardCharsets.UTF_8));
            Thread.sleep(500);
        }
        for (var val : messages.values()) {
            assertTrue(val.booleanValue());
        }
        conn.close();
    }
}
