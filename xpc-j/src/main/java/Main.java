import com.proofpoint.ecd.ipc.IPCException;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.AbstractContextHandler;
import com.proofpoint.ecd.ipc.context.JsonContextHandler;
import com.proofpoint.ecd.ipc.platform.mac.xpc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;


public class Main {
    static final Logger logger = LoggerFactory.getLogger(Main.class.getName());

    public static void main(String[] args) throws InterruptedException, IPCException.IPCConnectionException {

        t4();


        while (true) {
            Thread.sleep(10000);
            logger.info("im alive");
        }
    }

    static void t1() {
        var conn = XPC.INSTANCE.connection_create_mach_service("com.proofpoint.srv.test", XPC.XPC_CONNECTION_MACH_SERVICE_LISTENER);
        XPC.INSTANCE.set_listener_event_handler(conn, (pointer, pointer1, i) -> {
            var msg = XPCMessage.create(pointer1, i);
            logger.info("message received " + msg.getDescription());

        });
        logger.info("starting xpc...");
        XPC.INSTANCE.connection_activate(conn);
        logger.info("started xpc...");
    }

    static void t2() throws IPCException.IPCConnectionException {
        var conn = XPCConnection.createListenerConnection("com.proofpoint.srv.test");
        conn.setHandler(xpcMessage -> {
            logger.info("got message");
            logger.info(xpcMessage.getDescription());
        });
        logger.info("starting xpc...");
        conn.activate();
        logger.info("started xpc...");

    }

    static void t3() throws IPCException.IPCConnectionException {


        var conn = new XPCListenerConnection("com.proofpoint.srv.test");
        logger.info("starting xpc...");
        var handler = new AbstractContextHandler() {
            @Override
            public void handleRead(InterProcessCommunicable ipc, byte[] data) {
                logger.info("got message");
                logger.info(new String(data));
            }
        };
        conn.start(xpcMessage -> {
            handler.handleRead(new XPCPeerConnection(xpcMessage.getPeerConnection()), xpcMessage.getDescription().getBytes(StandardCharsets.UTF_8));
        });
        logger.info("started xpc...");

    }

    static void t4() throws IPCException.IPCConnectionException {


        var conn = new XPCListenerConnection("com.proofpoint.srv.test");
        logger.info("starting xpc...");
        var handler = new AbstractContextHandler() {
            @Override
            public void handleRead(InterProcessCommunicable ipc, byte[] data) {
                logger.info("got message");
                logger.info(new String(data));
                ipc.write(data);

            }
        };
        handler.setIPCOperationsHandler(new JsonContextHandler());
        conn.setIPCOperationsHandler(handler);
        conn.start();
        logger.info("started xpc...");

    }

}
