#include <iostream>
#include <xpc/xpc.h>

// void test_reply()
// {
//   xpc_connection_t conn = xpc_connection_create_mach_service("com.yourname.product.service", NULL, 0);
//   //  xpc_connection_t conn = xpc_connection_create( "com.yourname.product.service", NULL);
//   xpc_connection_set_event_handler(conn, ^(xpc_object_t object) {
//     printf("client received event: %s", xpc_copy_description(object));

//     //-> context.action->analyze->send(conn, analyzeResult)
//     xpc_object_t message = xpc_dictionary_create(NULL, NULL, 0);
//     xpc_dictionary_set_string(message, "message", "{dlpResult}");

//     xpc_connection_send_message_with_reply(conn, message, dispatch_get_main_queue(), ^(xpc_object_t object) {
//       printf("client received event: %s", xpc_copy_description(object));
//     });
//   });
//   xpc_connection_resume(conn);

//   //xpc_connection_send_message(conn, message);
// }
void test_msg()
{
  xpc_connection_t conn = xpc_connection_create_mach_service("com.proofpoint.it.analyze.service", NULL, 0);
  xpc_connection_set_event_handler(conn, ^(xpc_object_t object) {
    printf("client received event: %s", xpc_copy_description(object));
  });
  xpc_connection_activate(conn);

  xpc_object_t message = xpc_dictionary_create(NULL, NULL, 0);
  xpc_dictionary_set_string(message, "msg", "connect");
  xpc_connection_send_message(conn, message);
}

int main()
{
  //test();
  test_msg();
  dispatch_main();
}
