#include <iostream>
#include <string>
#include <thread>
#include <xpc/xpc.h>
#include <xpc/connection.h>

#include "../include/json.hpp"

using namespace nlohmann;

const char *srv = "com.proofpoint.it.analyze.service";
const char *msg_key = "msg";
const char *msg_connect = "connect";

xpc_connection_t *current_client;

void start_xpcj_test_server()
{
  xpc_connection_t conn = xpc_connection_create_mach_service(srv, dispatch_get_main_queue(), XPC_CONNECTION_MACH_SERVICE_LISTENER);
  xpc_connection_set_event_handler(conn, ^(xpc_object_t client) {
    xpc_connection_set_event_handler((xpc_connection_t)client, ^(xpc_object_t message) {
      xpc_type_t type = xpc_get_type(message);
      if (XPC_TYPE_ERROR == type)
      {
        const char *err = xpc_dictionary_get_string(message, XPC_ERROR_KEY_DESCRIPTION);
        std::cout << "an error occured [" << err << "]\n";
        const struct _xpc_dictionary_s *derr = (struct _xpc_dictionary_s *)message;

        if (message == XPC_ERROR_CONNECTION_INVALID)
        {
          printf("XPC_ERROR_CONNECTION_INVALID\n");
        }

        if (message == XPC_ERROR_CONNECTION_INTERRUPTED)
        {
          printf("XPC_ERROR_CONNECTION_INTERRUPTED\n");
        }
        else if (message == XPC_ERROR_TERMINATION_IMMINENT)
        {
          printf("XPC_ERROR_CONNECTION_TERMINATION_IMMINENT\n");
        }
        std::cout << "invalidating current client\n";
        current_client = NULL;
      }
      else if (XPC_TYPE_DICTIONARY == type)
      {
        std::string req = xpc_dictionary_get_string(message, msg_key);
        if (req.empty())
        {
          return;
        }
        //initial connection message
        if (req.compare(msg_connect) == 0)
        {
          current_client = (xpc_connection_t *)&client;
          std::cout << "client connected \n";
          xpc_object_t ores = xpc_dictionary_create(NULL, NULL, 0);
          xpc_dictionary_set_string(ores, msg_key, msg_connect);
          xpc_connection_send_message((xpc_connection_t)client, ores);
          return;
        }
        //assume json message
        try
        {
          json res = json::parse(req);
          std::cout << res.dump(4) << "\n";
        }
        catch (std::exception &e)
        {
          std::cout << "error " << e.what() << "\n";
        }
      }
    });

    xpc_connection_activate((xpc_connection_t)client);
  });
  xpc_connection_activate(conn);
}
void read_stdin()
{
  while (true)
  {
    for (std::string line; std::getline(std::cin, line);)
    {
      try
      {
        if (current_client)
        {
          json req = {
              {"path", line}};
          xpc_object_t msg = xpc_dictionary_create(NULL, NULL, 0);
          xpc_dictionary_set_string(msg, msg_key, req.dump().c_str());
          xpc_connection_send_message(*current_client, msg);
        }
      }
      catch (...)
      {
      }
    }
  }

  std::cout << "read line exit\n";
}
int main()
{
  //async
  std::thread t(read_stdin);
  start_xpcj_test_server();
  dispatch_main();
  return 0;
}

// void test_reply()
// {
//   xpc_connection_t conn = xpc_connection_create_mach_service(srv.c_str(), dispatch_get_main_queue(), XPC_CONNECTION_MACH_SERVICE_LISTENER);
//   xpc_connection_set_event_handler(conn, ^(xpc_object_t client) {
//     //send
//     //xpc_object_t msg = xpc_dictionary_create()
//     //msg.setstring(path)
//     //xpc_connection_send_message(client, msg);
//     xpc_connection_set_event_handler((xpc_connection_t)client, ^(xpc_object_t object) {
//       try
//       {
//         //var path = object.getPath()
//         //var res = analyze(path)
//         //xpc_dictionary_set_string(reply, "reply", res);
//         xpc_object_t reply = xpc_dictionary_create_reply(object);
//         xpc_dictionary_set_string(reply, "reply", "Back from the service");

//         xpc_connection_t remote = xpc_dictionary_get_remote_connection(object);
//         xpc_connection_send_message(remote, reply);
//       }
//       catch (const std::exception &e)
//       {
//         printf("error");
//       }
//       catch (const std::runtime_error &e)
//       {
//         printf("dead");
//       }
//       catch (...)
//       {
//         printf("dead...");
//       }
//       printf("received message: %s", xpc_copy_description(object));
//     });

//     xpc_connection_activate((xpc_connection_t)client);
//   });
//   xpc_connection_activate(conn);
// }