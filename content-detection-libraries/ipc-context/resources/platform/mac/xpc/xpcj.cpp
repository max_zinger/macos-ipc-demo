#include "xpcj.h"
#include <xpc/connection.h>

using std::cout;
using std::endl;
using std::string;

xpc_connection_t connection_create_mach_service(const char *srvname, u_int64_t flags)
{
    xpc_connection_t c = xpc_connection_create_mach_service(srvname, NULL, flags);
    // cout << "native@:"
    //      << "connect - address of conn: " << &c << endl;
    return c;
}
int set_peer_event_handler(xpc_connection_t *conn, void (*msgCallback)(xpc_object_t, int))
{
    // cout << "native@:"
    //      << "set_handler - address of conn: " << conn << endl;
    if (!conn || !msgCallback)
    {
        return -1;
    }

    xpc_connection_set_event_handler((xpc_connection_t)conn, ^(xpc_object_t dmsg) {
      handle_msg(NULL, dmsg, msgCallback, NULL);
    });

    return 0;
}
int set_listener_event_handler(xpc_connection_t *conn, void (*msgCallback)(xpc_connection_t, xpc_object_t, int))
{
    if (!conn || !msgCallback)
    {
        return -1;
    }
    xpc_connection_set_event_handler((xpc_connection_t)conn, ^(xpc_object_t peer) {
      xpc_connection_set_event_handler((xpc_connection_t)peer, ^(xpc_object_t dmsg) {
        handle_msg((xpc_connection_t)peer, dmsg, NULL, msgCallback);
        xpc_connection_activate((xpc_connection_t)peer);
      });
    });
    return 0;
}
xpc_object_t dictionary_create()
{
    return xpc_dictionary_create(NULL, NULL, 0);
}
int dictionary_set_string(xpc_object_t *msg, const char *key, const char *str)
{
    if (!msg || !key || !str)
    {
        return -1;
    }
    xpc_dictionary_set_string(msg, key, str);
    return 0;
}
const char *dictionary_get_string(xpc_object_t *msg, const char *key)
{
    return xpc_dictionary_get_string(msg, key);
}
int connection_send_message(xpc_connection_t *conn, xpc_object_t *msg)
{
    if (!conn || !msg)
    {
        return -1;
    }
    string desc = xpc_copy_description(msg);
    cout << "native@:"
         << "sending message -> " << desc << endl;
    xpc_connection_send_message((xpc_connection_t)conn, msg);
    return 0;
}
int connection_activate(xpc_connection_t *conn)
{
    xpc_connection_activate((xpc_connection_t)conn);
    return 0;
}
int connection_suspend(xpc_connection_t *conn)
{
    if (!conn)
    {
        return -1;
    }
    xpc_connection_suspend((xpc_connection_t)conn);
    return 0;
}
int connection_cancel(xpc_connection_t *conn)
{
    if (!conn)
    {
        return -1;
    }
    xpc_connection_cancel((xpc_connection_t)conn);
    return 0;
}
int connection_resume(xpc_connection_t *conn)
{
    if (!conn)
    {
        return -1;
    }
    xpc_connection_resume((xpc_connection_t)conn);
    return 0;
}
const char *copy_description(xpc_object_t *msg)
{
    return xpc_copy_description(msg);
}
int dictionary_apply(xpc_object_t *msg, void (*callback)(const char *, xpc_object_t msg))
{
    (void)xpc_dictionary_apply(msg, ^(const char *key, xpc_object_t value) {
      callback(key, value);
      return (bool)true;
    });
    return 0;
}
const char *type_get_name(xpc_object_t *value)
{
    xpc_type_t type = xpc_get_type(value);
    return xpc_type_get_name(type);
}

void handle_msg(xpc_connection_t peer, xpc_object_t dmsg, void (*msgPeerCallback)(xpc_object_t, int), void (*msgListenerCallback)(xpc_connection_t, xpc_object_t, int))
{
    string desc = xpc_copy_description(dmsg);
    cout << "native@:" << desc << endl;
    xpc_type_t type = xpc_get_type(dmsg);
    int error = 0;
    if (XPC_TYPE_ERROR == type)
    {
        //handle_error(peer, dmsg, msgCallback, msgPeerCallback);
        error = 1;
    }
    if (peer && msgListenerCallback)
    {
        msgListenerCallback(peer, dmsg, error);
    }
    else if (msgPeerCallback)
    {
        msgPeerCallback(dmsg, error);
    }
    else
    {
        cout << "native@:"
             << "invalid message handler" << endl;
    }
}
// void handle_error(xpc_connection_t peer, xpc_object_t dmsg, void (*msgCallback)(xpc_object_t), void (*msgPeerCallback)(xpc_connection_t, xpc_object_t))
// {
//     const char *err = xpc_dictionary_get_string(dmsg, XPC_ERROR_KEY_DESCRIPTION);
//     cout << "native@:"
//          << "an error occured [" << err << "]" << endl;
//     string errName = "XPC_ERROR_GENERAL";

//     if (dmsg == XPC_ERROR_CONNECTION_INVALID)
//     {
//         errName = GET_NAME(XPC_ERROR_CONNECTION_INVALID);
//     }
//     else if (dmsg == XPC_ERROR_CONNECTION_INTERRUPTED)
//     {
//         errName = GET_NAME(XPC_ERROR_CONNECTION_INTERRUPTED);
//     }
//     else if (dmsg == XPC_ERROR_TERMINATION_IMMINENT)
//     {
//         errName = GET_NAME(XPC_ERROR_TERMINATION_IMMINENT);
//     }
//     cout << "native@:" << errName << endl;

//     if (peer && msgPeerCallback)
//     {
//         msgPeerCallback(peer, dmsg);
//     }
//     else if (msgCallback)
//     {
//         msgCallback(dmsg);
//     }
// }
