package com.proofpoint.ecd.ipc.context;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.context.model.IPCMessageContext;
import com.proofpoint.ecd.ipc.context.model.IPCMessageStatus;
import com.proofpoint.ecd.ipc.platform.MockIPC;
import com.proofpoint.ecd.ipc.platform.win.pipe.MockPipe;
import org.junit.Test;

import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;

import static org.junit.Assert.*;


public class JsonContextHandlerTests {
    static class TestResources{
        final static ObjectMapper JSON_MAPPER = new ObjectMapper();
        static{
            JSON_MAPPER.setTimeZone(Calendar.getInstance().getTimeZone());
        }
    }
    @Test
    public void NamedPipeMessageStatusTest() throws IllegalAccessException, JsonProcessingException {
        var statusDefaults = new IPCMessageStatus.Defaults();
        Field[] fields = statusDefaults.getClass().getDeclaredFields();

        for (var field : fields) {
            var status = (IPCMessageStatus) field.get(statusDefaults);
            assertNotNull(status.getStatus());
            assertLargerThan(-1, status.getCode());
            System.out.println(TestResources.JSON_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(status));

        }
    }

    private void assertLargerThan(int minValue, int value) {
        assertTrue("assert larger than ", minValue < value);
    }

    @Test
    public void readHandlerTest() {
        var readHandler = new JsonContextHandler();
        readHandler.setIPCMode(IPCMode.READ);
        var actionHandler = new MockContextActionHandler();
        readHandler.addAction("test-action-0", actionHandler);
        readHandler.addAction("test-action-1", actionHandler);

        assertFalse(readHandler.canHandleWrite());
        assertTrue(readHandler.canHandleRead());
        assertFalse(readHandler.canHandleStateChange());

        var ctx = new IPCMessageContext();
        ctx.setAction("test-action-1");
        var msg = new IPCMessage<>();
        msg.setContext(ctx);
        msg.setData("test data");
        msg.set_status(IPCMessageStatus.Defaults.OK);
        msg.getContext().setTransactionId("test-id");
        try {
            readHandler.handleRead(new MockPipe(IPCMode.READ), TestResources.JSON_MAPPER.writeValueAsBytes(msg));
            var t1 = TestResources.JSON_MAPPER.valueToTree(msg);
            var t2 = TestResources.JSON_MAPPER.valueToTree(actionHandler.getResult());
            assertEquals("assert message result.", t1, t2);

        } catch (JsonProcessingException e) {
            throw new RuntimeException("failed to serialize message.", e);
        }
    }

    @Test
    public void readWriteHandlerTest() {
        var pipe = MockIPC.getPlatformIPC();
        var handler = new JsonContextHandler();
        handler.setIPCMode(IPCMode.READ_WRITE);
        handler.addAction("test-action-0", new MockContextActionHandler());
        var ctx = new IPCMessageContext();
        ctx.setAction("test-action-0");
        var msg = new IPCMessage<>();
        msg.setContext(ctx);
        msg.setData("test data");
        msg.set_status(IPCMessageStatus.Defaults.OK);
        msg.getContext().setTransactionId("test-id");
        try {
            handler.handleRead(pipe, TestResources.JSON_MAPPER.writeValueAsBytes(msg));
            assertEquals(IPCMessageStatus.Defaults.OK, ((IPCMessage<Object>) pipe.getResult()).get_status());

            var t1 = TestResources.JSON_MAPPER.valueToTree(msg);
            var t2 = TestResources.JSON_MAPPER.valueToTree(pipe.getResult());
            assertEquals("assert message result.", t1, t2);

        } catch (JsonProcessingException e) {
            throw new RuntimeException("failed to serialize message.", e);
        }
    }

    @Test
    public void readHandlerWithPipeWriterTest() {
        var pipeWriter = MockIPC.getPlatformIPC();
        var readHandler = new JsonContextHandler();
        readHandler.setWriter(pipeWriter);
        readHandler.setIPCMode(IPCMode.READ_WRITE);
        readHandler.addAction("test-action-0", new MockContextActionHandler());
        readHandler.addAction("test-action-1", new MockContextActionHandler());

        assertTrue(readHandler.canHandleWrite());
        assertTrue(readHandler.canHandleRead());
        assertFalse(readHandler.canHandleStateChange());

        var ctx = new IPCMessageContext();
        ctx.setAction("test-action-1");
        var msg = new IPCMessage();
        msg.setContext(ctx);
        msg.setData("test data");
        msg.getContext().setTransactionId("test-id");
        msg.set_status(IPCMessageStatus.Defaults.OK);
        try {
            readHandler.handleRead(null, TestResources.JSON_MAPPER.writeValueAsBytes(msg));
            assertEquals(IPCMessageStatus.Defaults.OK, ((IPCMessage) pipeWriter.getResult()).get_status());

            var t1 = TestResources.JSON_MAPPER.valueToTree(msg);
            var t2 = TestResources.JSON_MAPPER.valueToTree(pipeWriter.getResult());
            assertEquals("assert message result.", t1, t2);

        } catch (JsonProcessingException e) {
            throw new RuntimeException("failed to serialize message.", e);
        }
    }

    @Test
    public void readBadDataErrorHandlerTest() {
        var readHandler = new JsonContextHandler();
        readHandler.setIPCMode(IPCMode.READ);
        byte[] data = "bad-data".getBytes(StandardCharsets.UTF_8);
        try {
            var pipe = new MockPipe(IPCMode.READ_WRITE);
            readHandler.handleRead(pipe, data);
            var res = (IPCMessage) pipe.getResult();
            assertNotNull(res.getError());
            assertNull(res.getData());
            assertEquals(IPCMessageStatus.Defaults.BAD_ARGUMENTS, res.get_status());
            assertNotNull("assert incident id exists", res.getContext().getIncidentId());

        } catch (Exception e) {
            throw new RuntimeException("failed to serialize message.", e);
        }
    }

    @Test
    public void readActionErrorHandlerTest() {
        var readHandler = new JsonContextHandler();
        readHandler.setIPCMode(IPCMode.READ);
        var pipe = new MockPipe(IPCMode.READ_WRITE);
        var ctx = new IPCMessageContext();
        ctx.setAction("test-action-1");
        var msg = new IPCMessage();
        msg.setContext(ctx);
        msg.setData("test data");
        try {
            readHandler.handleRead(pipe, TestResources.JSON_MAPPER.writeValueAsBytes(msg));
            var res = (IPCMessage) pipe.getResult();
            assertNotNull(res.getError());
            assertNull(res.getData());
            assertEquals(IPCMessageStatus.Defaults.INVALID_ACTION, res.get_status());
            assertEquals("assert context action.", res.getContext().getAction(), msg.getContext().getAction());
            assertNotNull("assert incident id exists", res.getContext().getIncidentId());


        } catch (JsonProcessingException e) {
            throw new RuntimeException("failed to serialize message.", e);
        }
    }

    @Test
    public void readDefaultHandlerTest() {
        var readHandler = new JsonContextHandler();
        readHandler.setIPCMode(IPCMode.READ);

        var defaultHandler = new MockContextActionHandler();
        readHandler.addDefaultAction(defaultHandler);
        var ctx = new IPCMessageContext();
        ctx.setAction("test-no-action");
        var msg = new IPCMessage();
        msg.setData("test data");
        msg.setContext(ctx);
        msg.set_status(IPCMessageStatus.Defaults.OK);
        try {
            readHandler.handleRead(MockIPC.getPlatformIPC(IPCMode.READ), TestResources.JSON_MAPPER.writeValueAsBytes(msg));
            var t1 = TestResources.JSON_MAPPER.valueToTree(msg);
            var t2 = TestResources.JSON_MAPPER.valueToTree(defaultHandler.getResult());
            assertEquals("assert message result.", t1, t2);
            defaultHandler.resetResult();
            readHandler.addAction("test-action-0", new MockContextActionHandler());
            readHandler.addAction("test-action-1", new MockContextActionHandler());

            readHandler.handleRead(MockIPC.getPlatformIPC(IPCMode.READ), TestResources.JSON_MAPPER.writeValueAsBytes(msg));
            assertEquals(IPCMessageStatus.Defaults.OK, ((IPCMessage) defaultHandler.getResult()).get_status());

            t1 = TestResources.JSON_MAPPER.valueToTree(msg);
            t2 = TestResources.JSON_MAPPER.valueToTree(defaultHandler.getResult());
            assertEquals("assert message result.", t1, t2);


        } catch (JsonProcessingException e) {
            throw new RuntimeException("failed to serialize message.", e);
        }
    }

    static class TestNamedPipeMessage extends IPCMessage<String> {
    }


    static class MockContextActionHandler implements ContextActionHandler<TestNamedPipeMessage> {
        Object result;

        @Override
        public Class<? extends IPCMessage> getRefType() {
            return TestNamedPipeMessage.class;
        }

        @Override
        public void handle(InterProcessCommunicable ipc, TestNamedPipeMessage msg, IPCOperationsHandler ipcOperationsHandler) {
            handle(ipc, msg);
        }

        @Override
        public void handle(InterProcessCommunicable ipc, TestNamedPipeMessage msg) {
            ipc.write(msg);
        }

        @Override
        public void handle(TestNamedPipeMessage msg) {
            result = msg;
        }

        public Object getResult() {
            return result;
        }

        public void resetResult() {
            result = null;
        }
    }
}
