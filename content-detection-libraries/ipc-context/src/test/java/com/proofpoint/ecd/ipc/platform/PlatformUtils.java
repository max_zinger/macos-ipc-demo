package com.proofpoint.ecd.ipc.platform;

public interface PlatformUtils {
    class OS {
        public static final String NAME = System.getProperty("os.name").toLowerCase();
        public static final boolean WINDOWS = NAME.contains("win");
        public static final boolean MACOS = NAME.contains("mac");
    }
}
