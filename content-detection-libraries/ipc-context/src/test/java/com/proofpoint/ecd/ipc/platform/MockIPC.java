package com.proofpoint.ecd.ipc.platform;

import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.platform.win.pipe.MockPipe;

public interface MockIPC extends InterProcessCommunicable {
    Object getResult();
    void resetResult();

    static MockIPC getPlatformIPC() {
        return new MockPipe();
    }

    static MockIPC getPlatformIPC(IPCMode mode) {
        return new MockPipe(mode);

    }
    static MockIPC getPlatformIPC(String name){
        return new MockPipe(name);
    }

}
