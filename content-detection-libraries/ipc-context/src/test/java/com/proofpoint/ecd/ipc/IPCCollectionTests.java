package com.proofpoint.ecd.ipc;

import com.proofpoint.ecd.ipc.context.JsonContextHandler;
import com.proofpoint.ecd.ipc.platform.MockIPC;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class IPCCollectionTests {

    @Test
    public void addRemoveTest() {
        var ipcCollection = new IPCCollection();
        var ipcs = generateIPCItems(10);
        for (var ipc : ipcs) {
            ipcCollection.add(ipc.reader, ipc.writer);
        }
        assertEquals(ipcs.size(), ipcCollection.size());

        for (var ipc : ipcs) {
            ipcCollection.remove(ipc);
        }
        assertTrue(ipcCollection.isEmpty());

    }

    @Test
    public void closeTest() {
        var ipcCollection = new IPCCollection();
        addIPCAssert(ipcCollection, 10);
        for (var ipc : ipcCollection) {
            assertFalse(ipc.reader.isClosed());
            assertFalse(ipc.reader.isClosed());
        }
        ipcCollection.closeAll();
        assertTrue(ipcCollection.isEmpty());

    }

    @Test
    public void closeIdleInProcessTest() {

        var ipcCollection = new IPCCollection();
        int initialSize = 10;
        addIPCAssert(ipcCollection, initialSize);
        var opDateMock = new IPCOpHandlerMock(2, ChronoUnit.MINUTES);
        for (var ipc : ipcCollection) {
            assertFalse(ipc.reader.isClosed());
            assertFalse(ipc.writer.isClosed());

            ipc.reader.setIPCOperationsHandler(opDateMock);
            ipc.writer.setIPCOperationsHandler(opDateMock);

        }
        var handlerMock = new IPCOpHandlerMock(5, ChronoUnit.MINUTES);
        assertEquals("asset in process count", 1, handlerMock.addInProcess());
        ipcCollection.get(3).writer.setIPCOperationsHandler(handlerMock);
        ipcCollection.get(3).reader.setIPCOperationsHandler(handlerMock);
        var time = ipcCollection.get(3).getUpdatedAt();
        ipcCollection.closeAllIdle(1000 * 60 * 3);
        assertTrue("assert in process", handlerMock.isInProcess());
        assertEquals("assert idle ipc scheduler did not close in process context ipc", initialSize, ipcCollection.size());
        assertEquals("assert in process count", 0, handlerMock.removeInProcess());
        assertFalse("assert in process", handlerMock.isInProcess());
        assertTrue(ipcCollection.get(3).getUpdatedAt().isAfter(time));
        ipcCollection.closeAllIdle(1000 * 60 * 3);

        assertEquals("assert idle ipc scheduler did not close extended post process time for ipc ", initialSize, ipcCollection.size());

        handlerMock.resetTime();
        ipcCollection.closeAllIdle(1000 * 60 * 3);
        assertEquals("assert idle ipc scheduler closed one ipc.", initialSize - 1, ipcCollection.size());
        ipcCollection.closeAllIdle(0);
        assertTrue("assert idle ipc scheduler closed all ipcs", ipcCollection.isEmpty());

    }

    @Test
    public void closeIdleTest() {
        var ipcCollection = new IPCCollection();
        int initialSize = 10;
        addIPCAssert(ipcCollection, initialSize);
        var opDateMock = new IPCOpHandlerMock(2, ChronoUnit.MINUTES);
        for (var ipc : ipcCollection) {
            assertFalse(ipc.reader.isClosed());
            assertFalse(ipc.writer.isClosed());

            ipc.reader.setIPCOperationsHandler(opDateMock);
            ipc.writer.setIPCOperationsHandler(opDateMock);

        }
        ipcCollection.get(3).writer.setIPCOperationsHandler(new IPCOpHandlerMock(5, ChronoUnit.MINUTES));
        ipcCollection.get(3).reader.setIPCOperationsHandler(new IPCOpHandlerMock(5, ChronoUnit.MINUTES));
        ipcCollection.closeAllIdle(1000 * 60 * 3);

        assertEquals("assert idle ipc scheduler closed one ipc.", initialSize - 1, ipcCollection.size());

        ipcCollection.get(3).writer.setIPCOperationsHandler(new IPCOpHandlerMock(5, ChronoUnit.MINUTES));
        ipcCollection.get(3).reader.setIPCOperationsHandler(new IPCOpHandlerMock(5, ChronoUnit.MINUTES));

        ipcCollection.closeAllIdle(1000 * 60 * 3);

        assertEquals("assert idle ipc scheduler closed one ipc.", initialSize - 2, ipcCollection.size());

        ipcCollection.closeAllIdle(0);
        assertTrue("assert idle ipc scheduler closed all ipcs.", ipcCollection.isEmpty());

    }

    public static void addIPCAssert(IPCCollection ipcCollection, int numberOfIpcs) {
        var ipcCollectionItems = generateIPCItems(numberOfIpcs);
        for (var ipc : ipcCollectionItems) {
            ipcCollection.add(ipc);
        }
        assertEquals("assert number of ipcs.", ipcCollectionItems.size(), ipcCollection.size());
    }

    public static List<IPCCollection.IPCCollectionItem> generateIPCItems(int count) {
        var res = new ArrayList<IPCCollection.IPCCollectionItem>();
        var ipcCollection = new IPCCollection();
        for (int i = 0; i < count; i++) {
            var read = MockIPC.getPlatformIPC("read-" + i);
            var write = MockIPC.getPlatformIPC("write-" + i);
            res.add(ipcCollection.new IPCCollectionItem(read, write));
        }
        return res;
    }

    static class IPCOpHandlerMock extends JsonContextHandler {
        LocalDateTime updatedAt;
        LocalDateTime createdAt;

        IPCOpHandlerMock(long amountToSubtract, TemporalUnit unit) {
            updatedAt = LocalDateTime.now().minus(amountToSubtract, unit);
            createdAt = updatedAt;
        }

        @Override
        public LocalDateTime getUpdatedAt() {
            return updatedAt;
        }

        @Override
        public void updateTime() {
            updatedAt = LocalDateTime.now();
        }

        public void resetTime() {
            updatedAt = createdAt;
        }
    }


}
