package com.proofpoint.ecd.ipc.platform.win.pipe;

import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.platform.MockIPC;

public class MockPipe extends Kernel32NamedPipeServer implements MockIPC {
    private Object result;
    private boolean closed = false;
    public MockPipe() {
        super("MockPipe");
    }

    public MockPipe(String name){
        super(name);
    }
    public MockPipe(IPCMode mode) {
        super("MockPipe", mode);
    }

    @Override
    public <T> int write(T data) {
        result = data;
        return 0;
    }

    public Object getResult() {
        return result;
    }

    public void resetResult() {
        result = null;
    }
    @Override
    public void close() {

        closed = true;
    }
    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof MockPipe)) {
            return false;
        }
        return name == ((MockPipe) o).name;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
    @Override
    public boolean isClosed(){
        return closed;
    }
}