package com.proofpoint.ecd.ipc.platform.win.pipe;

import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.platform.PlatformUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class Kernel32NamedPipeServerClientTests {

    final String test_message_01_long = "[32] Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem " +
            "aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia " +
            "consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui do lorem ipsum, quia dolor sit amet consectetur adipisci[ng] velit, sed quia non " +
            "numquam [do] eius modi tempora inci[di]dunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum[d] exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid " +
            "ex ea commodi consequatur? [D]Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? [33] " +
            "At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, " +
            "similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi " +
            "optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet," +
            " ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\n" +
            "This is H. Rackham's translation as printed in the Loeb Classical Library edition with underlining added for the translation of the text found in the example of the Lorem ipsum:[2]\n" +
            "[32] But I must explain to you how all this mistaken idea of reprobating pleasure and extolling pain arose. To do so, I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, " +
            "the master-builder of human happiness. No one rejects, dislikes or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is " +
            "there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, " +
            "except to obtain some advantage from it? " +
            "But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, " +
            "or one who avoids a pain that produces no resultant pleasure? [33] On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; " +
            "and equal blame belongs to those who fail in their duty through weakness of will, " +
            "which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammeled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. " +
            "But in certain circumstances and owing to the claims of duty or the obligations of " +
            "business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.";
    final String test_message_02_short = "Short Test Message 2";
    final String test_message_03_short = "Short Test Message 3";
    final int buffer_size = 1024;

    @Before
    public void before() {

        Assume.assumeTrue(PlatformUtils.OS.WINDOWS);
    }

    @Test
    public void testServerWriteClientRead() {
        String testId = "-xyz";
        String pipeName = "win-pipe-test" + testId;


        var clientExec = Executors.newSingleThreadExecutor();
        var serverExec = Executors.newSingleThreadExecutor();

        final Kernel32NamedPipeClient client = new Kernel32NamedPipeClient(pipeName, IPCMode.READ);
        client.setBufferSize(buffer_size);
        final Kernel32NamedPipeServer server = new Kernel32NamedPipeServer(pipeName, IPCMode.WRITE);
        server.setBufferSize(buffer_size);

        clientExec.submit(client);
        serverExec.submit(() -> {
            try {
                server.start();
                server.write(test_message_01_long.getBytes());
                server.write(test_message_02_short.getBytes());
                server.write(test_message_01_long.getBytes());
                server.write(test_message_03_short.getBytes());

            } catch (Exception e) {
                e.printStackTrace();
            }


        });
        try {

            Utils.awaitTermination(clientExec, 5, TimeUnit.SECONDS);
            assertEquals("assert message over pipe.", test_message_01_long, new String(client.readMessage()));
            assertEquals("assert message over pipe.", test_message_02_short, new String(client.readMessage()));
            assertEquals("assert message over pipe.", test_message_01_long, new String(client.readMessage()));
            assertEquals("assert message over pipe.", test_message_03_short, new String(client.readMessage()));
            Utils.awaitTermination(serverExec, 60, TimeUnit.SECONDS);
        } finally {
            boolean closeError = false;
            if (server != null) {
                try {
                    server.close();
                } catch (Exception e) {
                    closeError = true;
                    e.printStackTrace();
                }
            }
            if (client != null) {
                try {
                    client.close();
                } catch (Exception e) {
                    closeError = true;
                    e.printStackTrace();
                }
            }
            if (closeError) {
                throw new RuntimeException("failed to close pipe");
            }
        }
    }

    @Test
    public void testServerReadClientWrite() {
        String testId = "-xyz";
        String pipeName = "win-pipe-test" + testId;

        var clientExec = Executors.newSingleThreadExecutor();
        var serverExec = Executors.newSingleThreadExecutor();

        final Kernel32NamedPipeClient client = new Kernel32NamedPipeClient(pipeName, IPCMode.WRITE);
        client.setBufferSize(buffer_size);
        final Kernel32NamedPipeServer server = new Kernel32NamedPipeServer(pipeName, IPCMode.READ);
        server.setBufferSize(buffer_size);
        serverExec.submit(server);

        clientExec.submit(() -> {
            try {
                client.start();
                Utils.awaitTermination(serverExec, 10, TimeUnit.SECONDS);
                client.write(test_message_01_long.getBytes());
                client.write(test_message_02_short.getBytes());
                client.write(test_message_01_long.getBytes());
                client.write(test_message_03_short.getBytes());

            } catch (Exception e) {
                e.printStackTrace();
            }


        });
        try {
            Thread.sleep(5000);
            Utils.awaitTermination(serverExec, 60, TimeUnit.SECONDS);
            assertEquals("assert message over pipe.", test_message_01_long, new String(server.readMessage()));
            assertEquals("assert message over pipe.", test_message_02_short, new String(server.readMessage()));
            assertEquals("assert message over pipe.", test_message_01_long, new String(server.readMessage()));
            assertEquals("assert message over pipe.", test_message_03_short, new String(server.readMessage()));
            Utils.awaitTermination(clientExec, 60, TimeUnit.SECONDS);

        } catch (Exception e) {
            var x = e;
        } finally {
            boolean closeError = false;
            if (server != null) {
                try {
                    server.close();
                } catch (Exception e) {
                    closeError = true;
                    e.printStackTrace();
                }
            }
            if (client != null) {
                try {
                    client.close();
                } catch (Exception e) {
                    closeError = true;
                    e.printStackTrace();
                }
            }
            if (closeError) {
                throw new RuntimeException("failed to close pipe");
            }

        }
    }
}
