package com.proofpoint.ecd.ipc;

public enum IPCMode {
    READ,
    WRITE,
    READ_WRITE
}
