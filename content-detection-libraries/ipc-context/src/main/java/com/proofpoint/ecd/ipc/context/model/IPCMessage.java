package com.proofpoint.ecd.ipc.context.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class IPCMessage<T> {

    IPCMessageContext context = new IPCMessageContext();
    IPCMessageStatus _status = IPCMessageStatus.Defaults.OK;
    T data;
    String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public IPCMessageContext getContext() {
        return context;
    }

    public void setContext(IPCMessageContext context) {
        this.context = context;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public IPCMessageStatus get_status() {
        return _status;
    }

    public void set_status(IPCMessageStatus _status) {
        this._status = _status;
    }

}
