package com.proofpoint.ecd.ipc.context;

/**
 * Key action pair
 * Maps a specific key to a handler
 */
public class ContextAction {
    private String name;
    private ContextActionHandler handler;
    public ContextAction(String name, ContextActionHandler handler){
        this.name = name;
        this.handler = handler;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ContextActionHandler getHandler() {
        return handler;
    }

    public void setHandler(ContextActionHandler handler) {
        this.handler = handler;
    }


}
