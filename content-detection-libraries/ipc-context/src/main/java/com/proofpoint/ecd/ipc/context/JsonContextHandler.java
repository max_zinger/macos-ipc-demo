package com.proofpoint.ecd.ipc.context;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.IPCState;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.context.model.IPCMessageStatus;
import com.proofpoint.ecd.ipc.context.model.MessageStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * base json context based pipe operations handling
 */
public class JsonContextHandler implements IPCOperationsHandler {
    private final static Logger logger = LoggerFactory.getLogger(JsonContextHandler.class);

    private final static ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    static {
        mapper.setTimeZone(Calendar.getInstance().getTimeZone());
    }

    private final static ContextActionHandler default_error_handler = new ContextActionHandler() {
        @Override
        public Class<? extends IPCMessage> getRefType() {
            return IPCMessage.class;
        }

        @Override
        public void handle(InterProcessCommunicable writer, IPCMessage msg, IPCOperationsHandler namedPipeOpHandler) {
            handle(writer, msg);
        }

        @Override
        public void handle(InterProcessCommunicable writer, IPCMessage msg) {
            msg.getContext().setIncidentId(UUID.randomUUID().toString());
            if (writer == null || writer.isClosed()) {
                handle(msg);
                return;
            }
            writer.write(msg);
        }

        @Override
        public void handle(IPCMessage msg) {

        }
    };
    private final Map<String, ContextAction> actionMap = new HashMap<>();
    private InterProcessCommunicable writer;
    private ContextActionHandler errorHandler = default_error_handler;
    private ContextActionHandler defaultHandler;
    private IPCMode ipcMode = IPCMode.READ_WRITE;
    private LocalDateTime updatedAt = LocalDateTime.now();
    private AtomicInteger inProcessCount = new AtomicInteger(0);
    private Map<String, MessageStatus> messageStatusMap = new ConcurrentHashMap<>();

    public MessageStatus addMessageStatus(IPCMessage message) {
        var messageStatus = new MessageStatus(message);
        messageStatusMap.put(messageStatus.getTransactionId(), messageStatus);
        return messageStatus;
    }

    public MessageStatus updateMessageStatusEvent(String id, IPCMessageStatus status) {
        var msgStat = getMessageStatus(id);
        if (msgStat != null) {
            msgStat.addEvent(status);
        }
        return msgStat;
    }

    @Override
    public Map<String, MessageStatus> getMessageStatusMap() {
        return messageStatusMap;
    }

    @Override
    public MessageStatus getMessageStatus(String id) {
        if (messageStatusMap.containsKey(id)) {
            return messageStatusMap.get(id);
        }
        return null;
    }

    @Override
    public boolean isInProcess() {
        return inProcessCount.get() > 0;
    }

    @Override
    public int getInProcess() {
        return inProcessCount.get();
    }

    @Override
    public int addInProcess() {
        return inProcessCount.incrementAndGet();
    }

    @Override
    public int removeInProcess() {
        return inProcessCount.decrementAndGet();
    }

    @Override
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public void updateTime() {
        updatedAt = LocalDateTime.now();
    }

    @Override
    public InterProcessCommunicable getWriter() {
        return writer;
    }

    @Override
    public IPCOperationsHandler setIPCMode(IPCMode ipcMode) {
        this.ipcMode = ipcMode;
        return this;
    }

    @Override
    public void setWriter(InterProcessCommunicable writer) {
        this.writer = writer;
    }


    @Override
    public IPCOperationsHandler setErrorHandler(ContextActionHandler errorHandler) {
        this.errorHandler = errorHandler;
        return this;
    }

    public void addDefaultAction(ContextActionHandler handler) {
        defaultHandler = handler;
    }

    public void addAction(ContextAction action) {
        actionMap.put(action.getName(), action);
    }

    public void addAction(String name, ContextActionHandler handler) {
        addAction(new ContextAction(name, handler));
    }

    /**
     * ipc read event handling
     *
     * @param ipc
     * @param data
     */
    @Override
    public void handleRead(InterProcessCommunicable ipc, byte[] data) {
        String obj;
        if (data != null && data.length > 0 && (obj = new String(data)) != null && obj.length() > 0) {
            IPCMessage msg = null;
            try {
                msg = mapper.readValue(data, new TypeReference<IPCMessage>() {
                });
                if (Utils.isStringNullOrWhitespaces(msg.getContext().getTransactionId())) {
                    msg.getContext().setTransactionId(UUID.randomUUID().toString());
                    logger.warn("transaction id is not set, generated random id [{}]", msg.getContext().getTransactionId());
                }
                addMessageStatus(msg);
                logger.info("message received - id [{}] action [{}].", msg.getContext().getTransactionId(), msg.getContext().getAction());

                msg.set_status(IPCMessageStatus.Defaults.OK);
                updateTime();
                if (!actionMap.containsKey(msg.getContext().getAction().toLowerCase(Locale.ROOT)) && defaultHandler == null) {
                    throw new IPCMessageStatus.InvalidActionException(msg.getContext().getAction());

                } else if (defaultHandler != null) {
                    handleMessage(defaultHandler, getMessage(data, defaultHandler.getRefType()), ipc);
                    return;
                }
                var handler = actionMap.get(msg.getContext().getAction()).getHandler();
                var msgRef = getMessage(data, handler.getRefType());
                msgRef.getContext().setTransactionId(msg.getContext().getTransactionId());
                handleMessage(handler, msgRef, ipc);


            } catch (Exception e) {
                handleError(ipc, msg, e, obj);
            }
            return;
        }
        logger.info("{} read empty data.", ipc.nameToString());
    }

    private void handleError(InterProcessCommunicable ipc, IPCMessage msg, Exception e, String obj) {
        if (msg == null) {
            msg = new IPCMessage();
            msg.getContext().setTransactionId(UUID.randomUUID().toString());
        }
        msg.set_status(IPCMessageStatus.fromException(e));
        msg.setError(e.getMessage());
        msg.setData(null);
        updateMessageStatusEvent(msg.getContext().getTransactionId(), msg.get_status());
        logger.error(String.format("message error [%s]", obj), e);
        handleMessage(errorHandler, msg, ipc);
        updateMessageStatusEvent(msg.getContext().getTransactionId(), IPCMessageStatus.Defaults.MSG_COMPLETED);
    }

    /**
     * ipc write event handling
     *
     * @param data
     */
    @Override
    public <T> byte[] handleWrite(T data) {
        try {
            var bytes = mapper.writeValueAsBytes(data);
            return bytes;
        } catch (JsonProcessingException e) {
            logger.error("error writing json data", e);
        }
        return null;
    }

    @Override
    public boolean canHandleRead() {
        return ipcMode != IPCMode.WRITE;
    }

    @Override
    public boolean canHandleWrite() {
        return ipcMode != IPCMode.READ;
    }

    @Override
    public boolean canHandleStateChange() {
        return false;
    }

    @Override
    public void handleStateChanged(IPCState oldState, IPCState newState) {

    }

    private void handleMessage(ContextActionHandler handler, IPCMessage msg, InterProcessCommunicable ipc) {
        if (Thread.currentThread().getContextClassLoader() == null) {
            Thread.currentThread().setContextClassLoader(ClassLoader.getSystemClassLoader());
        }
        logger.debug("reading context [{}].", msg.getContext());
        if (writer != null) {
            handler.handle(writer, msg, this);

        } else if (ipc.canWrite()) {
            handler.handle(ipc, msg, this);

        } else {
            handler.handle(msg);
        }
        updateTime();

    }

    public static <T> IPCMessage getMessage(byte[] data, Class<T> ref) throws IOException {
        var msg = (IPCMessage) mapper.readValue(data, ref);
        msg.set_status(IPCMessageStatus.Defaults.OK);
        return msg;
    }
}
