package com.proofpoint.ecd.ipc.context;

import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;

public interface ContextActionHandler<T extends IPCMessage> {
    Class<? extends IPCMessage> getRefType();

    void handle(InterProcessCommunicable writer, T msg, IPCOperationsHandler namedPipeOpHandler);

    void handle(InterProcessCommunicable writer, T msg);

    void handle(T msg);
}
