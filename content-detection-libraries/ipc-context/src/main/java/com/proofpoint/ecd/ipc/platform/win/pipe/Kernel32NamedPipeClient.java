package com.proofpoint.ecd.ipc.platform.win.pipe;

import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.IPCState;
import com.sun.jna.platform.win32.*;
import com.sun.jna.ptr.IntByReference;

import static com.proofpoint.ecd.ipc.platform.win.pipe.Kernel32Flags.*;
/**
 *  jna win32 client pipe impl
 */
public class Kernel32NamedPipeClient extends Kernel32NamedPipe {
    private static int connect_retry_interval_ms = 500;//0.5 sec


    public Kernel32NamedPipeClient(String name) {
        this(name, IPCMode.READ_WRITE);
    }

    public Kernel32NamedPipeClient(String name, IPCMode mode) {
        init(name, mode);
    }

    @Override
    public synchronized void start() throws Exception {
        try {
            isClosed = false;
            pipeStateChanged(ipcState, IPCState.STARTED);
            logger.info("{} waiting for connection.", nameToString());
            pipe_handle = tryConnectToNamedPipe(name, mode);
            logger.info("{} connecting to server.", nameToString());

            pipeStateChanged(ipcState, IPCState.CREATED);


            WinDef.ULONGByReference serverProcessIdRf = new WinDef.ULONGByReference();
            Kernel32.INSTANCE.GetNamedPipeServerProcessId(pipe_handle, serverProcessIdRf);
            serverProcessId = serverProcessIdRf.getValue().longValue();

            WinDef.ULONGByReference serverSessionIdRef = new WinDef.ULONGByReference();
            Kernel32.INSTANCE.GetNamedPipeServerSessionId(pipe_handle, serverSessionIdRef);
            serverSessionId = serverSessionIdRef.getValue().longValue();

            pipeStateChanged(ipcState, IPCState.CONNECTED);
            logger.info("{} connected to server.", nameToString());
            if (canRead() && handler != null && handler.canHandleRead()) {
                try {
                    startRead();

                } finally {
                    close();
                }
            }
        } catch (Exception e) {
            logger.error("client pipe [" + name + "] error ", e);
            close();
        }

    }

    private WinNT.HANDLE tryConnectToNamedPipe(String pipeName, final IPCMode mode) {
        int pipeFileMode;
        switch (mode) {
            case READ:
                pipeFileMode = WinNT.GENERIC_READ;
                break;
            case WRITE:
                pipeFileMode = WinNT.GENERIC_WRITE;
                break;
            default:
                pipeFileMode = WinNT.GENERIC_READ | WinNT.GENERIC_WRITE;
                break;
        }
        WinNT.HANDLE pipe = null;
        int currentTotalConnectMs = 0;
        while (currentTotalConnectMs < connectTimeoutMs && !isClosed) {
            try {
                pipe = connectToNamedPipe(pipeName, pipeFileMode);
                if (pipe != null) {
                    break;
                }
                currentTotalConnectMs += connect_retry_interval_ms;
                Thread.sleep(connect_retry_interval_ms);
            } catch (InterruptedException e) {
                throw new RuntimeException(String.format("failed waiting for pipe [%s] connection.", pipeName), e);
            }
        }
        if (pipe == null) {
            throw new RuntimeException(isClosed ? String.format("failed to connect pipe [%s]. pipe was closed.", pipeName) : String.format("failed to connect pipe [%s]. timeout exceeded [%d].", pipeName, connectTimeoutMs));
        }
        return pipe;

    }

    /**
     * connect to a server pipe
     * @param pipeName
     * @param mode
     * @return
     */
    private static WinNT.HANDLE connectToNamedPipe(String pipeName, int mode) {

        WinNT.HANDLE pipe = Kernel32.INSTANCE.CreateFile(pipeName, mode, 0, null, WinNT.OPEN_EXISTING, WinNT.FILE_FLAG_OVERLAPPED | SECURITY_SQOS_PRESENT | SECURITY_IMPERSONATION, null);
        if (pipe == WinBase.INVALID_HANDLE_VALUE) {
            int lastError = Kernel32.INSTANCE.GetLastError();
            if (lastError == WinError.ERROR_FILE_NOT_FOUND) {
                logger.debug("ERROR_FILE_NOT_FOUND : [{}] pipe not found.", pipeName);
                return null;
            }
            String emsg = String.format("pipe client [%s] creation failed, error ref [%d].", pipeName, lastError);
            logger.error(emsg);
            throw new RuntimeException(emsg);
        }
        logger.debug("[{}] pipe connected.", pipeName);
        IntByReference lpMode = new IntByReference(WinBase.PIPE_READMODE_MESSAGE);
        Kernel32.INSTANCE.SetNamedPipeHandleState(pipe, lpMode, null, null);
        return pipe;

    }

    @Override
    public String nameToString() {
        return String.format("pipe client [%s]", name);
    }



}
