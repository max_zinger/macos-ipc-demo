package com.proofpoint.ecd.ipc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

/**
 * holds the current open ipc instances
 */
public class IPCCollection extends ArrayList<IPCCollection.IPCCollectionItem> {
    private final static Logger logger = LoggerFactory.getLogger(IPCCollection.class);
    private final ReentrantLock lock = new ReentrantLock();

    public boolean remove(IPCCollectionItem ipcItem) {
        lock.lock();
        boolean res;
        try {
            res = super.remove(ipcItem);
        } finally {
            lock.unlock();
        }
        return res;
    }

    public boolean remove(InterProcessCommunicable writer) {
        return this.remove(new IPCCollectionItem(writer));
    }

    @Override
    public boolean add(IPCCollectionItem ipcItem) {
        lock.lock();
        boolean res = false;
        try {
            if (!this.contains(ipcItem)) {
                res = super.add(ipcItem);
            }
        } finally {
            lock.unlock();
        }
        return res;

    }

    public boolean add(InterProcessCommunicable reader, InterProcessCommunicable writer) {
        return this.add(new IPCCollectionItem(reader, writer));
    }

    public boolean contains(String name) {
        return this.stream().filter(p -> p.writer.getName() == name || p.reader.getName() == name).findFirst().isPresent();
    }

    public void close(InterProcessCommunicable writer) {
        close(new IPCCollectionItem(writer));
    }

    public void close(IPCCollectionItem ipcCollectionItem) {
        try {
            int idx = indexOf(ipcCollectionItem);
            if (idx >= 0) {
                ipcCollectionItem = get(idx);
                ipcCollectionItem.close();
                remove(ipcCollectionItem);
            }
        } catch (Exception e) {
            logger.error("failed to close " + ipcCollectionItem, e);
        }
    }

    public void closeAll() {
        for (var ipc : List.copyOf(this)) {
            close(ipc);
        }
    }

    public void closeAllIdle(long idleTimeMs) {

        for (var ipc : List.copyOf(this)) {
            if (ipc.isInProcess()) {
                ipc.updateTime();
                continue;
            }
            var targetTime = LocalDateTime.now().minus(idleTimeMs, ChronoUnit.MILLIS);
            if (ipc.getUpdatedAt().isBefore(targetTime)) {
                close(ipc);
                logger.info("{} closed after [{}] ms of inactivity.", ipc, idleTimeMs);
            }
        }

    }

    class IPCCollectionItem {
        InterProcessCommunicable reader;
        InterProcessCommunicable writer;

        IPCCollectionItem(InterProcessCommunicable reader, InterProcessCommunicable writer) {
            this.reader = reader;
            this.writer = writer;
        }

        IPCCollectionItem(InterProcessCommunicable writer) {
            this.writer = writer;
        }

        boolean isInProcess() {
            return writer.getIPCOperationsHandler().isInProcess() || reader.getIPCOperationsHandler().isInProcess();
        }

        void updateTime() {
            writer.getIPCOperationsHandler().updateTime();
        }

        LocalDateTime getUpdatedAt() {
            var date1 = reader.getIPCOperationsHandler().getUpdatedAt();
            var date2 = writer.getIPCOperationsHandler().getUpdatedAt();
            return date1.isAfter(date2) ? date1 : date2;

        }

        @Override
        public String toString() {
            return reader.nameToString() + " / " + writer.nameToString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            IPCCollectionItem rw = (IPCCollectionItem) o;

            return Objects.equals(writer, rw.writer);
        }

        @Override
        public int hashCode() {
            return Objects.hash(writer);
        }

        void close() throws Exception {
            if (!writer.isClosePending()) {
                writer.close();
            }
            if (!reader.isClosePending()) {
                reader.close();
            }
        }
    }
}
