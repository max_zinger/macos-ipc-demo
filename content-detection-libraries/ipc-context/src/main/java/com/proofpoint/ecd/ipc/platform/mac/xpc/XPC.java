package com.proofpoint.ecd.ipc.platform.mac.xpc;

import com.sun.jna.Callback;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

/**
 * jna java interface for xpc service api
 */
interface XPC extends Library {
    String XPCJ_DYLIB_NAME = "xpcj";
    XPC INSTANCE = Native.load(XPCJ_DYLIB_NAME, XPC.class);

    long XPC_CONNECTION_MACH_SERVICE_NONE = 0;//xpc connection as client peer;
    long XPC_CONNECTION_MACH_SERVICE_LISTENER = 1 << 0;//xpc connection as server;
    long XPC_CONNECTION_MACH_SERVICE_PRIVILEGED = 1 << 1;//?

    interface PeerMessageCallback extends Callback {
        void invoke(Pointer msg, int error);
    }

    interface ListenerMessageCallback extends Callback {
        void invoke(Pointer peer, Pointer msg, int error);
    }

    interface DictionaryApplyCallback extends Callback {
        void invoke(String key, Pointer value);
    }

    /*xpc_connection_t*/Pointer connection_create_mach_service(String srv, long flags);

    int set_peer_event_handler(Pointer xpcConn, PeerMessageCallback msgCallback);

    int set_listener_event_handler(Pointer xpcConn, ListenerMessageCallback msgCallback);

    int connection_send_message(Pointer xpcConn, Pointer xpcMsg);

    /*xpc_object_t*/Pointer dictionary_create();

    int dictionary_set_string(Pointer xpcConn, String key, String msg);

    String dictionary_get_string(Pointer xpcMsg, String key);

    int connection_activate(Pointer xpcConn);

    int connection_suspend(Pointer xpcConn);

    int connection_cancel(Pointer xpcConn);

    int connection_resume(Pointer xpcConn);

    String copy_description(Pointer xpcMsg);

    int dictionary_apply(Pointer pMsg, DictionaryApplyCallback callback);

    String type_get_name(Pointer pValue);
}