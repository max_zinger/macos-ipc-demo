package com.proofpoint.ecd.ipc.platform.mac.xpc;

import com.sun.jna.Pointer;

import java.util.HashMap;

public class XPCMessage {
    private static final XPC xpc = XPC.INSTANCE;
    private Pointer msgPntr;
    private XPCConnection peerConnection;
    private boolean isError;

    private XPCMessage() {
    }

    protected static XPCMessage create(XPCConnection peerConnection, Pointer msgPntr, int error) {
        var xpcMsg = create(msgPntr, error);
        xpcMsg.peerConnection = peerConnection;

        return xpcMsg;
    }

    protected static XPCMessage create(Pointer msgPntr, int error) {
        var xpcMsg = new XPCMessage();
        xpcMsg.msgPntr = msgPntr;
        xpcMsg.isError = error > 0;
        return xpcMsg;
    }

    public static XPCMessage create() {
        var xpcMsg = new XPCMessage();
        xpcMsg.msgPntr = xpc.dictionary_create();
        return xpcMsg;
    }

    public static XPCMessage create(String key, String value) {
        var xpcMsg = create();
        xpcMsg.setValue(key, value);
        return xpcMsg;
    }

    public boolean isError() {
        return isError;
    }

    public XPCConnection getPeerConnection() {
        return peerConnection;
    }

    public String getValue(String key) {
        return xpc.dictionary_get_string(msgPntr, key);
    }

    public void setValue(String key, String value) {
        xpc.dictionary_set_string(msgPntr, key, value);
    }

    protected Pointer getPointer() {
        return msgPntr;
    }

    public String getDescription() {
        return xpc.copy_description(msgPntr);
    }

    public HashMap<String, String> getKeyValueTypeNames() {
        final HashMap<String, String> dict = new HashMap<>();
        xpc.dictionary_apply(msgPntr, (key, value) -> {
            dict.put(key, xpc.type_get_name(value));
        });
        return dict;
    }
}
