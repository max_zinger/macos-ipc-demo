package com.proofpoint.ecd.ipc;

import com.proofpoint.ecd.ipc.context.ContextAction;
import com.proofpoint.ecd.ipc.context.ContextActionHandler;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.context.model.MessageStatus;

import java.time.LocalDateTime;
import java.util.Map;

public interface IPCOperationsHandler {
    LocalDateTime getUpdatedAt();

    void updateTime();

    boolean isInProcess();

    int getInProcess();

    int addInProcess();

    int removeInProcess();

    boolean canHandleRead();

    boolean canHandleWrite();

    boolean canHandleStateChange();

    InterProcessCommunicable getWriter();

    IPCOperationsHandler setIPCMode(IPCMode ipcMode);

    void setWriter(InterProcessCommunicable writer);

    void handleRead(InterProcessCommunicable ipc, byte[] data);

    <T> byte[] handleWrite(T data);

    void handleStateChanged(IPCState oldState, IPCState newState);

    IPCOperationsHandler setErrorHandler(ContextActionHandler errorHandler);

    void addDefaultAction(ContextActionHandler handler);

    void addAction(ContextAction action);

    void addAction(String name, ContextActionHandler handler);

    MessageStatus getMessageStatus(String id);

    Map<String, MessageStatus> getMessageStatusMap();

    static MessageStatus getMessageStatus(IPCOperationsHandler handler, IPCMessage msg) {
        if (handler != null) {
            var msgStat = handler.getMessageStatus(msg.getContext().getTransactionId());
            if (msgStat != null) {
                return msgStat;
            }
        }
        return new MessageStatus(msg);
    }

    static void addInProcess(InterProcessCommunicable ipc) {
        if (ipc != null && ipc.getIPCOperationsHandler() != null) {
            ipc.getIPCOperationsHandler().addInProcess();
        }
    }

    static void removeInProcess(InterProcessCommunicable ipc) {
        if (ipc != null && ipc.getIPCOperationsHandler() != null) {
            ipc.getIPCOperationsHandler().removeInProcess();
        }
    }
    static IPCOperationsHandler getIPCOperationsHandler(InterProcessCommunicable ipc, IPCOperationsHandler ipcOperationsHandler){
        return ipcOperationsHandler != null || ipc == null ? ipcOperationsHandler : ipc.getIPCOperationsHandler();
    }


}
