package com.proofpoint.ecd.ipc;

public enum IPCState {

    NONE(0),
    CREATED(2),
    CONNECTED(3),
    RESTART(4),
    CLOSED(5),
    STARTED(6),
    FLUSHED(7),
    DISCONNECTED(8),
    ERROR_CONNECT(101),
    ERROR_CLOSED(102),
    ERROR_UNKNOWN(103);

    private final int value;

    IPCState(int value) {
        this.value = value;
    }
}
