package com.proofpoint.ecd.ipc.platform.win.pipe;

import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.IPCState;
import com.sun.jna.platform.win32.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  jna win32 server pipe impl
 */
public class Kernel32NamedPipeServer extends Kernel32NamedPipe {

    final static Logger logger = LoggerFactory.getLogger(Kernel32NamedPipeServer.class);
    private int pipeInstances;

    public Kernel32NamedPipeServer(String name) {
        this(name, IPCMode.READ_WRITE, 1);
    }

    public Kernel32NamedPipeServer(String name, IPCMode mode) {
        this(name, mode, 1);
    }

    public Kernel32NamedPipeServer(String name, IPCMode mode, int pipeInstances) {
        init(name, mode);
        this.pipeInstances = pipeInstances;
        if (this.pipeInstances <= 0 || this.pipeInstances > WinBase.PIPE_UNLIMITED_INSTANCES) {
            this.pipeInstances = WinBase.PIPE_UNLIMITED_INSTANCES;
        }
    }
    public synchronized void start() throws Exception {
        isClosed = false;
        boolean isRestart = ipcState == IPCState.RESTART;
        if (!canStart()) {
            logger.warn("{} is not closed.", nameToString());
            return;
        }
        pipeStateChanged(ipcState, IPCState.STARTED);
        logger.debug("{} is being {}.", nameToString(), isRestart ? "restarted" : "created");
        pipe_handle = createNamedPipe(name, mode, pipeInstances);
        pipeStateChanged(ipcState, IPCState.CREATED);


        WinDef.ULONGByReference serverProcessIdRf = new WinDef.ULONGByReference();
        Kernel32.INSTANCE.GetNamedPipeServerProcessId(pipe_handle, serverProcessIdRf);
        serverProcessId = serverProcessIdRf.getValue().longValue();

        WinDef.ULONGByReference serverSessionIdRef = new WinDef.ULONGByReference();
        Kernel32.INSTANCE.GetNamedPipeServerSessionId(pipe_handle, serverSessionIdRef);
        serverSessionId = serverSessionIdRef.getValue().longValue();

        logger.info("{} {}, waiting for client connection.", nameToString(), isRestart ? "restarted" : "created");
        boolean connected = Kernel32.INSTANCE.ConnectNamedPipe(pipe_handle, null) || Kernel32.INSTANCE.GetLastError() == WinError.ERROR_PIPE_CONNECTED;
        if (!connected) {
            logger.warn("{} connection interrupted or client could not connect, closing.", nameToString());
            if (shouldClose()) {
                pipeStateChanged(ipcState, IPCState.ERROR_CONNECT);
                close();
                pipeStateChanged(ipcState, IPCState.ERROR_CLOSED);
            }
            return;
        }
        pipeStateChanged(ipcState, IPCState.CONNECTED);
        logger.info("{} connected to client.", nameToString());
        if (canRead() && handler != null && handler.canHandleRead()) {
            boolean restartServer;
            try {
                restartServer = startRead();

            } finally {
                close();
            }
            if (restartServer) {
                pipeStateChanged(ipcState, IPCState.RESTART);
                start();
            }
        }
    }

    /**
     * create a pipe
     * @param pipeName
     * @param mode
     * @param pipeInstances
     * @return
     */
    private WinNT.HANDLE createNamedPipe(String pipeName, final IPCMode mode, int pipeInstances) {

        int pipeMode;
        switch (mode) {
            case READ:
                pipeMode = WinBase.PIPE_ACCESS_INBOUND;
                break;
            case WRITE:
                pipeMode = WinBase.PIPE_ACCESS_OUTBOUND;
                break;
            default:
                pipeMode = WinBase.PIPE_ACCESS_DUPLEX;
                break;
        }

        WinNT.HANDLE pipe = Kernel32.INSTANCE.CreateNamedPipe(pipeName,
                pipeMode | WinNT.FILE_FLAG_OVERLAPPED, // dwOpenMode
                WinBase.PIPE_TYPE_MESSAGE | WinBase.PIPE_READMODE_MESSAGE | WinBase.PIPE_WAIT | WinBase.PIPE_REJECT_REMOTE_CLIENTS,// dwPipeMode
                pipeInstances,    // nMaxInstances,
                bufferSize,    // nOutBufferSize,
                bufferSize,    // nInBufferSize,
                connectTimeoutMs,    // nDefaultTimeOut,
                null    // lpSecurityAttributes
        );
        if (pipe == WinBase.INVALID_HANDLE_VALUE) {
            String emsg = String.format("pipe [%s] creation failed, error ref [%d].", pipeName, Kernel32.INSTANCE.GetLastError());
            logger.error(emsg);
            throw new RuntimeException(emsg);
        }
        return pipe;
    }
    @Override
    public String nameToString() {
        return String.format("pipe server [%s]", name);
    }
}
