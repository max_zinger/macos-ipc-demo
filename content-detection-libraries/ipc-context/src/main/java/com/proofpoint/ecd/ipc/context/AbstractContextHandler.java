package com.proofpoint.ecd.ipc.context;

import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.IPCState;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.context.model.MessageStatus;

import java.time.LocalDateTime;
import java.util.Map;

public abstract class AbstractContextHandler implements IPCOperationsHandler {

    private IPCOperationsHandler ipcOperationsHandler;

    public IPCOperationsHandler setIPCOperationsHandler(IPCOperationsHandler namedPipeOpHandler) {
        this.ipcOperationsHandler = namedPipeOpHandler;
        return this;
    }

    @Override
    public LocalDateTime getUpdatedAt() {
        return ipcOperationsHandler.getUpdatedAt();
    }

    @Override
    public void updateTime() {
        ipcOperationsHandler.updateTime();
    }

    @Override
    public boolean isInProcess() {
        return ipcOperationsHandler.isInProcess();
    }

    @Override
    public int getInProcess() {
        return ipcOperationsHandler.getInProcess();
    }

    @Override
    public int addInProcess() {
        return ipcOperationsHandler.addInProcess();
    }

    @Override
    public int removeInProcess() {
        return ipcOperationsHandler.removeInProcess();
    }

    @Override
    public boolean canHandleRead() {
        return ipcOperationsHandler.canHandleRead();
    }

    @Override
    public boolean canHandleWrite() {
        return ipcOperationsHandler.canHandleWrite();
    }

    @Override
    public boolean canHandleStateChange() {
        return ipcOperationsHandler.canHandleStateChange();
    }

    @Override
    public InterProcessCommunicable getWriter() {
        return ipcOperationsHandler.getWriter();
    }

    @Override
    public IPCOperationsHandler setIPCMode(IPCMode ipcMode) {
        return ipcOperationsHandler.setIPCMode(ipcMode);
    }

    @Override
    public void setWriter(InterProcessCommunicable writer) {
        ipcOperationsHandler.setWriter(writer);
    }

    @Override
    public void handleRead(InterProcessCommunicable pipe, byte[] data) {

        ipcOperationsHandler.handleRead(pipe, data);
    }

    @Override
    public <T> byte[] handleWrite(T data) {
        return ipcOperationsHandler.handleWrite(data);
    }

    @Override
    public void handleStateChanged(IPCState oldState, IPCState newState) {
        ipcOperationsHandler.handleStateChanged(oldState, newState);
    }

    @Override
    public IPCOperationsHandler setErrorHandler(ContextActionHandler errorHandler) {
        return ipcOperationsHandler.setErrorHandler(errorHandler);
    }

    @Override
    public void addDefaultAction(ContextActionHandler handler) {
        ipcOperationsHandler.addDefaultAction(handler);
    }

    @Override
    public void addAction(ContextAction action) {
        ipcOperationsHandler.addAction(action);
    }

    @Override
    public void addAction(String name, ContextActionHandler handler) {
        ipcOperationsHandler.addAction(name, handler);
    }

    @Override
    public MessageStatus getMessageStatus(String id) {
        return ipcOperationsHandler.getMessageStatus(id);
    }

    public MessageStatus getMessageStatus(IPCMessage msg) {
        return IPCOperationsHandler.getMessageStatus(this, msg);
    }

    @Override
    public Map<String, MessageStatus> getMessageStatusMap() {
        return ipcOperationsHandler.getMessageStatusMap();
    }
}
