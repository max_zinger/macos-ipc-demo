package com.proofpoint.ecd.ipc;

public interface IPCException {
    class IPCConnectionException extends Exception {
        public IPCConnectionException(String message) {
            super(message);
        }
    }
}
