package com.proofpoint.ecd.ipc.platform.mac.xpc;

import com.proofpoint.ecd.ipc.IPCException;

public interface XPCException extends IPCException {
    class XPCConnectionException extends IPCConnectionException {
        public XPCConnectionException(String message) {
            super(message);
        }
    }
}
