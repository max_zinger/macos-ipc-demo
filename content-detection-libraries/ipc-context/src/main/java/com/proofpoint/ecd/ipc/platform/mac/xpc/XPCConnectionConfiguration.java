package com.proofpoint.ecd.ipc.platform.mac.xpc;

import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;

public class XPCConnectionConfiguration {
    private String messageKey = "msg";
    private String messageConnectValue = "connect";
    private String serviceName;
    private int connectTimeoutMs = InterProcessCommunicable.CONNECT_TIMEOUT_MS;
    private boolean restartOnRemoteConnectionClosed = false;

    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public String getMessageConnectValue() {
        return messageConnectValue;
    }

    public void setMessageConnectValue(String messageConnectValue) {
        this.messageConnectValue = messageConnectValue;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getConnectTimeoutMs() {
        return connectTimeoutMs;
    }

    public void setConnectTimeoutMs(int connectTimeoutMs) {
        if (connectTimeoutMs > 0) {
            this.connectTimeoutMs = connectTimeoutMs;
        }

    }

    public boolean isRestartOnRemoteConnectionClosed() {
        return restartOnRemoteConnectionClosed;
    }

    public void setRestartOnRemoteConnectionClosed(boolean restartOnRemoteConnectionClosed) {
        this.restartOnRemoteConnectionClosed = restartOnRemoteConnectionClosed;
    }

    public void validate() {
        validate(this.getServiceName(), "service name");
        validate(this.getMessageKey(), "message key");
        validate(this.getMessageConnectValue(), "message connect value");

    }

    private static void validate(String val, String desc) {
        if (Utils.isStringNullOrWhitespaces(val)) {
            throw new IllegalArgumentException(desc);
        }
    }
}
