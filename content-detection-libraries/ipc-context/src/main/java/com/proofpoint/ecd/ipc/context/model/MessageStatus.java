package com.proofpoint.ecd.ipc.context.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageStatus {
    public static class Status {
        public final static String COMPLETED = "completed";
        public final static String RECEIVED = "received";
        public final static String PROCESSING = "processing";
        public final static String ERROR = "error";

        Status() {
        }
    }

    public static class MessageStatusEvent {

        String name;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        Date createdAt = new Date();

        MessageStatusEvent() {
            this(IPCMessageStatus.Defaults.MSG_RECEIVED.getStatus());
        }

        MessageStatusEvent(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public Date getCreatedAt() {
            return createdAt;
        }
    }

    private String transactionId;
    private String action;
    private String correlationId;
    private String incidentId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Date updatedAt = new Date();
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Date createdAt = new Date();


    private boolean isCompleted;
    private boolean hasError;
    private String status;
    private List<MessageStatusEvent> events = new ArrayList<>();

    public MessageStatus(IPCMessage message) {
        transactionId = message.getContext().getTransactionId();
        correlationId = message.getContext().getCorrelationId();
        action = message.getContext().getAction();
        events.add(new MessageStatusEvent());
    }

    public String getAction() {
        return action;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public String getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(String incidentId) {
        this.incidentId = incidentId;
    }

    public boolean getIsCompleted() {
        return isCompleted;
    }

    public boolean getHasError() {
        return hasError;
    }

    public String getStatus() {
        return status;
    }

    private void addEvent(String name) {
        var event = new MessageStatusEvent(name);
        events.add(event);
        updatedAt = event.createdAt;

    }

    public void addEvent(IPCMessageStatus namedPipeMessageStatus) {
        addEvent(namedPipeMessageStatus.getStatus());
        if (IPCMessageStatus.Defaults.MSG_COMPLETED.equals(namedPipeMessageStatus)) {
            isCompleted = true;
        } else if (namedPipeMessageStatus.isError()) {
            hasError = true;
        }
        setCurrentStatusFromMessage(namedPipeMessageStatus);

    }

    private void setCurrentStatusFromMessage(IPCMessageStatus namedPipeMessageStatus) {
        if (Status.ERROR.equals(this.status)) {
            return;
        }
        if (namedPipeMessageStatus.isError()) {
            this.status = Status.ERROR;
            return;
        }
        if (IPCMessageStatus.Defaults.MSG_COMPLETED.equals(namedPipeMessageStatus)) {
            this.status = Status.COMPLETED;
        } else if (IPCMessageStatus.Defaults.MSG_RECEIVED.equals(namedPipeMessageStatus)) {
            this.status = Status.RECEIVED;
        } else {
            this.status = Status.PROCESSING;
        }

    }

    public String getTransactionId() {
        return transactionId;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public List<MessageStatusEvent> getEvents() {
        return events;
    }


}
