package com.proofpoint.ecd.ipc;

public interface InterProcessCommunicable extends Runnable {
    int CONNECT_TIMEOUT_MS = 1000 * 30;//30 sec


    InterProcessCommunicable setConnectTimeoutMs(int timeout);
    InterProcessCommunicable setRestartOnRemoteConnectionClosed(boolean restartOnRemoteConnectionClosed);
    IPCState getIPCState();
    IPCOperationsHandler getIPCOperationsHandler();
    void setIPCOperationsHandler(IPCOperationsHandler handler);
    int write(byte[] data);
    <T> int write(T data);
    boolean canRead();
    boolean canWrite();
    void close() throws Exception;
    boolean isClosePending();
    boolean isClosed();
    String nameToString();
    String getName();
    void start() throws Exception;



}
