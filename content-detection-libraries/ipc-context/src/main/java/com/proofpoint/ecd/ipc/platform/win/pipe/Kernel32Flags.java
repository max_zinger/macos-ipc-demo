package com.proofpoint.ecd.ipc.platform.win.pipe;

import jdk.jfr.Unsigned;

abstract class Kernel32Flags {
    @Unsigned
    public static final int SECURITY_SQOS_PRESENT = 0x00100000;
    @Unsigned
    public static final int SECURITY_ANONYMOUS = 0 << 16;
    @Unsigned
    public static final int SECURITY_IDENTIFICATION = 1 << 16;
    @Unsigned
    public static final int SECURITY_IMPERSONATION = 2 << 16;
    @Unsigned
    public static final int SECURITY_DELEGATION = 3 << 16;
}
