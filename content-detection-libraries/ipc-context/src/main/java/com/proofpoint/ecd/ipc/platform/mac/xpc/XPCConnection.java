package com.proofpoint.ecd.ipc.platform.mac.xpc;

import com.sun.jna.Pointer;

public class XPCConnection {
    private static final XPC xpc = XPC.INSTANCE;
    XPC.PeerMessageCallback msgCallback;
    public interface MessageCallback {
        void invoke(XPCMessage xpcMessage) throws Exception;

    }

    private Pointer xpcConnectionPntr;
    private long flags = -1;
    private String serviceName;

    private XPCConnection() {
    }

    public String getServiceName() {
        return serviceName;
    }

    public static XPCConnection createPeerConnection(String serviceName) {
        return createConnection(serviceName, XPC.XPC_CONNECTION_MACH_SERVICE_NONE);
    }

//    public static XPCConnection createListenerConnection(String serviceName) {
//        return createConnection(serviceName, XPC.XPC_CONNECTION_MACH_SERVICE_LISTENER);
//    }
//
//    private static XPCConnection createPeerConnection(Pointer peer) {
//        var xpcService = new XPCConnection();
//        xpcService.xpcConnectionPntr = peer;
//        return xpcService;
//    }

    private static XPCConnection createConnection(String serviceName, long flags) {
        var xpcService = new XPCConnection();
        xpcService.serviceName = serviceName;
        xpcService.flags = flags;
        xpcService.xpcConnectionPntr = xpc.connection_create_mach_service(serviceName, xpcService.flags);
        return xpcService;
    }

    public void sendMessage(String key, String msg) {
        var message = XPCMessage.create(key, msg);
        sendMessage(message);
    }

    public void sendMessage(XPCMessage message) {
        xpc.connection_send_message(xpcConnectionPntr, message.getPointer());
    }

    public void setHandler(MessageCallback messageCallback) {
        if (flags == XPC.XPC_CONNECTION_MACH_SERVICE_NONE) {
            msgCallback = (pmsg, error) -> {
                try {
                    messageCallback.invoke(XPCMessage.create(pmsg, error));
                } catch (Exception e) {
                    //..
                }

            };
            xpc.set_peer_event_handler(xpcConnectionPntr, msgCallback);
//        } else if (flags == XPC.XPC_CONNECTION_MACH_SERVICE_LISTENER) {
//            XPC.ListenerMessageCallback msgCallback = (peer, pmsg, error) -> {
//                messageCallback.invoke(XPCMessage.create(createPeerConnection(peer), pmsg, error));
//            };
//            xpc.set_listener_event_handler(xpcConnectionPntr, msgCallback);

        } else {
            throw new RuntimeException("invalid xpc connection");
        }

    }

    public void activate() {
        xpc.connection_activate(xpcConnectionPntr);
    }

    public void suspend() {
        xpc.connection_suspend(xpcConnectionPntr);
    }

    public void cancel() {
        xpc.connection_cancel(xpcConnectionPntr);
    }

    public void resume() {
        xpc.connection_resume(xpcConnectionPntr);
    }
}
