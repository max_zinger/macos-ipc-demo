package com.proofpoint.ecd.ipc.platform.win.pipe;

import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.IPCState;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.sun.jna.platform.win32.*;
import com.sun.jna.ptr.IntByReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.locks.ReentrantLock;

/**
 * base jna win32 pipe
 */
public abstract class Kernel32NamedPipe implements NamedPipe {

    static final Logger logger = LoggerFactory.getLogger(Kernel32NamedPipe.class);
    static final byte[] new_line = {'\r', '\n'};
    protected final static String pipe_name_prefix = "\\\\.\\pipe\\";
    protected final ReentrantLock io_lock = new ReentrantLock();
    protected final ReentrantLock close_lock = new ReentrantLock();
    protected IPCState ipcState = IPCState.NONE;
    protected boolean restartOnRemoteConnectionClosed = false;
    protected boolean closePending = false;
    protected boolean isClosed = false;
    protected IPCMode mode;
    protected String name;
    protected String nameWithoutPrefix;
    protected WinNT.HANDLE pipe_handle;
    protected long serverProcessId;
    protected long serverSessionId;
    protected int bufferSize = DEFAULT_BUFFER_SIZE;
    protected int connectTimeoutMs = 1000 * 30;//30 sec
    protected IPCOperationsHandler handler;

    public void init(String name, IPCMode mode) {
        if (name == null || name.length() == 0 || name.length() > Kernel32.MAX_PIPE_NAME_LENGTH) {
            throw new IllegalArgumentException("name=[" + name + "] .pipe must have a valid name, " + Kernel32.MAX_PIPE_NAME_LENGTH + " char max.");
        }
        nameWithoutPrefix = name;
        if (!name.startsWith(pipe_name_prefix)) {
            name = pipe_name_prefix + name;
        }
        this.name = name;
        this.mode = mode;
    }

    /**
     * get current pipe state
     * @return
     */
    @Override
    public IPCState getIPCState() {
        return ipcState;
    }

    /**
     * return true if pipe is in the process of being disposed
     * @return
     */
    @Override
    public boolean isClosePending() {
        return closePending;
    }

    @Override
    public boolean canRead() {
        return mode != IPCMode.WRITE;
    }

    @Override
    public boolean canWrite() {
        return mode != IPCMode.READ;
    }

    /**
     * return true if pipe should be restarted after a recoverable error or a peer disconnect
     * @return
     */
    public boolean isRestartOnRemoteConnectionClosed() {
        return restartOnRemoteConnectionClosed;
    }

    @Override
    public NamedPipe setRestartOnRemoteConnectionClosed(boolean restartOnRemoteConnectionClosed) {
        this.restartOnRemoteConnectionClosed = restartOnRemoteConnectionClosed;
        return this;
    }

    @Override
    public void setIPCOperationsHandler(IPCOperationsHandler handler) {
        this.handler = handler;
    }

    @Override
    public IPCOperationsHandler getIPCOperationsHandler() {
        return handler;
    }

    @Override
    public boolean isClosed() {
        return isClosed;
    }

    /**
     * returns true if current state allows to dispose the pipe
     * @return
     */
    protected boolean shouldClose() {
        return !(pipe_handle == null || ipcState == IPCState.CLOSED || ipcState == IPCState.ERROR_CLOSED);
    }
    @Override
    public InterProcessCommunicable setConnectTimeoutMs(int timeout) {
        if (timeout > 0) {
            connectTimeoutMs = timeout;
        }
        return this;
    }
    /**
     * flush, disconnect, close and dispose pipe
     * @throws Exception
     */
    public void close() throws Exception {
        isClosed = true;
        if (!shouldClose()) {
            return;
        }
        close_lock.lock();
        if (!shouldClose()) {
            return;
        }
        closePending = true;
        try {
            if (ipcState == IPCState.CONNECTED) {
                Kernel32.INSTANCE.FlushFileBuffers(pipe_handle);
                pipeStateChanged(ipcState, IPCState.FLUSHED);
                Kernel32.INSTANCE.DisconnectNamedPipe(pipe_handle);
                pipeStateChanged(ipcState, IPCState.DISCONNECTED);
            }
            Kernel32.INSTANCE.CloseHandle(pipe_handle);
            pipeStateChanged(ipcState, IPCState.CLOSED);
            logger.info("{} is closed.", nameToString());

        } catch (Exception e) {
            logger.error(nameToString() + " safe close failed, last state [" + getIPCState().name() + "] error ref " + Kernel32.INSTANCE.GetLastError(), e);
            pipeStateChanged(ipcState, IPCState.ERROR_UNKNOWN);
            throw new Exception(e);
        } finally {

            close_lock.unlock();
        }
        closePending = false;

    }

    /**
     * write to pipe
     * @param data
     * @return
     */
    @Override
    public int write(byte[] data) {
        if (!canWrite()) {
            throw new RuntimeException(nameToString() + " write mode unsupported.");
        }
        if (pipe_handle == null) {
            throw new NullPointerException(nameToString() + " must be initialized.");
        }
        if (data == null || data.length == 0) {
            throw new IllegalArgumentException("no data.");
        }
        io_lock.lock();
        int bytesWritten = 0;
        data = addAbsentNewline(data);

        IntByReference lpNumberOfBytesWritten = new IntByReference(0);

        try {
            if (Kernel32.INSTANCE.WriteFile(pipe_handle, data, data.length, lpNumberOfBytesWritten, null)) {
                bytesWritten = lpNumberOfBytesWritten.getValue();
                logger.debug("{} message sent to client. message length [{}], bytes written [{}].", nameToString(), data.length, bytesWritten);
            } else {
                logger.info("{} failed to send message.", nameToString());
                if (mode == IPCMode.WRITE && lastErrorState()) {
                    close();
                    pipeStateChanged(ipcState, IPCState.RESTART);
                    start();
                }

            }
        } catch (Exception e) {
            logger.error(nameToString() + " an error occurred while writing.", e);
        } finally {
            io_lock.unlock();
        }

        return bytesWritten;
    }
    /**
     * write to pipe
     * @param obj
     * @return
     */
    @Override
    public <T> int write(T obj) {
        if (!canWrite() || handler == null || !handler.canHandleWrite()) {
            throw new RuntimeException(nameToString() + " write mode unsupported.");
        }
        var data = handler.handleWrite(obj);
        return write(data);

    }

    @Override
    public boolean equals(Object o) {
        if (pipe_handle == null || o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }
        Kernel32NamedPipe pipeServer = (Kernel32NamedPipe) o;
        return pipe_handle.equals(pipeServer.pipe_handle);
    }

    @Override
    public int hashCode() {
        return pipe_handle.hashCode();
    }

    /**
     * returns the pipe name without os prefix
     * @return
     */
    @Override
    public String getName() {
        return nameWithoutPrefix;
    }

    /**
     * get current buffer size
     * @param bufferSize
     */
    @Override
    public void setBufferSize(int bufferSize) {
        if (bufferSize > 0) {
            this.bufferSize = bufferSize;
        }

    }

    @Override
    public void run() {
        try {
            start();
        } catch (Exception e) {
            logger.error("an error occurred", e);
        }
    }

    /**
     * blocking read from pipe
     * @return
     */
    public byte[] readMessage() {
        byte[] readBuffer = new byte[bufferSize];
        IntByReference lpBytesReadPeek = new IntByReference(0);
        IntByReference lpTotalBytesAvailPeek = new IntByReference(0);
        IntByReference lpBytesLeftThisMessagePeek = new IntByReference(0);
        IntByReference lpNumberOfBytesRead = new IntByReference(0);
        byte[] message = null;
        int messageIdx = 0, 
            currentMsgReadIter = 0;
        boolean isMessageComplete = false, 
            maxMsgReadIterExceeded = false, 
            peek = false, 
            read = false;
        do {
            peek = Kernel32.INSTANCE.PeekNamedPipe(pipe_handle,
                    null,
                    readBuffer.length,
                    lpBytesReadPeek,
                    lpTotalBytesAvailPeek,
                    lpBytesLeftThisMessagePeek);
            if (!peek) {
                break;
            }
            logger.debug("{} [PeekNamedPipe] current bytes to read [{}]; total buffered bytes [{}]; bytes left for message after current read [{}].", nameToString(), lpBytesReadPeek.getValue(), lpTotalBytesAvailPeek.getValue(), lpBytesLeftThisMessagePeek.getValue());

            read = Kernel32.INSTANCE.ReadFile(pipe_handle,
                    readBuffer,
                    lpBytesReadPeek.getValue(),
                    lpNumberOfBytesRead,
                    null);
            if (!read && Kernel32.INSTANCE.GetLastError() != WinError.ERROR_MORE_DATA) {
                break;
            }
            read = true;
            int messageBytesLeft = lpBytesLeftThisMessagePeek == null || lpBytesLeftThisMessagePeek.getValue() <= 0 ? 0 : lpBytesLeftThisMessagePeek.getValue();
            int readSize = lpNumberOfBytesRead.getValue();
            logger.debug("{} received remote message. current bytes read [{}]. current bytes read [{} / {}].", nameToString(), readSize, messageIdx + readSize, messageIdx + readSize + messageBytesLeft);
            if (message == null) {
                message = new byte[readSize + messageBytesLeft];
            }
            if (readSize <= 0) {
                break;
            }
            for (int i = 0; i < readSize; i++) {
                message[messageIdx + i] = readBuffer[i];
            }
            messageIdx += readSize;

            isMessageComplete = messageBytesLeft <= 0;

            currentMsgReadIter++;
            maxMsgReadIterExceeded = currentMsgReadIter > MAX_READ_ITERATIONS;

        } while (!isMessageComplete && !maxMsgReadIterExceeded);

        if (!peek || !read) {
            return null;
        }
        if (!isMessageComplete && maxMsgReadIterExceeded) {
            logger.warn("{} message read iterations exceeded limit [{}], buffer size [{}], max message length [{}].", nameToString(), MAX_READ_ITERATIONS, bufferSize, MAX_READ_ITERATIONS * bufferSize);
        }
        if (message.length != messageIdx) {
            logger.warn("{} message is missing data [{} / {}].", nameToString(), message.length, messageIdx);
        }
        return removeExistingNewline(message, message.length);
    }

    /**
     * start reading from pipe
     * @return
     */
    protected boolean startRead() {
        boolean restartServer = false;
        while (!closePending && !isClosed) {
            byte[] data = readMessage();
            if (data == null) {
                restartServer = lastErrorState();
                break;
            } else if (data.length > 0) {
                handler.handleRead(this, data);
            }
        }

        return restartServer;
    }

    /**
     * returns true if current state allows starting the pipe
     * @return
     */
    protected boolean canStart() {
        return ipcState == IPCState.CLOSED || ipcState == IPCState.ERROR_CLOSED || ipcState == IPCState.NONE || ipcState == IPCState.RESTART;
    }

    /**
     * inspects last error and determine if pipe can be restarted
     * @return
     */
    private boolean lastErrorState() {
        int err = Kernel32.INSTANCE.GetLastError();
        boolean shouldRestart = false;
        switch (err) {
            case WinError.ERROR_NO_DATA:
            case WinError.ERROR_BROKEN_PIPE:
                logger.info("{} remote connection closed.", nameToString());
                shouldRestart = true;
                break;
            case WinError.ERROR_INVALID_HANDLE:
            case WinError.ERROR_PIPE_NOT_CONNECTED:
                logger.info("{} handle invalidated.", nameToString());
                break;
            default:
                logger.error("{} unexpectedly closed. error ref [{}]", nameToString(), err);
        }
        return restartOnRemoteConnectionClosed && shouldRestart;
    }

    /**
     * add a newline at the end of the message
     * @param data
     * @return
     */
    private byte[] addAbsentNewline(byte[] data) {
        if (data.length >= 2 && data[data.length - 2] != new_line[0] && data[data.length - 1] != new_line[1]) {
            var tdata = new byte[data.length + 2];
            System.arraycopy(data, 0, tdata, 0, data.length);
            tdata[tdata.length - 2] = new_line[0];
            tdata[tdata.length - 1] = new_line[1];
            data = tdata;
        }
        return data;
    }

    /**
     * removes a newline from the end of the message
     * @param data
     * @param bytesToRead
     * @return
     */
    private byte[] removeExistingNewline(byte[] data, int bytesToRead) {

        if (bytesToRead >= 2 && data[bytesToRead - 2] == '\r' && data[bytesToRead - 1] == '\n') {
            bytesToRead -= 2;
        }
        if (bytesToRead != data.length) {
            var tdata = new byte[bytesToRead];
            System.arraycopy(data, 0, tdata, 0, bytesToRead);
            return tdata;
        }
        return data;
    }

    protected void pipeStateChanged(IPCState oldState, IPCState newState) {
        if (handler != null && handler.canHandleStateChange()) {
            handler.handleStateChanged(oldState, newState);
        }
        ipcState = newState;
        logger.debug("{} state change to [{}]. was [{}].", nameToString(), newState.name(), oldState.name());
    }


}
