package com.proofpoint.ecd.ipc.context.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.FileNotFoundException;
import java.nio.file.AccessDeniedException;
import java.nio.file.NoSuchFileException;
import java.util.Objects;
import java.util.concurrent.TimeoutException;

public class IPCMessageStatus {
    public static class InvalidActionException extends Exception {
        public InvalidActionException(String action) {
            super("action not found [" + action + "]");
        }
    }

    public static class Defaults {
        public static final IPCMessageStatus OK = new IPCMessageStatus(0, "ok");
        public static final IPCMessageStatus PARTIAL_SUCCESS_WRITE_LIMIT = new IPCMessageStatus(1, "partially-succeeded:write-limit");
        public static final IPCMessageStatus PARTIAL_SUCCESS_TIMEOUT = new IPCMessageStatus(2, "partially-succeeded:timeout");
        public static final IPCMessageStatus INTERNAL_ERROR = new IPCMessageStatus(100, "internal-error");
        public static final IPCMessageStatus BAD_ARGUMENTS = new IPCMessageStatus(101, "bad-arguments");
        public static final IPCMessageStatus NOT_FOUND = new IPCMessageStatus(102, "resource-not-found");
        public static final IPCMessageStatus INVALID_ACTION = new IPCMessageStatus(103, "invalid-action");
        public static final IPCMessageStatus TIMEOUT = new IPCMessageStatus(104, "operation-timeout");
        public static final IPCMessageStatus ACCESS_DENIED = new IPCMessageStatus(105, "access-denied");
        public static final IPCMessageStatus FILE_NOT_FOUND = new IPCMessageStatus(106, "file-not-found");

        public static final IPCMessageStatus MSG_RECEIVED = new IPCMessageStatus(1000, "msg-received");
        public static final IPCMessageStatus MSG_ENQUEUE = new IPCMessageStatus(1001, "msg-enqueue");
        public static final IPCMessageStatus MSG_PROCESSING = new IPCMessageStatus(1002, "msg-processing");
        public static final IPCMessageStatus MSG_COMPLETED = new IPCMessageStatus(1003, "msg-completed");

    }

    @JsonIgnore
    public boolean isError() {
        return getCode() >= 100 && getCode() < 200;
    }

    public static IPCMessageStatus fromException(Exception e) {
        if (e == null) {
            return Defaults.INTERNAL_ERROR;
        }
        if (e instanceof JsonProcessingException) {
            return Defaults.BAD_ARGUMENTS;
        }
        if (e instanceof IllegalArgumentException) {
            return Defaults.BAD_ARGUMENTS;
        }
        if (e instanceof NoSuchFileException) {
            return Defaults.FILE_NOT_FOUND;
        }
        if (e instanceof FileNotFoundException) {
            return Defaults.FILE_NOT_FOUND;
        }
        if (e instanceof TimeoutException) {
            return Defaults.TIMEOUT;
        }
        if (e instanceof AccessDeniedException) {
            return Defaults.ACCESS_DENIED;
        }
        if (e instanceof InvalidActionException) {
            return Defaults.INVALID_ACTION;
        }
        return Defaults.INTERNAL_ERROR;
    }

    private int code;
    private String status;

    public IPCMessageStatus() {
    }

    public IPCMessageStatus(int code, String status) {
        this.code = code;
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "NamedPipeMessageStatus{" +
                "code=" + code +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IPCMessageStatus that = (IPCMessageStatus) o;
        return code == that.code && Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, status);
    }
}
