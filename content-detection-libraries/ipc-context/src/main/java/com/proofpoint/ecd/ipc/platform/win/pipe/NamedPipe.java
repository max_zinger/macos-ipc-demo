package com.proofpoint.ecd.ipc.platform.win.pipe;

import com.proofpoint.ecd.ipc.InterProcessCommunicable;

public interface NamedPipe extends InterProcessCommunicable {
    int DEFAULT_BUFFER_SIZE = 4096;
    int MAX_READ_ITERATIONS = 1024;
    byte[] readMessage();
    void setBufferSize(int bufferSize);

}
