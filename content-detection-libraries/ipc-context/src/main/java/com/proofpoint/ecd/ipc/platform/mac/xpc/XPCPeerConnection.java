package com.proofpoint.ecd.ipc.platform.mac.xpc;

import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.ipc.IPCException;
import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.IPCState;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

public class XPCPeerConnection implements InterProcessCommunicable {
    private static final Logger logger = LoggerFactory.getLogger(XPCPeerConnection.class);
    private static int connect_retry_interval_ms = 500;//0.5 sec

    private XPCConnection connection;
    private IPCState ipcState = IPCState.NONE;
    private boolean closePending = false;
    private boolean isClosed = false;
    private IPCOperationsHandler handler;
    private XPCConnectionConfiguration configuration = new XPCConnectionConfiguration();
    XPCPeerEventHandler messageHandler = null;


    public XPCPeerConnection(String name) {
        configuration.setServiceName(name);
        configuration.validate();
    }

    public XPCPeerConnection(XPCConnectionConfiguration configuration) {
        configuration.validate();
        this.configuration = configuration;
    }
    @Override
    public InterProcessCommunicable setConnectTimeoutMs(int timeout) {
        configuration.setConnectTimeoutMs(timeout);
        return this;
    }

    @Override
    public InterProcessCommunicable setRestartOnRemoteConnectionClosed(boolean restartOnRemoteConnectionClosed) {
        configuration.setRestartOnRemoteConnectionClosed(restartOnRemoteConnectionClosed);
        return this;
    }

    @Override
    public IPCState getIPCState() {
        return ipcState;
    }

    @Override
    public IPCOperationsHandler getIPCOperationsHandler() {
        return handler;
    }

    @Override
    public void setIPCOperationsHandler(IPCOperationsHandler handler) {
        this.handler = handler;
    }

    @Override
    public int write(byte[] data) {
        String str = new String(data, StandardCharsets.UTF_8);
        connection.sendMessage(configuration.getMessageKey(), str);
        return str.length();
    }

    @Override
    public <T> int write(T obj) {
        if (handler == null || !handler.canHandleWrite()) {
            throw new RuntimeException(nameToString() + " write mode unsupported.");
        }
        return write(handler.handleWrite(obj));
    }

    @Override
    public boolean canRead() {
        return true;
    }

    @Override
    public boolean canWrite() {
        return true;
    }

    @Override
    public void close() throws Exception {
        if (isClosed) {
            return;
        }
        logger.info("canceling {} connection", nameToString());
        try {
            connection.cancel();
            ipcStateChanged(ipcState, IPCState.CLOSED);

        } catch (Exception e) {
            ipcStateChanged(ipcState, IPCState.ERROR_CLOSED);
            throw e;

        } finally {
            isClosed = true;
        }
        logger.info("{} connection cancelled", nameToString());

    }

    @Override
    public boolean isClosePending() {
        return closePending;
    }

    @Override
    public boolean isClosed() {
        return isClosed;
    }

    @Override
    public String nameToString() {
        return String.format("xpc peer [%s]", configuration.getServiceName());
    }


    @Override
    public String getName() {
        return configuration.getServiceName();
    }

    @Override
    public void start() throws Exception {
        try {
            isClosed = false;
            ipcStateChanged(ipcState, IPCState.STARTED);
            int currentTotalConnectMs = 0;
            while (!isClosed && (currentTotalConnectMs < configuration.getConnectTimeoutMs())) {
                logger.info("{} waiting for listener connection [{} ms].", nameToString(), currentTotalConnectMs);
                currentTotalConnectMs += connect_retry_interval_ms;

                try {
                    messageHandler = new XPCPeerEventHandler(this, handler, configuration);
                    messageHandler.error = false;
                    messageHandler.closeOnIPCException = false;

                    connection = XPCConnection.createPeerConnection(configuration.getServiceName());
                    connection.setHandler(messageHandler);
                    connection.activate();

                    Thread.sleep(connect_retry_interval_ms);
                    if (messageHandler.error) {
                        continue;
                    }
                    connection.sendMessage(configuration.getMessageKey(), configuration.getMessageConnectValue());
                    if (messageHandler.error) {
                        continue;
                    }
                    ipcStateChanged(ipcState, IPCState.CREATED);
                    break;
                } catch (Exception e) {
                    logger.error("failed to connect", e);
                }

            }
            if (messageHandler.error) {
                throw new RuntimeException(String.format("failed to connect [%s]. timeout exceeded [%d].", nameToString(), configuration.getConnectTimeoutMs()));
            }
            if (isClosed) {
                throw new RuntimeException(String.format("failed to connect [%s]. was closed.", nameToString()));
            }
            if (messageHandler == null) {
                throw new RuntimeException("message callback handler");
            }
            messageHandler.closeOnIPCException = true;

            logger.info("{} connecting to listener.", nameToString());
            ipcStateChanged(ipcState, IPCState.CONNECTED);
            logger.info("{} connected to listener.", nameToString());

        } catch (Exception e) {
            logger.error(String.format("[%s] error", nameToString()), e);
            close();
        }
    }

    private static class XPCPeerEventHandler implements XPCConnection.MessageCallback {
        InterProcessCommunicable ipc;
        IPCOperationsHandler handler;
        XPCConnectionConfiguration configuration;
        boolean error;
        boolean closeOnIPCException;

        XPCPeerEventHandler(InterProcessCommunicable ipc, IPCOperationsHandler handler, XPCConnectionConfiguration configuration) {
            this.ipc = ipc;
            this.handler = handler;
            this.configuration = configuration;
        }

        @Override
        public void invoke(XPCMessage xpcMessage) throws Exception {
            try {
                String desc = xpcMessage.getDescription();
                if (xpcMessage.isError()) {
                    error = true;
                    throw new XPCException.XPCConnectionException(xpcMessage.getDescription());
                }
                logger.debug("{} incoming message {}", ipc.nameToString(), desc);
                var msg = xpcMessage.getValue(configuration.getMessageKey());
                if (configuration.getMessageConnectValue().equals(msg)) {
                    logger.info("{} connected", ipc.nameToString());
                    return;
                }
                handler.handleRead(ipc, msg.getBytes(StandardCharsets.UTF_8));
            } catch (Exception e) {

                if (closeOnIPCException && IPCException.IPCConnectionException.class.isAssignableFrom(e.getClass())) {
                    logger.error(String.format("[%s] error occurred on message read callback", ipc.nameToString()), e);
                    ipc.close();
                }
            }

        }
    }

    @Override
    public void run() {
        try {
            start();
        } catch (Exception e) {
            logger.error("an error occurred", e);
        }
    }

    private void ipcStateChanged(IPCState oldState, IPCState newState) {
        if (handler != null && handler.canHandleStateChange()) {
            handler.handleStateChanged(oldState, newState);
        }
        ipcState = newState;
        logger.debug("{} state change to [{}]. was [{}].", nameToString(), newState.name(), oldState.name());
    }
}
