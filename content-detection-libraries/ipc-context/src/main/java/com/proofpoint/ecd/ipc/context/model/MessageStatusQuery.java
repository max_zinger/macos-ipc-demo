package com.proofpoint.ecd.ipc.context.model;

public class MessageStatusQuery {
    private String[] transactionIdSet = {};

    public String[] getTransactionIdSet() {
        return transactionIdSet;
    }

    public void setTransactionIdSet(String[] transactionIdSet) {
        this.transactionIdSet = transactionIdSet;
    }

    public static class Message extends IPCMessage<MessageStatusQuery> {
        public Message() {
            data = new MessageStatusQuery();
        }
    }
}
