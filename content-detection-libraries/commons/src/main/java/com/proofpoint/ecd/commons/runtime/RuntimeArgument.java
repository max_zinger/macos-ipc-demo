package com.proofpoint.ecd.commons.runtime;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface RuntimeArgument {
    boolean ignore() default false;

    int minValue() default Integer.MIN_VALUE;

    String [] arg();

    String desc() default "";

    boolean hidden() default false;

    class Value {
        RuntimeArgument arg;

        public Value(RuntimeArgument arg) {
            this.arg = arg;
        }

        public RuntimeArgument get() {
            return arg;
        }

        public boolean hasMinIntValue() {
            return arg.minValue() != Integer.MIN_VALUE;
        }

    }

}