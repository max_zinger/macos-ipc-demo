package com.proofpoint.ecd.commons.runtime;

import com.proofpoint.ecd.commons.Utils;
import org.apache.commons.lang3.ClassUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * parse runtime arguments
 */
public abstract class RuntimeConfiguration {
    public abstract void apply();

    /**
     * appends runtime argument list to configuration object
     *
     * @param args
     */
    public void append(List<String> args) {
        if (args == null || args.isEmpty()) {
            return;
        }

        for (Field field : getAddDeclaredFields(getClass())) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(RuntimeArgument.class)) {
                var annotation = new RuntimeArgument.Value(field.getAnnotation(RuntimeArgument.class));
                if (annotation.get().ignore()) {
                    continue;
                }
                try {
                    var value = parse(this, args, field, annotation);
                    if (value != null && value.getClass() == String.class && ((String) value).startsWith("--")) {
                        throw new RuntimeException("invalid argument value " + value);
                    }
                    field.set(this, value);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            } else if (!ClassUtils.isPrimitiveOrWrapper(field.getType())) {
                try {
                    if (RuntimeConfiguration.class.isAssignableFrom(Class.forName(field.getGenericType().getTypeName()))) {
                        var obj = (RuntimeConfiguration) field.get(this);
                        obj.append(args);
                    }
                } catch (ClassNotFoundException e) {
                    continue;
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }

            }
            field.setAccessible(false);
        }
        apply();
    }

    public static List<String> help(Object target) {
        var helpList = new ArrayList<String>();
        return help(target, helpList);
    }

    public static List<String> help(Object target, List<String> helpList) {

        for (Field field : getAddDeclaredFields(target.getClass())) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(RuntimeArgument.class)) {
                var annotation = new RuntimeArgument.Value(field.getAnnotation(RuntimeArgument.class));
                if (annotation.get().ignore() || annotation.get().hidden()) {
                    continue;
                }
                try {
                    var argNames = String.join(" | ", Arrays.stream(annotation.get().arg()).distinct().filter(a -> !Utils.isStringNullOrWhitespaces(a)).toArray(String[]::new));
                    String helpEntry = String.format("%-35s %s (default: %s%s)", argNames,
                            annotation.get().desc(), field.get(target),
                            annotation.hasMinIntValue() ? String.format(", min: %s", annotation.get().minValue()) : "");
                    helpList.add(helpEntry);

                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            } else if (!ClassUtils.isPrimitiveOrWrapper(field.getType())) {
                try {
                    if (RuntimeConfiguration.class.isAssignableFrom(Class.forName(field.getGenericType().getTypeName()))) {
                        help(field.get(target), helpList);
                    }
                } catch (ClassNotFoundException e) {
                    continue;
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
            field.setAccessible(false);
        }
        return helpList;
    }

    public static RuntimeArgument getArgumentByFieldName(Object target, String fieldName) {
        return getArgumentByFieldName(target.getClass(), fieldName);
    }

    public static RuntimeArgument getArgumentByFieldName(Class<?> c, String fieldName) {
        var field = getAddDeclaredFields(c).stream().filter(f -> f.isAnnotationPresent(RuntimeArgument.class)
                && f.getName().equalsIgnoreCase(fieldName)).findFirst();
        if (field != null && !field.isEmpty()) {
            return field.get().getAnnotation(RuntimeArgument.class);
        }
        return null;
    }

    private static String getArgName(List<String> args, RuntimeArgument arg) {
        var argName = Arrays.stream(arg.arg()).distinct().filter(a -> !Utils.isStringNullOrWhitespaces(a) && Utils.hasArg(args, a)).findFirst();
        if (argName.isPresent()) {
            return argName.get();
        }
        return arg.arg()[0];
    }

    private static Object parse(Object target, List<String> args, Field field, RuntimeArgument.Value annotation)
            throws IllegalAccessException {
        String argName = getArgName(args, annotation.get());
        if (field.getType() == int.class) {
            var defaultValue = field.getInt(target);
            if (annotation.hasMinIntValue()) {
                return Utils.getArgsIntValue(args, argName, defaultValue, annotation.get().minValue());
            }
            return Utils.getArgsIntValue(args, argName, defaultValue);
        }
        if (field.getType() == long.class) {
            var defaultValue = field.getLong(target);
            if (annotation.hasMinIntValue()) {
                return Utils.getArgsLongValue(args, argName, defaultValue, annotation.get().minValue());
            }
            return Utils.getArgsLongValue(args, argName, defaultValue);
        }
        if (field.getType() == boolean.class) {
            var defaultValue = field.getBoolean(target);
            return Utils.getArgsBooleanValue(args, argName, defaultValue);
        }
        var defaultValue = field.get(target);
        return Utils.getArgsValue(args, argName, defaultValue);
    }

    private static List<Field> getAddDeclaredFields(Class<?> c) {
        var fields = new ArrayList<Field>();
        addDeclaredFields(fields, c);
        return fields;
    }

    private static void addDeclaredFields(List<Field> fields, Class<?> c) {
        fields.addAll(Arrays.asList(c.getDeclaredFields()));

        if (c.getSuperclass() != null) {
            addDeclaredFields(fields, c.getSuperclass());
        }
    }
}
