package com.proofpoint.ecd.commons;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class Utils {
    /**
     * logging json mapper
     */
    public static final ObjectMapper JSON_FORMATTER_INDENT_OUTPUT = new ObjectMapper()
            .enable(SerializationFeature.INDENT_OUTPUT);
    /**
     * default json mapper
     */
    private static final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    static {
        var timezone = Calendar.getInstance().getTimeZone();
        JSON_FORMATTER_INDENT_OUTPUT.setTimeZone(timezone);
        mapper.setTimeZone(timezone);
    }

    private static final String format_err_min = "[%d] is an illegal value for [%s] = [%d]";

    public static boolean isStringNullOrWhitespaces(String str) {
        return str == null || str.trim().isEmpty();
    }

    public static boolean getArgsBooleanValue(List<String> args, String argName, boolean defaultValue) {
        return Boolean.parseBoolean(getArgsValue(args, argName, defaultValue).toString());
    }

    public static int getArgsIntValue(List<String> args, String argName, int defaultValue) {
        return Integer.parseInt(getArgsValue(args, argName, defaultValue).toString());
    }

    public static int getArgsIntValue(List<String> args, String argName, int defaultValue, int minValue) {
        var val = getArgsIntValue(args, argName, defaultValue);
        if (val < minValue) {
            val = minValue;
        }
        return val;
    }

    public static long getArgsLongValue(List<String> args, String argName, long defaultValue, long minValue) {
        var val = getArgsLongValue(args, argName, defaultValue);
        if (val < minValue) {
            val = minValue;
        }
        return val;
    }

    public static long getArgsLongValue(List<String> args, String argName, long defaultValue) {
        return Long.parseLong(getArgsValue(args, argName, defaultValue).toString());
    }

    public static Object getArgsValue(List<String> args, String argName, Object defaultValue) {
        int idx = args.indexOf(argName);
        String arg;
        if (idx >= 0 && args.size() > idx + 1 && !isStringNullOrWhitespaces(arg = args.get(idx + 1))) {
            return arg;
        }
        return defaultValue;
    }

    public static boolean hasArg(List<String> args, String argName) {
        if (args == null || args.isEmpty()) {
            return false;
        }
        return args.indexOf(argName) >= 0;
    }

    public static <T> T readJsonFromResource(Class<?> c, String filename, TypeReference<T> type) {
        try {
            return mapper.readValue(readFromResource(c, filename), type);
        } catch (IOException e) {
            throw new RuntimeException("failed to read JSON file from resource.", e);
        }
    }

    public static <T> T readJsonFromResource(Class<?> c, String filename, Class<T> type) {
        try {
            return mapper.readValue(readFromResource(c, filename), type);
        } catch (IOException e) {
            throw new RuntimeException("failed to read JSON file from resource.", e);
        }
    }

    public static byte[] readFromResource(Class<?> c, String filename) {
        try {
            return c.getClassLoader().getResourceAsStream(filename).readAllBytes();
        } catch (IOException e) {
            throw new RuntimeException("failed to read file from resource.", e);
        }

    }

    public static String readStringFromResource(Class<?> c, String filename) {
        return new String(readFromResource(c, filename));
    }

    public static void awaitTermination(ExecutorService executors, long timeout, TimeUnit unit) {
        executors.shutdown();
        try {
            if (!executors.awaitTermination(timeout, unit)) {
                executors.shutdownNow();
            }
        } catch (InterruptedException e) {
            executors.shutdownNow();
            Thread.currentThread().interrupt();
            throw new RuntimeException("failed to terminate executor threads.", e);
        }
    }

    /**
     * get application context path
     *
     * @return
     */
    public static String getAppPath(Class<?> c) {
        String userDir = System.getProperty("user.dir");
        String dest;
        try {
            var file = new File(c.getProtectionDomain().getCodeSource().getLocation().toURI());
            if (file.isFile()) {
                dest = file.getParentFile().getPath();
            } else if (Path.of(userDir, "/target/classes").equals(file.toPath())) {
                dest = userDir;
            } else {
                dest = file.getPath();
            }
            if (!Files.exists(Paths.get(dest))) {
                try {
                    Files.createDirectories(Paths.get(dest));
                } catch (Exception e) {
                    return userDir;
                }
            }
            return dest;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return userDir;
    }

    public static void validateMinValue(long value, long minValue, String argName) {
        if (value < minValue) {
            throw new IllegalArgumentException(String.format(format_err_min, value, argName, minValue));
        }
    }

}
