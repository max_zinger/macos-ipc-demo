package com.proofpoint.ecd.commons.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.LoggerContextListener;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.spi.ContextAwareBase;
import ch.qos.logback.core.spi.LifeCycle;
import com.proofpoint.ecd.commons.Utils;

/**
 * listener will be instantiated on the first logger call - all configurations should be applied before that
 */
public class RootLoggerConfiguration extends ContextAwareBase implements LoggerContextListener, LifeCycle {

    public static final String CTX_LOG_PATH = "LOG_PATH";
    public static final String CTX_LOG_FILE = "LOG_FILE";

    private static final String default_log_path = "logs";
    private static final String default_log_file = "log";

    private static Level defaultLevel = Level.INFO;
    private static Level currentLevel = Level.INFO;

    public static Level getDefaultLogLevel() {
        return defaultLevel;
    }

    public static Level getRootLogLevel() {
        return currentLevel;
    }

    private boolean started = false;
    private static String log_path = default_log_path;
    private static String log_file = default_log_file;

    public static void configure(String logPath, String logFile) {
        if (!Utils.isStringNullOrWhitespaces(logPath)) {
            log_path = logPath;
        }
        if (!Utils.isStringNullOrWhitespaces(logFile)) {
            log_file = logFile;
        }
        defaultLevel = ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
                .getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME)).getLevel();
        currentLevel = defaultLevel;
    }

    public static void setRootLogLevel(String levelStr) {
        Level level = Level.toLevel(levelStr, currentLevel);
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME))
                .setLevel(level);
        currentLevel = level;
    }

    public static void resetRootLogLevel() {
        ((ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME))
                .setLevel(defaultLevel);
    }

    @Override
    public void start() {
        if (started) {
            return;
        }
        Context context = getContext();

        context.putProperty(CTX_LOG_PATH, log_path);
        context.putProperty(CTX_LOG_FILE, log_file);

        started = true;
    }

    @Override
    public void stop() {
    }

    @Override
    public boolean isStarted() {
        return started;
    }

    @Override
    public boolean isResetResistant() {
        return true;
    }

    @Override
    public void onStart(LoggerContext context) {
    }

    @Override
    public void onReset(LoggerContext context) {
    }

    @Override
    public void onStop(LoggerContext context) {
    }

    @Override
    public void onLevelChange(Logger logger, Level level) {
    }

}
