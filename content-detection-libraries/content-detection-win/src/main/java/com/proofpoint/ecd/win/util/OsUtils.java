package com.proofpoint.ecd.win.util;

public interface OsUtils {
    class OS {
        public static final String NAME = System.getProperty("os.name").toLowerCase();
        public static final boolean WINDOWS = NAME.contains("win");
        public static final boolean MACOS = NAME.contains("mac");
    }

    class ServiceState {
        public static final int UNKNOWN = -1;
        public static final int STOPPED = 1;
        public static final int RUNNING = 4;
        //public static final int STARTED = 2;
        private ServiceState() {
        }
    }

    String getUsername();

    String getHostname();

    boolean isEndpointITCloudServiceRunning();

    boolean isServiceRunning(String serviceName);

    int getServiceState(String serviceName);
}
