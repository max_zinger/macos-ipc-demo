package com.proofpoint.ecd.win.util.mutex;

import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinNT;

public class Kernel32Mutex implements Mutex {

    private String name;
    private WinNT.HANDLE handle;

    public String getName() {
        return name;
    }

    public Kernel32Mutex(String name) {
        this.name = name;
    }

    public boolean create() {
        handle = Kernel32.INSTANCE.CreateMutex(null, true, name);
        if (handle == null) {
            throw new RuntimeException("failed to create mutex [" + name + "]");
        }
        return true;
    }

    public WinNT.HANDLE getHandle() {
        return handle;
    }

    public boolean release() {
        if (handle == null) {
            throw new NullPointerException("mutex handle is null");
        }

        return Kernel32.INSTANCE.ReleaseMutex(handle) && Kernel32.INSTANCE.CloseHandle(handle);// && open(name) == null;

    }

    public static Kernel32Mutex open(String name) {
        var handle = Kernel32.INSTANCE.OpenMutex(WinNT.SYNCHRONIZE, false, name);
        if (handle == null) {
            return null;
        }
        var mutex = new Kernel32Mutex(name);
        mutex.handle = handle;
        return mutex;
    }

}
