package com.proofpoint.ecd.win.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Optional;

public class WinEndpointServiceQuery {

    //win sc query
    /*
     SERVICE_NAME: wscsvc
     DISPLAY_NAME: Security Center
     TYPE               : 30  WIN32
     STATE              : 4  RUNNING
             (STOPPABLE, NOT_PAUSABLE, ACCEPTS_SHUTDOWN)
     WIN32_EXIT_CODE    : 0  (0x0)
     SERVICE_EXIT_CODE  : 0  (0x0)
     CHECKPOINT         : 0x0
     WAIT_HINT          : 0x0
     */
    private static final String win_endpoint_itc_service_name = "IT Cloud Service";
    private static final String win_query_service_cmd = "cmd.exe /c sc query";

    public static boolean isEndpointITCloudServiceRunning() throws IOException, InterruptedException {
        return isServiceRunning(win_endpoint_itc_service_name);
    }

    public static boolean isServiceRunning(String serviceName) throws IOException, InterruptedException {
        return getServiceState(serviceName) == OsUtils.ServiceState.RUNNING;
    }

    public static int getServiceState(String serviceName) throws IOException, InterruptedException {

        var output = runProcessWaitForOutput(String.format("%s \"%s\"", win_query_service_cmd, serviceName));
        Optional<String[]> stateRow = output.stream().filter(r -> "STATE".equals(r[0])).findFirst();
        try {
            if (stateRow.isPresent() && stateRow.get().length == 4) {
                return Integer.parseInt(stateRow.get()[2]);
            }
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }
        return OsUtils.ServiceState.UNKNOWN;
    }

    public static ArrayList<String[]> runProcessWaitForOutput(String exec) throws IOException, InterruptedException {
        var process = Runtime.getRuntime().exec(exec);
        BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
        var lines = new ArrayList<String[]>();
        String line;
        while ((line = in.readLine()) != null) {
            if (!line.trim().equals("")) {
                var cols = line.trim().split("\\s+");
                lines.add(cols);
            }
        }
        process.waitFor();
        return lines;

    }
}