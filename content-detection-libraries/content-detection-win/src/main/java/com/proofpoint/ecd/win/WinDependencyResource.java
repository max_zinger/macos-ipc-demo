package com.proofpoint.ecd.win;

import com.proofpoint.ecd.win.configuration.WinApplicationConfiguration;
import com.proofpoint.ecd.service.DependencyResource;

public class WinDependencyResource{

    public static DependencyResource getInstance(){
        return DependencyResource.getInstance();
    }
    public static WinApplicationConfiguration getConfiguration(){
        return (WinApplicationConfiguration)DependencyResource.getInstance().getConfiguration();
    }

}
