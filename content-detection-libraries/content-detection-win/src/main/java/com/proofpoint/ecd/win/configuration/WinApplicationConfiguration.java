package com.proofpoint.ecd.win.configuration;

import com.proofpoint.ecd.commons.runtime.RuntimeArgument;
import com.proofpoint.ecd.win.util.NamedPipeFactory;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;

import java.util.List;


public class WinApplicationConfiguration extends ApplicationConfiguration {

    public WinApplicationConfiguration() {
        super();
    }

    public WinApplicationConfiguration(List<String> args) {
        super(args);
        append(args);
    }

    @RuntimeArgument(arg = {"--main-pipe", "-mp"}, desc = "remote main write pipe name")
    private String mainPipe = "w-main";

    @RuntimeArgument(arg = {"--main-pipe-connect-timeout-ms", "-mpct"}, minValue = 10000, desc = "connection timeout for main pipe before application exits")
    private int mainPipeConnectTimeoutMs = 10000;

    @RuntimeArgument(arg = {"--pipe-inst-type", "-pit"}, desc = "type of pipe implementation used by the application - client,server")
    private String pipeInstanceType = NamedPipeFactory.InstanceType.CLIENT;

    @RuntimeArgument(arg = {"--check-eitc", "-citc"}, hidden = true, desc = "check if endpoint it cloud service is running")
    private boolean checkEndpointITCloudServiceRunning = false;

    public int getPipeBufferSize() {
        return pipeBufferSize;
    }

    @RuntimeArgument(arg = {"--pipe-buffer", "-pb"}, minValue = 256, desc = "pipe io buffer size - number of bytes")
    private int pipeBufferSize = com.proofpoint.ecd.ipc.platform.win.pipe.NamedPipe.DEFAULT_BUFFER_SIZE;

    public String getMutexName() {
        return mutexName;
    }

    @RuntimeArgument(arg = {"--mutex-name", "-mn"}, desc = "mutex name")
    private String mutexName = "itc-content-detection";

    public String getMainPipe() {
        return mainPipe;
    }

    public int getMainPipeConnectTimeoutMs() {
        return mainPipeConnectTimeoutMs;
    }

    public String getPipeInstanceType() {
        return pipeInstanceType;
    }

    public boolean isCheckEndpointITCloudServiceRunning() {
        return checkEndpointITCloudServiceRunning;
    }
}
