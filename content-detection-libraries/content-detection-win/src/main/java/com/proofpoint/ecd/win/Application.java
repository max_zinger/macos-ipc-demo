package com.proofpoint.ecd.win;

import com.proofpoint.ecd.commons.log.RootLoggerConfiguration;
import com.proofpoint.ecd.commons.runtime.RuntimeConfiguration;
import com.proofpoint.ecd.service.event.ApplicationEventPublisher;
import com.proofpoint.ecd.service.event.GcAppEventListener;
import com.proofpoint.ecd.win.configuration.WinApplicationConfiguration;
import com.proofpoint.ecd.win.handler.ConnectPipeContextHandler;
import com.proofpoint.ecd.win.util.NamedPipeFactory;
import com.proofpoint.ecd.win.util.mutex.Kernel32Mutex;
import com.proofpoint.ecd.win.util.WinEndpointServiceQuery;
import com.proofpoint.ecd.win.util.mutex.Mutex;
import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.IPCState;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.AbstractContextHandler;
import com.proofpoint.ecd.ipc.context.ContextActionHandler;
import com.proofpoint.ecd.ipc.context.JsonContextHandler;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.platform.win.pipe.NamedPipe;
import com.proofpoint.ecd.service.BlockingTaskWorker;
import com.proofpoint.ecd.service.DependencyResource;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;
import com.proofpoint.ecd.service.handler.ContextErrorHandler;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Application {
    private final static Logger logger = LoggerFactory.getLogger(Application.class);
    private static InterProcessCommunicable main_read_pipe;
    private static Mutex mutex;

    private static boolean isStarted = false;


    public static void start(List<String> args) throws Exception {
        start(new WinApplicationConfiguration(args));
    }

    public static void start(WinApplicationConfiguration configuration) throws Exception {

        synchronized (Application.class) {

            if (isStarted) {
                throw new RuntimeException("application already running.");
            }
            if (configuration.isCheckEndpointITCloudServiceRunning() && !WinEndpointServiceQuery.isEndpointITCloudServiceRunning()) {
                throw new RuntimeException("permission denied.");
            }
            if (Kernel32Mutex.open(configuration.getMutexName()) != null) {
                throw new RuntimeException("mutex [" + configuration.getMutexName() + "] resource taken.");
            }

            isStarted = true;
            BlockingTaskWorker.getInstance().put(new BlockingTaskWorker.BlockingTask() {
                @Override
                public BlockingTaskWorker.BlockingTaskResult exec() {
                    mutex = new Kernel32Mutex(configuration.getMutexName());
                    boolean created = mutex.create();
                    logger.debug("created mutex [{}] [{}]", mutex.getName(), created);
                    return new BlockingTaskWorker.BlockingTaskResult(created);
                }

                @Override
                public String getName() {
                    return "create mutex [" + configuration.getMutexName() + "]";
                }
            });
        }
        Runtime.getRuntime().addShutdownHook(new ApplicationCloseThread());
        DependencyResource.getInstance().configure(configuration);
        DependencyResource.getInstance().getEventPublisher().addListener(new GcAppEventListener(configuration));
        DependencyResource.getInstance().getEventPublisher().publish(ApplicationEventPublisher.Event.APP_STARTUP);
        logAppStart();

        main_read_pipe = createMainPipe();
        //main pipe connect timeout
        try {
            DependencyResource.getInstance().getRwIPCExecutorService().invokeAny(Collections.singletonList(Executors.callable(main_read_pipe)), WinDependencyResource.getConfiguration().getMainPipeConnectTimeoutMs(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException | TimeoutException | ExecutionException e) {
            if (main_read_pipe.getIPCState() == IPCState.CREATED || main_read_pipe.getIPCState() == IPCState.STARTED) {
                logger.warn("main {} connect timeout [{}] ms exceeded, application will exit.", main_read_pipe.nameToString(), WinDependencyResource.getConfiguration().getMainPipeConnectTimeoutMs());
                close();
                return;
            }
        }
        if (main_read_pipe.isClosed()) {
            close();
            return;
        }

        //scheduled idle pipe cleanup
        DependencyResource.getInstance().getScheduledExecutorService().scheduleWithFixedDelay(() -> {
                    try {
                        DependencyResource.getInstance().getIpcCollection().closeAllIdle(DependencyResource.getInstance().getConfiguration().getIdleTimeout());
                    } catch (Exception e) {
                        logger.error("an error occurred while executing scheduled idle pipe cleanup.", e);
                    }
                },
                DependencyResource.getInstance().getConfiguration().getIdleTimeout() + DependencyResource.getInstance().getConfiguration().getIdleTimeoutIntervalMs(),
                DependencyResource.getInstance().getConfiguration().getIdleTimeoutIntervalMs(),
                TimeUnit.MILLISECONDS);
    }

    public static void close() {
        if (main_read_pipe != null && !main_read_pipe.isClosed()) {
            try {
                main_read_pipe.close();
                main_read_pipe = null;

            } catch (Exception e) {
                logger.error("error", e);
            }
        }
        DependencyResource.getInstance().terminateResources();

        try {
            BlockingTaskWorker.getInstance().put(new BlockingTaskWorker.BlockingTask() {
                @Override
                public BlockingTaskWorker.BlockingTaskResult exec() {
                    boolean released = mutex.release();
                    logger.debug("mutex [{}] released [{}]", mutex.getName(), released);
                    return new BlockingTaskWorker.BlockingTaskResult(released, true);
                }

                @Override
                public String getName() {
                    return "release mutex [" + WinDependencyResource.getConfiguration().getMutexName() + "]";
                }
            });
            BlockingTaskWorker.getInstance().awaitComplete();
        } catch (InterruptedException e) {
            logger.error("error releasing mutex ", e);
        }

        isStarted = false;
        logger.info("application closed.");
    }

    private static class ApplicationCloseThread extends Thread {
        @SneakyThrows
        @Override
        public void run() {
            close();
            logger.info("application exit.");
        }
    }

    /**
     * create main pipe and assign context and action handlers
     *
     * @return
     */
    private static NamedPipe createMainPipe() {

        var mainReadPipe = NamedPipeFactory.getNamedPipe(WinDependencyResource.getConfiguration().getMainPipe(), IPCMode.READ, WinDependencyResource.getConfiguration().getPipeInstanceType());
        mainReadPipe.setConnectTimeoutMs(WinDependencyResource.getConfiguration().getMainPipeConnectTimeoutMs());
        var readHandler = new AbstractContextHandler() {
            @Override
            public boolean canHandleStateChange() {
                return true;
            }

            @Override
            public void handleStateChanged(IPCState oldState, IPCState newState) {
                if (newState == IPCState.CLOSED || newState == IPCState.ERROR_UNKNOWN) {
                    logger.info("main pipe closed.");
                    DependencyResource.getInstance().terminateResources();
                }
            }

        };
        readHandler.setIPCOperationsHandler(new JsonContextHandler());
        readHandler.setIPCMode(IPCMode.READ);
        readHandler.setErrorHandler(new ContextErrorHandler());
        readHandler.addAction("connect-read-write", new ConnectPipeContextHandler());
        readHandler.addAction("terminate-process", new ContextActionHandler() {

            @Override
            public Class<? extends IPCMessage> getRefType() {
                return IPCMessage.class;
            }

            @Override
            public void handle(InterProcessCommunicable ipc, IPCMessage msg, IPCOperationsHandler namedPipeOpHandler) {

            }

            @Override
            public void handle(InterProcessCommunicable ipc, IPCMessage msg) {
                handle(msg);
            }

            @Override
            public void handle(IPCMessage msg) {
                logger.info("terminate-process called.");
                close();
            }
        });
        mainReadPipe.setIPCOperationsHandler(readHandler);
        mainReadPipe.setBufferSize(WinDependencyResource.getConfiguration().getPipeBufferSize());
        return mainReadPipe;
    }

    private static void logAppStart() {

        RootLoggerConfiguration.setRootLogLevel("info");
        logger.info("{} [{}] started.", ApplicationConfiguration.APP_NAME, ApplicationConfiguration.APP_VERSION);
        RootLoggerConfiguration.setRootLogLevel(WinDependencyResource.getConfiguration().getLogLevel());
        if (logger.isDebugEnabled()) {
            logger.debug("application configuration [{}].", WinDependencyResource.getConfiguration());
        }
    }

    public static void printHelp() {
        System.out.printf("%s %s\r\n", ApplicationConfiguration.APP_NAME, ApplicationConfiguration.APP_VERSION);
        System.out.println("---------------------------------------------");
        RuntimeConfiguration.help(new WinApplicationConfiguration()).forEach(System.out::println);
    }
}
