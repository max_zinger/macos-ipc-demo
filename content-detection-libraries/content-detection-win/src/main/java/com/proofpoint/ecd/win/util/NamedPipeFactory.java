package com.proofpoint.ecd.win.util;

import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.platform.win.pipe.Kernel32NamedPipeClient;
import com.proofpoint.ecd.ipc.platform.win.pipe.Kernel32NamedPipeServer;
import com.proofpoint.ecd.ipc.platform.win.pipe.NamedPipe;

public class NamedPipeFactory {
    public static class InstanceType {
        public static final String SERVER = "server";
        public static final String CLIENT = "client";
    }

    public static NamedPipe getNamedPipe(String name, IPCMode mode, String type) {
        switch (type.toLowerCase()) {
            case InstanceType.CLIENT:
                return new Kernel32NamedPipeClient(name, mode);

            case InstanceType.SERVER:
            default:
                return new Kernel32NamedPipeServer(name, mode);
        }

    }

    public static String parseInstanceTypeOrDefault(String type) {
        type = Utils.isStringNullOrWhitespaces(type) ? "" : type;
        switch (type.toLowerCase()) {
            case InstanceType.SERVER:
                return InstanceType.SERVER;
            case InstanceType.CLIENT:
            default:
                return InstanceType.CLIENT;
        }
    }
}
