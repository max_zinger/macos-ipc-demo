package com.proofpoint.ecd.win.util;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.Advapi32;
import com.sun.jna.ptr.IntByReference;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class WinUtils implements OsUtils {

    @Override
    public String getUsername() {
        IntByReference len = new IntByReference(256);
        char[] buffer = new char[len.getValue()];
        Advapi32.INSTANCE.GetUserNameW(buffer, len);
        return Native.toString(buffer);

    }

    @Override
    public String getHostname() {

        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
        }

        return null;
    }

    @Override
    public boolean isEndpointITCloudServiceRunning() {
        try {
            return WinEndpointServiceQuery.isEndpointITCloudServiceRunning();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean isServiceRunning(String serviceName) {
        try {
            return WinEndpointServiceQuery.isServiceRunning(serviceName);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public int getServiceState(String serviceName) {

        try {
            return WinEndpointServiceQuery.getServiceState(serviceName);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ServiceState.UNKNOWN;
    }
}
