package com.proofpoint.ecd.win.util.mutex;

public interface Mutex {
    String getName();

    boolean create();

    boolean release();
}
