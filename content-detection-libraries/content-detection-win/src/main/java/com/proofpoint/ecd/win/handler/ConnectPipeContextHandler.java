package com.proofpoint.ecd.win.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.proofpoint.ecd.win.configuration.WinApplicationConfiguration;
import com.proofpoint.ecd.win.util.NamedPipeFactory;
import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.win.WinDependencyResource;
import com.proofpoint.ecd.ContentAnalyze;
import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.ContextActionHandler;
import com.proofpoint.ecd.ipc.context.JsonContextHandler;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.platform.win.pipe.NamedPipe;
import com.proofpoint.ecd.service.handler.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * pipe connection message handling
 */
@SuppressWarnings("rawtypes")
public class ConnectPipeContextHandler implements ContextActionHandler<ConnectPipeMessage> {
    private static final Logger logger = LoggerFactory.getLogger(ConnectPipeContextHandler.class);
    private final static ContextActionHandler error_handler = new ContextErrorHandler();

    private final ThreadPoolExecutor executors;
    private final WinApplicationConfiguration configuration;

    public ConnectPipeContextHandler() {
        this.executors = (ThreadPoolExecutor) WinDependencyResource.getInstance().getRwIPCExecutorService();
        this.configuration = WinDependencyResource.getConfiguration();
    }

    /**
     * handle pipe connect message by type
     *
     * @param msg
     */
    @Override
    public void handle(ConnectPipeMessage msg) {
        if (logger.isDebugEnabled()) {
            try {
                logger.debug(Utils.JSON_FORMATTER_INDENT_OUTPUT.writeValueAsString(msg));
            } catch (JsonProcessingException e) {
                logger.error("failed to serialize message", e);
            }
        }
        if (logger.isWarnEnabled() && executors.getActiveCount() + 1 > executors.getCorePoolSize()) {
            logger.warn("there are no available pipe threads at the moment. read/write pipes will start once resource released. active threads [{}] pool size [{}].", executors.getActiveCount(), executors.getCorePoolSize());
        } else if (logger.isDebugEnabled()) {
            logger.debug("active threads [{}] pool size [{}].", executors.getActiveCount(), executors.getCorePoolSize());
        }
        logger.debug("active pipe tuples [{}].", WinDependencyResource.getInstance().getIpcCollection().size());
        switch (msg.getData().kind) {
            case "analyze":
                createAnalyzeReadWritePipes(msg);
                return;
        }
        logger.warn("invalid pipe kind [{}].", msg.getData().kind);

    }

    /**
     * async read and write pipe connection of type analyze
     *
     * @param msg
     */
    private void createAnalyzeReadWritePipes(ConnectPipeMessage msg) {
        if (!validateConnectionData(msg)) {
            return;
        }
        synchronized (ConnectPipeContextHandler.class) {
            if (!validateConnectionData(msg)) {
                return;
            }
        }

        NamedPipe writePipe = createWritePipe(msg.getData().connection.write);
        NamedPipe readPipe = createReadPipe(msg.getData().connection.read, writePipe);
        WinDependencyResource.getInstance().getIpcCollection().add(readPipe, writePipe);

        executors.submit(writePipe);
        executors.submit(readPipe);

    }

    private boolean validateConnectionData(ConnectPipeMessage msg) {
        if (Utils.isStringNullOrWhitespaces(msg.getData().connection.read) || Utils.isStringNullOrWhitespaces(msg.getData().connection.write)) {
            logger.warn("invalid pipe connection read or write pipe names are null or empty [{}].", msg.getData().kind);
            return false;
        }
        if (WinDependencyResource.getInstance().getIpcCollection().contains(msg.getData().connection.read)) {
            logger.warn("pipe sid [{}] exists.", msg.getData().connection.read);
            return false;
        }
        if (WinDependencyResource.getInstance().getIpcCollection().contains(msg.getData().connection.write)) {
            logger.warn("pipe sid [{}] exists.", msg.getData().connection.write);
            return false;
        }
        return true;
    }

    /**
     * instantiate analyze write pipe
     *
     * @param name
     * @return
     */
    private NamedPipe createWritePipe(String name) {
        NamedPipe writePipe = NamedPipeFactory.getNamedPipe(name, IPCMode.WRITE, configuration.getPipeInstanceType());
        writePipe.setRestartOnRemoteConnectionClosed(false);
        writePipe.setIPCOperationsHandler(new JsonContextHandler().setIPCMode(IPCMode.WRITE));
        writePipe.setBufferSize(configuration.getPipeBufferSize());
        return writePipe;
    }

    /**
     * instantiate analyze read pipe
     * <p>
     * analyze message
     * dlp configuration updates
     *
     * @param name
     * @param writePipe
     * @return
     */
    private NamedPipe createReadPipe(String name, NamedPipe writePipe) {
        NamedPipe readPipe = NamedPipeFactory.getNamedPipe(name, IPCMode.READ, configuration.getPipeInstanceType());
        readPipe.setRestartOnRemoteConnectionClosed(false);
        readPipe.setBufferSize(configuration.getPipeBufferSize());
        var readHandler = new AnalyzeContextHandler(writePipe);
        readHandler.setIPCOperationsHandler(new JsonContextHandler())
                .setIPCMode(IPCMode.READ)
                .setErrorHandler(error_handler);
        readHandler.setWriter(writePipe);
        var parser = WinDependencyResource.getInstance().getParser();
        var analyzer = WinDependencyResource.getInstance().getAnalyzer();
        readHandler.addAction("analyze", new AnalyzeContextHandler.AnalyzeMessageHandler(new ContentAnalyze(analyzer, parser)));
        readHandler.addAction("update-dlp-conf", new UpdateDlpConfigurationHandler());
        readHandler.addAction("update-limitation", new UpdateLimitationsConfigurationHandler());
        readHandler.addAction("get-status", new MessageStatusQueryHandler());
        readPipe.setIPCOperationsHandler(readHandler);

        return readPipe;
    }


    @Override
    public Class<? extends IPCMessage> getRefType() {
        return ConnectPipeMessage.class;
    }

    @Override
    public void handle(InterProcessCommunicable ipc, ConnectPipeMessage msg, IPCOperationsHandler ipcOperationsHandler) {
        handle(msg);
    }

    @Override
    public void handle(InterProcessCommunicable ipc, ConnectPipeMessage msg) {
        handle(msg);
    }


}