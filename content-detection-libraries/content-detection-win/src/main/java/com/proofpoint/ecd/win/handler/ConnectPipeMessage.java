package com.proofpoint.ecd.win.handler;

import com.proofpoint.ecd.ipc.context.model.IPCMessage;

public class ConnectPipeMessage extends IPCMessage<ConnectPipeMessage.ConnectPipeData> {
    public static class ConnectPipeData {
        public String kind;
        public Connection connection = new Connection();
    }
    public static class Connection {
        public String read;
        public String write;
    }
}
