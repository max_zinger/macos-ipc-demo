package com.proofpoint.ecd.win;

import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.commons.log.RootLoggerConfiguration;
import com.proofpoint.ecd.service.model.SerializationMixins;
import com.proofpoint.ecd.win.configuration.WinApplicationConfiguration;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        try {
            if (args != null && args.length > 0 && args[0].equalsIgnoreCase("-h")) {
                Application.printHelp();
                return;
            }
            ApplicationConfiguration.setApplication(Main.class);
            var configuration = new WinApplicationConfiguration();
            configure(args, configuration);
            Application.start(configuration);
        } catch (Exception e) {
            LoggerFactory.getLogger(Main.class).error("error", e);
        }
    }

    private static void configure(String[] args, WinApplicationConfiguration configuration) {
        SerializationMixins.dlpResultSuppressSnippetReferenceAndContent(Utils.JSON_FORMATTER_INDENT_OUTPUT);

        configuration.append(Arrays.asList(args));
        RootLoggerConfiguration.configure(configuration.getLogDir(), Utils.isStringNullOrWhitespaces(WinApplicationConfiguration.APP_NAME) ? "content-detection-win" : WinApplicationConfiguration.APP_NAME);
        RootLoggerConfiguration.setRootLogLevel(configuration.getLogLevel());
    }
}
