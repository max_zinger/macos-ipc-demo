package com.proofpoint.ecd.win;

import com.fasterxml.jackson.core.type.TypeReference;
import com.proofpoint.ecd.analyze.AnalyzeConfiguration;
import com.proofpoint.ecd.analyze.model.AnalyzeResponse;
import com.proofpoint.ecd.commons.runtime.RuntimeConfiguration;
import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.ContextActionHandler;
import com.proofpoint.ecd.ipc.context.JsonContextHandler;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.context.model.MessageStatusQuery;
import com.proofpoint.ecd.ipc.platform.win.pipe.NamedPipe;
import com.proofpoint.ecd.parse.ContentParserConfiguration;
import com.proofpoint.ecd.service.DependencyResource;
import com.proofpoint.ecd.service.handler.UpdateDlpConfigurationHandler;
import com.proofpoint.ecd.service.handler.UpdateLimitationsConfigurationHandler;
import com.proofpoint.ecd.service.model.DlpConfigurationUpdate;
import com.proofpoint.ecd.service.model.UpdateLimitationsConfiguration;
import com.proofpoint.ecd.win.configuration.WinApplicationConfiguration;
import com.proofpoint.ecd.win.handler.ConnectPipeMessage;
import com.proofpoint.ecd.win.util.NamedPipeFactory;
import com.proofpoint.ecd.win.util.OsUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class ApplicationTests {

    private static final String main_pipe_prefix = "main-pipe-test";
    private static final String analyze_msg_id_ok = "result-analyze-ok";
    private static final String analyze_msg_id_error = "result-analyze-error";
    private static final String analyze_msg_update_dlp = "result-analyze-update-dlp";
    private static final String analyze_msg_update_limitation = "result-analyze-update-limitations";
    private static final String analyze_msg_get_status = "result-analyze-get-status";


    private CountDownLatch countDownLatch = new CountDownLatch(1);

    @Before
    public void before() {
        Assume.assumeTrue(OsUtils.OS.WINDOWS);
    }

    @Test
    public void simpleKernel32DefaultAppReadWriteJsonNamedPipeAnalyzeApplicationTest() {
        simpleKernel32ReadWriteNamedPipeAnalyzeApplicationTest(new ArrayList<>(), "-default-context", "-default-context-sid");

    }

    @Test
    public void simpleKernel32ServerAppReadWriteJsonNamedPipeAnalyzeApplicationTest() {
        var args = new ArrayList<String>();
        args.add(RuntimeConfiguration.getArgumentByFieldName(WinApplicationConfiguration.class, "pipeInstanceType").arg()[0]);
        args.add(NamedPipeFactory.InstanceType.SERVER);
        simpleKernel32ReadWriteNamedPipeAnalyzeApplicationTest(args, "-json-context", "-json-context-sid");

    }

    public void simpleKernel32ReadWriteNamedPipeAnalyzeApplicationTest(List<String> args, String testId, String sid) {
        String mainPipeName = main_pipe_prefix + testId;
        args.add(RuntimeConfiguration.getArgumentByFieldName(WinApplicationConfiguration.class, "mainPipe").arg()[0]);
        args.add(mainPipeName);
        args.add(RuntimeConfiguration.getArgumentByFieldName(WinApplicationConfiguration.class, "pipeBufferSize").arg()[0]);
        args.add("256");
        args.add(RuntimeConfiguration.getArgumentByFieldName(WinApplicationConfiguration.class, "logLevel").arg()[0]);
        args.add("debug");

        var configuration = new WinApplicationConfiguration(args);
        Map<String, IPCMessage> results = new HashMap<>();
        var executors = Executors.newFixedThreadPool(3);
        Future<?> app = executors.submit(() -> {
            try {
                Application.start(args);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("failed to start application.", e);
            }
        });
        Future<?> remote = executors.submit(() -> {

            NamedPipe main = null;
            NamedPipe read = null;
            NamedPipe write = null;
            String pipeInstanceType = configuration.getPipeInstanceType() == NamedPipeFactory.InstanceType.CLIENT ? NamedPipeFactory.InstanceType.SERVER : NamedPipeFactory.InstanceType.CLIENT;
            try {
                main = NamedPipeFactory.getNamedPipe(mainPipeName, IPCMode.WRITE, pipeInstanceType);
                var mainHandler = new JsonContextHandler();
                mainHandler.setIPCMode(IPCMode.WRITE);
                main.setIPCOperationsHandler(mainHandler);
                main.setBufferSize(configuration.getPipeBufferSize());
                try {
                    main.start();

                    var connectPipesMsg = TestResources.getJson(TestResources.CONNECT_PIPE_MSG_FILE_NAME, ConnectPipeMessage.class);
                    connectPipesMsg.getData().connection.read = "read" + sid;
                    connectPipesMsg.getData().connection.write = "write" + sid;
                    main.write(connectPipesMsg);

                    read = NamedPipeFactory.getNamedPipe(connectPipesMsg.getData().connection.write, IPCMode.READ, pipeInstanceType);
                    read.setBufferSize(configuration.getPipeBufferSize());
                    var readHandler = new JsonContextHandler();
                    readHandler.addDefaultAction(new ContextActionHandler<>() {

                        @Override
                        public Class<? extends IPCMessage> getRefType() {
                            return IPCMessage.class;
                        }

                        @Override
                        public void handle(InterProcessCommunicable writer, IPCMessage msg, IPCOperationsHandler ipcOperationsHandler) {

                        }

                        @Override
                        public void handle(InterProcessCommunicable writer, IPCMessage msg) {
                        }

                        @Override
                        public void handle(IPCMessage msg) {
                            results.put(msg.getContext().getTransactionId(), msg);
                            countDownLatch.countDown();
                            countDownLatch = new CountDownLatch(1);
                        }
                    });
                    readHandler.setIPCMode(IPCMode.READ);
                    read.setIPCOperationsHandler(readHandler);
                    var readFuture = executors.submit(read);


                    write = NamedPipeFactory.getNamedPipe(connectPipesMsg.getData().connection.read, IPCMode.WRITE, pipeInstanceType);
                    write.setBufferSize(configuration.getPipeBufferSize());
                    var writeHandler = new JsonContextHandler();
                    writeHandler.setIPCMode(IPCMode.WRITE);
                    write.setIPCOperationsHandler(writeHandler);
                    write.start();

                    var updateDlpMsg = DlpConfigurationUpdate.getMessage(TestResources.getContent(TestResources.ANALYZE_CONFIG_FILE_NAME), DlpConfigurationUpdate.Encoding.JSON, DlpConfigurationUpdate.Compression.NONE);
                    updateDlpMsg.getContext().setAction("update-dlp-conf");
                    updateDlpMsg.getContext().setTransactionId(analyze_msg_update_dlp);
                    write.write(updateDlpMsg);
                    countDownLatch.await(10, TimeUnit.SECONDS);

                    var updateLimitationsMsg = UpdateLimitationsConfiguration.getMessage(new ContentParserConfiguration(), new AnalyzeConfiguration());
                    updateLimitationsMsg.getContext().setAction("update-limitation");
                    updateLimitationsMsg.getContext().setTransactionId(analyze_msg_update_limitation);
                    write.write(updateLimitationsMsg);
                    countDownLatch.await(10, TimeUnit.SECONDS);

                    var analyzeMsg = TestResources.getAnalyzeMessage();
                    analyzeMsg.getData().setDlpConfig(null);
                    write.write(analyzeMsg);
                    countDownLatch.await(10, TimeUnit.SECONDS);

                    var badAnalyzeMsg = TestResources.getBadAnalyzeMessage();
                    badAnalyzeMsg.getContext().setTransactionId(analyze_msg_id_error);
                    write.write(badAnalyzeMsg);
                    countDownLatch.await(5, TimeUnit.SECONDS);

                    var statusQueryMsg = new MessageStatusQuery.Message();
                    statusQueryMsg.getContext().setAction("get-status");
                    statusQueryMsg.getData().setTransactionIdSet(new String[]{analyze_msg_update_limitation, analyze_msg_id_ok});
                    statusQueryMsg.getContext().setTransactionId(analyze_msg_get_status);
                    write.write(statusQueryMsg);
                    countDownLatch.await(5, TimeUnit.SECONDS);

                    readFuture.get(30L, TimeUnit.SECONDS);

                } catch (Exception e) {
                    throw new RuntimeException("failed to start remote pipes.", e);
                }


            } finally {
                try {
                    read.close();
                    write.close();
                    main.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        });
        for (Future<?> f : Arrays.asList(remote, app)) {
            try {
                f.get(60L, TimeUnit.SECONDS);
            } catch (Exception e) {
                throw new RuntimeException("pipe thread interrupted.", e);
            }
        }
        assertEquals("assert number of expected results.", 5, results.size());

        assertEquals("assert dlp update response data.", UpdateDlpConfigurationHandler.UPDATE_OK, results.get(analyze_msg_update_dlp).getData());
        assertEquals("assert limitation update response data.", UpdateLimitationsConfigurationHandler.UPDATE_OK, results.get(analyze_msg_update_limitation).getData());

        var expectedResultOk = TestResources.getJson(TestResources.ANALYZE_MSG_RESULT_FILE_NAME, new TypeReference<AnalyzeResponseMessageTest>() {
        });

        try {
            expectedResultOk.getData().getMetric().setPath(TestResources.getPath(TestResources.ANALYZE_1_MB_DOC_FILE_NAME));
        } catch (URISyntaxException e) {
            throw new RuntimeException("error", e);
        }

        var t1 = TestResources.removeMetricData(TestResources.JSON_MAPPER.valueToTree(expectedResultOk));
        var t2 = TestResources.removeMetricData(TestResources.JSON_MAPPER.valueToTree(results.get(analyze_msg_id_ok)));

        assertEquals("assert analyze message result.", t1.toString(), t2.toString());
        assertNull("assert server analyze rw pipes are closed.", DependencyResource.getInstance().getIpcCollection());

        var expectedResultError = results.get(analyze_msg_id_error);

        assertTrue("assert error message.",expectedResultError.getError().contains("path must be a file"));
        assertNotNull("error should generate an incident id.", expectedResultError.getContext().getIncidentId());

        var msgStatRes = (IPCMessage<ArrayList<Object>>) results.get(analyze_msg_get_status);
        assertEquals("assert number of status query response data items.", 2, msgStatRes.getData().size());

        Application.close();
        assertTrue("assert scheduler resource is terminated.", DependencyResource.getInstance().getScheduledExecutorService().isShutdown());
        assertTrue("assert pipe thread resource is terminated.", DependencyResource.getInstance().getRwIPCExecutorService().isShutdown());
        assertTrue("assert io thread resource is terminated.", DependencyResource.getInstance().getIoExecutorService().isShutdown());
    }

    public static class AnalyzeResponseMessageTest extends IPCMessage<AnalyzeResponse> {
    }


}
