package com.proofpoint.ecd.analyze.model;

public class AnalyzeRequest {

    private String path;
    private DlpConfig dlpConfig = new DlpConfig();
    private boolean extractEmbedded = true;
    private Path pathEx = new Path();

    public Path getPathEx() {
        return pathEx;
    }

    public void setPathEx(Path pathEx) {
        this.pathEx = pathEx;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isExtractEmbedded() {
        return extractEmbedded;
    }

    public void setExtractEmbedded(boolean extractEmbedded) {
        this.extractEmbedded = extractEmbedded;
    }

    public DlpConfig getDlpConfig() {
        return dlpConfig;
    }

    public void setDlpConfig(DlpConfig dlpConfig) {
        this.dlpConfig = dlpConfig;
    }

    public static class Path{
        private String source;
        private String target;

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getTarget() {
            return target;
        }

        public void setTarget(String target) {
            this.target = target;
        }
    }

}
