package com.proofpoint.ecd.analyze.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

public class FileIdentifier {

    BasicFileAttributes attributes;
    String absolutePath;
    String key;

    public FileIdentifier(String path) throws IOException {
        try{
            Path filePath = Paths.get(path);
            this.absolutePath = filePath.toAbsolutePath().toString();
            this.key = absolutePath;
            this.attributes = Files.readAttributes(filePath, BasicFileAttributes.class);
        }catch (IOException e){
            throw new NoSuchFileException("path must be a file.");
        }

    }

    public BasicFileAttributes getAttributes() {
        return attributes;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public String getKey() {
        return key;
    }

    @Override
    public String toString() {
        return "FileIdentifier{" +
                "\r\n lastModifiedTime=" + attributes.lastModifiedTime() +
                ",\r\n creationTime=" + attributes.creationTime() +
                ",\r\n lastAccessTime=" + attributes.lastAccessTime() +
                ",\r\n key='" + key + '\'' +
                "\r\n}";
    }
}
