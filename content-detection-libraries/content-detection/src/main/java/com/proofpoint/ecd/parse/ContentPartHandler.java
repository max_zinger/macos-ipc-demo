package com.proofpoint.ecd.parse;

import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.analyze.model.FileProcessing;
import com.proofpoint.dlp.api.java.ContentPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

public class ContentPartHandler implements ContentParser {
    public static final String CONTENT_PART_EXTENSION = ".cpart";

    private static final Logger logger = LoggerFactory.getLogger(ContentPartHandler.class);

    @Override
    public ContentParseResult parse(Path path, FileProcessing metric, boolean isExtractEmbedded) throws Exception {

        String filename = path.getFileName().toString();
        var start = System.currentTimeMillis();
        if (!filename.endsWith(CONTENT_PART_EXTENSION)) {
            return null;
        }
        String content = String.join(System.lineSeparator(), Files.readAllLines(path));
        long duration = System.currentTimeMillis() - start;
        logger.info("reading content part file [{}] took [{}] ms.", path.toAbsolutePath(), duration);
        metric.getParse().setDurationMs(duration);
        metric.getSize().setCharCount(content.length());
        return new ContentParseResult(content, ContentParseResult.Status.COMPLETED);
    }

    @Override
    public ContentParseResult parse(byte[] bytes, String fileId, FileProcessing metric, boolean isExtractEmbedded) throws Exception {
        var start = System.currentTimeMillis();
        if (bytes == null || bytes.length <= 0) {
            return null;
        }
        String content = new String(bytes, StandardCharsets.UTF_8);
        long duration = System.currentTimeMillis() - start;
        logger.info("reading content part file [{}] took [{}] ms.", fileId, duration);
        metric.getParse().setDurationMs(duration);
        metric.getSize().setCharCount(content.length());
        return new ContentParseResult(content, ContentParseResult.Status.COMPLETED);
    }

    @Override
    public ContentParserConfiguration getConfiguration() {
        return null;
    }

    @Override
    public void setConfiguration(ContentParserConfiguration configuration) {

    }

    public static void SaveContentPart(Map<String, ContentPart> parts, String path) throws IOException {
        if (parts == null || parts.isEmpty()) {
            logger.error("cannot save empty content part to path [{}].", path);
            return;
        }
        var content = parts.entrySet().iterator().next();
        String contentPartPath = Utils.isStringNullOrWhitespaces(path) ? content.getKey() : path + "\\" + content.getKey();
        FileWriter writer = null;
        try {
            writer = new FileWriter(new File(contentPartPath + ContentPartHandler.CONTENT_PART_EXTENSION), false);
            writer.write(content.getValue().getContent());

        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
}
