package com.proofpoint.ecd.analyze.model.metric;

public class ExtendedProcessMetric extends ProcessMetric{
    private long avgMs;
    private long minMs;
    private long maxMs;

    public long getAvgMs() {
        return avgMs;
    }

    public void setAvgMs(long avgMs) {
        this.avgMs = avgMs;
    }

    public long getMinMs() {
        return minMs;
    }

    public void setMinMs(long minMs) {
        this.minMs = minMs;
    }

    public long getMaxMs() {
        return maxMs;
    }

    public void setMaxMs(long maxMs) {
        this.maxMs = maxMs;
    }
}
