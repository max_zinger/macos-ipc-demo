package com.proofpoint.ecd.analyze.model.metric;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.proofpoint.ecd.analyze.model.AnalyzeResponse;
import com.proofpoint.ecd.analyze.model.FileProcessing;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AnalyzeMetric{

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Date startedAt = new Date();
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Date endedAt;
    private int fileCount;
    private final ExtendedProcessMetric analyzer = new ExtendedProcessMetric();
    private final ExtendedProcessMetric parser = new ExtendedProcessMetric();
    private final ExtendedProcessMetric general = new ExtendedProcessMetric();
    private final List<AnalyzeResponse> fileMetrics = new ArrayList<>();
    private FileProcessing min, max;


    public Date getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Date startedAt) {
        this.startedAt = startedAt;
    }

    public Date getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(Date endedAt) {
        this.endedAt = endedAt;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public ExtendedProcessMetric getAnalyzer() {
        return analyzer;
    }

    public ExtendedProcessMetric getParser() {
        return parser;
    }

    public ExtendedProcessMetric getGeneral() {
        return general;
    }

    public List<AnalyzeResponse> getFileMetrics() {
        return fileMetrics;
    }

    private void setProcessMetric(ExtendedProcessMetric metric, long duration) {
        metric.setDurationMs(metric.getDurationMs() + duration);
        if (metric.getMaxMs() < duration) {
            metric.setMaxMs(duration);
        }
        if (metric.getMinMs() <= 0 || metric.getMinMs() > duration) {
            metric.setMinMs(duration);
        }
        metric.setAvgMs(metric.getDurationMs() / fileMetrics.size());
    }

    public void add(AnalyzeResponse analyzeResponse) {
        fileMetrics.add(analyzeResponse);
        if (max == null || max.getDurationMs() < analyzeResponse.getMetric().getDurationMs()) {
            max = analyzeResponse.getMetric();
        }
        if (min == null || min.getDurationMs() > analyzeResponse.getMetric().getDurationMs()) {
            min = analyzeResponse.getMetric();
        }
        setProcessMetric(analyzer, analyzeResponse.getMetric().getAnalyze().getDurationMs());
        setProcessMetric(parser, analyzeResponse.getMetric().getParse().getDurationMs());
        setProcessMetric(general, analyzeResponse.getMetric().getDurationMs());
        setFileCount(getFileMetrics().size());

    }

    public FileProcessing getMin() {
        return min;
    }

    public FileProcessing getMax() {
        return max;
    }
}
