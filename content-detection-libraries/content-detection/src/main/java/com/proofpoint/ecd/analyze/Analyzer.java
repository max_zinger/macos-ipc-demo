package com.proofpoint.ecd.analyze;

import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.analyze.model.DlpCustomerConfiguration;
import com.proofpoint.ecd.analyze.model.FileProcessing;
import com.proofpoint.dlp.api.java.*;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * wrapper class for the dlp engine
 */
public class Analyzer {
    static {
        System.setProperty("nashorn.args=", "--no-deprecation-warning");
        System.setErr(new PrintStream(new NullOutputStream()));
    }

    public static final String DEFAULT_DLP_CONF_PATH = "./dlp-config.json";
    public static final String DEFAULT_DLP_CONF_CLASS_PATH = "dlp-config.json";
    private static final Logger logger = LoggerFactory.getLogger(Analyzer.class);
    private DlpService service;
    private DlpConfigurationLoader dlpConfigurationLoader;
    private AnalyzeConfiguration analyzeConfiguration;
    private DlpCustomerConfiguration dlpCustomerConfiguration;

    public Analyzer() throws IOException {
        this(new AnalyzeConfiguration(), Paths.get(DEFAULT_DLP_CONF_PATH));
    }

    /**
     * configure with raw dlp config json
     *
     * @param dlpConfigJson
     */
    public Analyzer(AnalyzeConfiguration analyzeConfiguration, String dlpConfigJson) {
        updateConfiguration(analyzeConfiguration, dlpConfigJson);
    }

    /**
     * configure with dlp config json file path
     *
     * @param dlpConfigPath
     * @throws IOException
     */
    public Analyzer(AnalyzeConfiguration analyzeConfiguration, Path dlpConfigPath) throws IOException {
        updateConfiguration(analyzeConfiguration, dlpConfigPath);
    }

    public Analyzer(AnalyzeConfiguration analyzeConfiguration, DlpConfigurationLoader dlpConfigurationLoader) {
        updateConfiguration(analyzeConfiguration, dlpConfigurationLoader, null);
    }

    public void updateConfiguration(AnalyzeConfiguration analyzeConfiguration, Path dlpConfigPath) throws IOException {
        updateConfiguration(analyzeConfiguration, Files.readString(dlpConfigPath), null);
    }

    public void updateConfiguration(AnalyzeConfiguration analyzeConfiguration, String dlpConfigJson, DlpCustomerConfiguration dlpCustomerConfiguration) {
        updateConfiguration(analyzeConfiguration, new SimpleDlpConfigurationLoader(dlpConfigJson), dlpCustomerConfiguration);
    }

    public void updateConfiguration(AnalyzeConfiguration analyzeConfiguration, String dlpConfigJson) {
        updateConfiguration(analyzeConfiguration, new SimpleDlpConfigurationLoader(dlpConfigJson), null);
    }

    public void updateConfiguration(AnalyzeConfiguration analyzeConfiguration, byte[] compressedDlpConfiguration, DlpCustomerConfiguration dlpCustomerConfiguration) throws Exception {
        updateConfiguration(analyzeConfiguration, decompress(compressedDlpConfiguration), dlpCustomerConfiguration);
    }

    public void updateConfiguration(AnalyzeConfiguration analyzeConfiguration, byte[] compressedDlpConfiguration) throws Exception {
        updateConfiguration(analyzeConfiguration, decompress(compressedDlpConfiguration), null);
    }

    public void updateConfiguration(AnalyzeConfiguration analyzeConfiguration) {
        updateConfiguration(analyzeConfiguration, dlpConfigurationLoader, null);
    }

    public void updateConfiguration(AnalyzeConfiguration analyzeConfiguration, DlpConfigurationLoader dlpConfigurationLoader, DlpCustomerConfiguration dlpCustomerConfiguration) {

        var dlpConfiguration = dlpConfigurationLoader.loadConfiguration();

        var tCustomerConfiguration = new DlpCustomerConfiguration();
        tCustomerConfiguration.setCustomer(dlpConfiguration.getCustomerConfigurations().keySet().stream().findFirst().get());
        tCustomerConfiguration.setDetectorSet(dlpConfiguration.getCustomerConfigurations().get(tCustomerConfiguration.getCustomer()).getDetectorSets().keySet());

        if (dlpCustomerConfiguration != null && !dlpCustomerConfiguration.isEmpty()) {
            if (!Utils.isStringNullOrWhitespaces(dlpCustomerConfiguration.getCustomer()) && dlpConfiguration.getCustomerConfigurations().containsKey(tCustomerConfiguration.getCustomer())) {
                tCustomerConfiguration.setCustomer(dlpCustomerConfiguration.getCustomer());
            }
            if (dlpCustomerConfiguration.getDetectorSet() != null && !dlpCustomerConfiguration.getDetectorSet().isEmpty()) {
                tCustomerConfiguration.setDetectorSet(dlpCustomerConfiguration.getDetectorSet());
            }
        }
        var config = ConfigFactory.parseResources("application.properties");
        config = tCustomerConfiguration.append(config);
        if (analyzeConfiguration != null) {
            config = analyzeConfiguration.append(config);
        }
        var dlpConfigurationValidationMetric = new DlpConfigurationValidationMetric();
        var tDlpService = new DlpService(config, dlpConfigurationLoader, dlpConfigurationValidationMetric);

        if (!dlpConfigurationValidationMetric.isValid()) {
            throw new RuntimeException(new BadDlpConfigurationException(dlpConfigurationValidationMetric, String.format("configuration is invalid. %s", dlpConfigurationValidationMetric)));
        }
        this.dlpCustomerConfiguration = dlpCustomerConfiguration;
        this.analyzeConfiguration = analyzeConfiguration;
        this.dlpConfigurationLoader = dlpConfigurationLoader;
        service = tDlpService;

    }

    private static SimpleDlpConfigurationLoader decompress(byte[] compressedDlpConfiguration) throws IOException {
        if (compressedDlpConfiguration == null) {
            throw new NullPointerException();
        }
        SimpleDlpConfigurationLoader loader;
        try (InputStream stream = new ByteArrayInputStream(compressedDlpConfiguration)) {
            loader = new SimpleDlpConfigurationLoader(com.proofpoint.dlp.api.Compression.decompress(stream));

        }
        return loader;
    }

    /**
     * execute analyze process for contentPartMap with specific dlp customer config
     *
     * @param contentPartMap
     * @param fileId
     * @param metric
     * @return
     */
    public DlpResult analyze(Map<String, ContentPart> contentPartMap, String fileId, FileProcessing metric) throws ExecutionException, InterruptedException, TimeoutException {
        var start = System.currentTimeMillis();
        Future<DlpResult> future = service.analyze(contentPartMap)
                .whenComplete((result, exception) -> {
                    var duration = System.currentTimeMillis() - start;
                    logger.info("analyzing [{}] took [{}] ms", fileId, duration);
                    metric.getAnalyze().setDurationMs(duration);
                });
        try {
            return future.get(analyzeConfiguration.getTimeoutMs(), TimeUnit.MILLISECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            logger.error("analyze timed out [" + analyzeConfiguration.getTimeoutMs() + " ms] for [" + fileId + "]");
            future.cancel(true);

            throw e;
        }

    }

    public DlpResult analyze(Map<String, ContentPart> contentPartMap, Path path, FileProcessing metric) throws ExecutionException, InterruptedException, TimeoutException {
        return analyze(contentPartMap, path.toAbsolutePath().toString(), metric);

    }

    /**
     * check if costumer configuration key exists in current dlp configuration
     *
     * @param costumer_configuration
     * @return
     */
    public boolean hasCustomerConfiguration(String costumer_configuration) {
        return dlpConfigurationLoader.loadConfiguration().getCustomerConfigurations().containsKey(costumer_configuration);
    }

    public AnalyzeConfiguration getAnalyzeConfiguration() {
        return analyzeConfiguration;
    }

}