package com.proofpoint.ecd.analyze.model.metric;

public class SizeMetric {
    private long bytes;
    private long charCount;

    public long getBytes() {
        return bytes;
    }

    public void setBytes(long bytes) {
        this.bytes = bytes;
    }

    public long getCharCount() {
        return charCount;
    }

    public void setCharCount(long charCount) {
        this.charCount = charCount;
    }
}
