package com.proofpoint.ecd.parse;

import com.proofpoint.ecd.analyze.model.FileProcessing;

import java.nio.file.Path;


public interface ContentParser {
    ContentParseResult parse(Path path, FileProcessing metric, boolean isExtractEmbedded) throws Exception;

    ContentParseResult parse(byte[] bytes, String fileId, FileProcessing metric, boolean isExtractEmbedded) throws Exception;

    ContentParserConfiguration getConfiguration();

    void setConfiguration(ContentParserConfiguration configuration);


}
