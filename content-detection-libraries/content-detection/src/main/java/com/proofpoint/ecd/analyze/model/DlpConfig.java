package com.proofpoint.ecd.analyze.model;

public class DlpConfig {
    private String path;
    private DlpCustomerConfiguration dlpCustomerConfiguration;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public DlpCustomerConfiguration getDlpCustomerConfiguration() {
        return dlpCustomerConfiguration;
    }

    public void setDlpCustomerConfiguration(DlpCustomerConfiguration dlpCustomerConfiguration) {
        this.dlpCustomerConfiguration = dlpCustomerConfiguration;
    }
}
