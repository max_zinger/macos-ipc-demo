package com.proofpoint.ecd.analyze;

import com.proofpoint.dlp.api.java.MetricService;
import com.proofpoint.dlp.api.java.MetricTags;
import com.proofpoint.dlp.api.java.Timer;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DlpConfigurationValidationMetric implements MetricService {
    public static class Increment {
        String name;
        long times;
        MetricTags tags;

        Increment(String reason, long times, MetricTags tags) {
            this.name = reason;
            this.times = times;
            this.tags = tags;
        }

        public String getName() {
            return name;
        }

        public long getTimes() {
            return times;
        }

        public MetricTags getTags() {
            return tags;
        }
    }

    private List<Increment> increments = new ArrayList<>();

    public List<Increment> getIncrements() {
        return increments;
    }

    public DlpConfigurationValidationMetric() {
    }

    public Timer startTimer(String var1) {
        return () -> {
        };
    }

    public void elapsedTime(String var1, Duration var2) {
    }

    public void gauge(String var1, long var2, MetricTags var4) {
    }

    public void increment(String var1, long var2, MetricTags var4) {

        increments.add(new Increment(var1, var2, var4));
    }

    public void stop() {
    }

    public boolean isValid() {

        return increments.isEmpty();
    }

    public String toString() {
        return String.format("errors : [%s]", String.join(", ", increments.stream().map(i -> i.getName()).collect(Collectors.toList())));
    }
}
