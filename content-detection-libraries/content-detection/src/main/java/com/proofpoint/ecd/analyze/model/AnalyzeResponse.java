package com.proofpoint.ecd.analyze.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.proofpoint.ecd.parse.ContentParseResult;

public class AnalyzeResponse{


    private DlpResult dlpResult;
    private FileProcessing metric = new FileProcessing();
    private ContentParseResult contentParseResult;
    public DlpResult getDlpResult() {
        return dlpResult;
    }

    public void setDlpResult(DlpResult dlpResult) {
        this.dlpResult = dlpResult;
    }

    public FileProcessing getMetric() {
        return metric;
    }

    public void setMetric(FileProcessing metric) {
        this.metric = metric;
    }

    @JsonIgnore
    public ContentParseResult getContentParseResult() {
        return contentParseResult;
    }

    public void setContentParseResult(ContentParseResult contentParseResult) {
        this.contentParseResult = contentParseResult;
    }
}
