package com.proofpoint.ecd.analyze.model.metric;

public class ProcessMetric {
    private long durationMs;

    public long getDurationMs() {
        return durationMs;
    }

    public void setDurationMs(long durationMs) {
        this.durationMs = durationMs;
    }
}
