package com.proofpoint.ecd.parse;


import com.proofpoint.ecd.commons.Utils;

import java.util.List;

public class ContentParserConfiguration {
    public static final int FILE_SIZE_MIN = 1;
    public static final int TIMEOUT_MIN = 0;

    private int maxFileSizeMB = 150;
    private long timeoutMs = 20000;
    private int writeLimit = 100000000;

    private boolean allowPartialExtraction = true;

    public boolean isAllowPartialExtraction() {
        return allowPartialExtraction;
    }

    public void setAllowPartialExtraction(boolean allowPartialExtraction) {
        this.allowPartialExtraction = allowPartialExtraction;
    }

    private List<String> allowedMediaTypes;

    public void apply() {

    }

    public int getMaxFileSizeMB() {
        return maxFileSizeMB;
    }

    public void setMaxFileSizeMB(int maxFileSizeMB) {
        Utils.validateMinValue(maxFileSizeMB, FILE_SIZE_MIN, "maxFileSizeMB");
        this.maxFileSizeMB = maxFileSizeMB;
    }

    public long getTimeoutMs() {
        return timeoutMs;
    }

    public void setTimeoutMs(int timeoutMs) {
        Utils.validateMinValue(timeoutMs, TIMEOUT_MIN, "timeoutMs");
        this.timeoutMs = timeoutMs;
    }

    public int getWriteLimit() {
        return writeLimit;
    }

    public void setWriteLimit(int writeLimit) {
        this.writeLimit = writeLimit < 0 ? -1 : writeLimit;
    }


    public long getMaxFileSizeBytes() {
        return maxFileSizeMB * 1000000l;
    }

    public List<String> getAllowedMediaTypes() {
        return allowedMediaTypes;
    }
}
