package com.proofpoint.ecd.analyze;

public class BadDlpConfigurationException extends Exception {
    private DlpConfigurationValidationMetric dlpConfigurationValidationMetric;

    public BadDlpConfigurationException(DlpConfigurationValidationMetric dlpConfigurationValidationMetric, String message) {
        super(message);
        this.dlpConfigurationValidationMetric = dlpConfigurationValidationMetric;

    }

    public DlpConfigurationValidationMetric getDlpConfigurationValidationMetric() {
        return dlpConfigurationValidationMetric;
    }
}
