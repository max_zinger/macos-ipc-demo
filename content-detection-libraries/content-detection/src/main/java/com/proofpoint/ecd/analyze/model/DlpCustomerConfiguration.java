package com.proofpoint.ecd.analyze.model;

import com.proofpoint.ecd.commons.Utils;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValueFactory;

import java.time.Duration;
import java.util.Set;

public class DlpCustomerConfiguration {
    private String customer;
    private Set<String> detectorSet;

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Set<String> getDetectorSet() {
        return detectorSet;
    }

    public void setDetectorSet(Set<String> detectorSet) {
        this.detectorSet = detectorSet;
    }

    public boolean isEmpty() {
        return Utils.isStringNullOrWhitespaces(customer)
                && (detectorSet == null || detectorSet.isEmpty());
    }

    public Config toConfig() {
        return append(ConfigFactory.empty());
    }

    public Config append(Config config) {
        return config.withValue("dlp.customer-id", ConfigValueFactory.fromAnyRef(customer))
                .withValue("dlp.detector-set-ids", ConfigValueFactory.fromIterable(detectorSet));

    }

}
