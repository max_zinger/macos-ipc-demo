package com.proofpoint.ecd;

import com.proofpoint.ecd.analyze.Analyzer;
import com.proofpoint.ecd.analyze.model.AnalyzeRequest;
import com.proofpoint.ecd.analyze.model.AnalyzeResponse;
import com.proofpoint.ecd.analyze.model.DlpResult;
import com.proofpoint.dlp.api.java.ContentPart;
import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.parse.ContentParser;

import java.io.File;
import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Map;

/**
 * integrates parse and analyze processes with formatted results
 */
public class ContentAnalyze {
    protected Analyzer analyzer;
    protected ContentParser parser;

    public ContentAnalyze(Analyzer analyzer, ContentParser parser) {
        this.analyzer = analyzer;
        this.parser = parser;
    }

    /**
     * reads file from the file system and analyzes its content
     *
     * @param analyzeRequest
     * @return
     * @throws Exception
     */
    public AnalyzeResponse analyze(AnalyzeRequest analyzeRequest) throws Exception {

        return analyzeFile(getAnalyzer(analyzeRequest), parser, analyzeRequest);


    }

    /**
     * returns current analyzer or a default analyzer based on provided dlp configuration
     *
     * @param analyzeRequest
     * @return
     * @throws IOException
     */
    protected Analyzer getAnalyzer(AnalyzeRequest analyzeRequest) throws IOException {
        Analyzer mAnalyzer = analyzer;
        if (!Utils.isStringNullOrWhitespaces(analyzeRequest.getDlpConfig().getPath())) {
            mAnalyzer = new Analyzer(analyzer.getAnalyzeConfiguration(), Paths.get(analyzeRequest.getDlpConfig().getPath()));
        }
        return mAnalyzer;
    }

    /**
     * parse and analyze file
     *
     * @param mAnalyzer
     * @param parser
     * @param analyzeRequest
     * @return
     * @throws Exception
     */
    protected AnalyzeResponse analyzeFile(Analyzer mAnalyzer, ContentParser parser, AnalyzeRequest analyzeRequest) throws Exception {

//        if (!mAnalyzer.hasCustomerConfiguration(analyzeRequest.getDlpConfig().getDlpCustomerConfiguration().getCustomer())) {
//            throw new IllegalArgumentException("invalid customer configuration.");
//        }
        File file = new File(analyzeRequest.getPath());
        if (!file.isFile()) {
            throw new NoSuchFileException("[" + analyzeRequest.getPath() + "] path must be a file.");
        }
        var res = new AnalyzeResponse();
        res.getMetric().setPath(file.getAbsolutePath());
        res.getMetric().getSize().setBytes(file.length());
        Path path = Paths.get(file.getAbsolutePath());
        var contentParseResult = parser.parse(path, res.getMetric(), analyzeRequest.isExtractEmbedded());
        if (contentParseResult != null) {
            var map = Map.of(file.getName(), new ContentPart(contentParseResult.getContent()));
            var dlpRes = mAnalyzer.analyze(map, path, res.getMetric());
            res.setDlpResult(DlpResult.fromApiDlpResult(dlpRes));
            res.setContentParseResult(contentParseResult);

        } else {

            res.setDlpResult(DlpResult.empty());
        }
        res.getMetric().setEndedAt(new Date());
        return res;
    }
}
