package com.proofpoint.ecd.analyze.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.proofpoint.ecd.analyze.model.metric.ProcessMetric;
import com.proofpoint.ecd.analyze.model.metric.SizeMetric;

import java.util.Date;

public class FileProcessing {
    private String path;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Date startedAt = new Date();
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Date endedAt;
    private ProcessMetric parse = new ProcessMetric();
    private ProcessMetric analyze = new ProcessMetric();
    private SizeMetric size = new SizeMetric();
    private boolean isContentCached = false;

    public boolean isContentCached() {
        return isContentCached;
    }

    public void setContentCached(boolean contentCached) {
        isContentCached = contentCached;
    }

    public SizeMetric getSize() {
        return size;
    }

    public void setSize(SizeMetric size) {
        this.size = size;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Date startedAt) {
        this.startedAt = startedAt;
    }

    public Date getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(Date endedAt) {
        this.endedAt = endedAt;
    }

    public ProcessMetric getParse() {
        return parse;
    }

    public void setParse(ProcessMetric parse) {
        this.parse = parse;
    }

    public ProcessMetric getAnalyze() {
        return analyze;
    }

    public void setAnalyze(ProcessMetric analyze) {
        this.analyze = analyze;
    }


    public long getDurationMs() {
        return parse.getDurationMs() + analyze.getDurationMs();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
