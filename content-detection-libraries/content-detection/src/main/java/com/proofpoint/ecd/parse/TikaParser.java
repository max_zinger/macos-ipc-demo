package com.proofpoint.ecd.parse;

import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.analyze.model.FileProcessing;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.EmptyParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.*;

public class TikaParser implements ContentParser {
    private static final String tika_write_limit_reached_ex_name = "WriteLimitReachedException";

    private AutoDetectParser parser;
    private static final Logger logger = LoggerFactory.getLogger(TikaParser.class);
    private static final String tika_md_char_count_key = "Character-Count-With-Spaces";
    private ContentParserConfiguration configuration;

    public ContentParserConfiguration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(ContentParserConfiguration configuration) {
        this.configuration = configuration;
        if (configuration != null) {
            this.configuration.apply();
        }

    }

    public TikaParser(ContentParserConfiguration configuration) {
        this.configuration = configuration;
        // load tika configuration
        TikaConfig tikaConfig = null;
        try {
            tikaConfig = new TikaConfig(this.getClass().getClassLoader()
                    .getResourceAsStream("tika-config.xml"));
        } catch (TikaException | IOException | SAXException e) {
            logger.error("Error while loading tika-config", e);
            throw new RuntimeException("Error while loading tika-config", e);
        }
        parser = new AutoDetectParser(tikaConfig);
    }

    public TikaParser() {
        this(new ContentParserConfiguration());
    }

    @Override
    public ContentParseResult parse(Path path, FileProcessing metric, boolean isExtractEmbedded) throws Exception {
        return parseFromPath(path, metric, isExtractEmbedded);
    }

    @Override
    public ContentParseResult parse(byte[] bytes, String fileId, FileProcessing metric, boolean isExtractEmbedded) throws Exception {
        return parseFromBytes(bytes, fileId, metric, isExtractEmbedded);
    }

    private ContentParseResult parseFromPath(Path path, FileProcessing metric, boolean isExtractEmbedded) throws Exception {

        var start = System.currentTimeMillis();
        long fileSize = Files.size(path);
        if (fileSize == 0) {
            logger.debug("[{}] has no content.", path.getFileName().toString());
            return null;
        }
        if (fileSize > configuration.getMaxFileSizeBytes()) {
            throw new RuntimeException("[" + path.getFileName().toString() + "] max file size [" + fileSize + "] exceeded [" + configuration.getWriteLimit() + "]");
        }
        try (InputStream stream = new BufferedInputStream(Files.newInputStream(path))) {

            ContentParseResult result;
            if (configuration.getTimeoutMs() > 0) {
                result = parseFromStreamWithTimeout(stream, path.getFileName().toString(), isExtractEmbedded);
            } else {
                result = parseFromStream(stream, path.getFileName().toString(), isExtractEmbedded);
            }
            long duration = System.currentTimeMillis() - start;
            logger.info("parse result [{}] : content length [{}]. took [{}] ms. status [{}].", path.toAbsolutePath(), result.getContent().length(), duration, result.getStatus());
            metric.getParse().setDurationMs(duration);
            metric.getSize().setCharCount(result.getContent().length());

            return result;
        } catch (Exception e) {
            logger.error(path.toString(), e);
            throw e;
        }
    }

    private ContentParseResult parseFromBytes(byte[] bytes, String id, FileProcessing metric, boolean isExtractEmbedded) throws Exception {
        var start = System.currentTimeMillis();
        if (bytes == null || bytes.length == 0) {
            logger.debug("[{}] has no content.", id);
            return null;
        }
        if (bytes.length > configuration.getMaxFileSizeBytes()) {
            throw new RuntimeException("max file size [" + bytes.length + "] exceeded [" + configuration.getWriteLimit() + "]");
        }
        try (InputStream stream = new ByteArrayInputStream(bytes)) {
            ContentParseResult result;
            if (configuration.getTimeoutMs() > 0) {
                result = parseFromStreamWithTimeout(stream, id, isExtractEmbedded);
            } else {
                result = parseFromStream(stream, id, isExtractEmbedded);
            }
            long duration = System.currentTimeMillis() - start;
            logger.info("parse result [{}] : content length [{}]. took [{}] ms. status [{}].", id, result.getContent().length(), duration, result.getStatus());
            metric.getParse().setDurationMs(duration);
            metric.getSize().setCharCount(result.getContent().length());

            return result;
        }
    }

    private ContentParseResult parseFromStreamWithTimeout(InputStream stream, String id, boolean isExtractEmbedded) throws Exception {
        Metadata metadata = new Metadata();
        var mediaType = parser.getDetector().detect(stream, metadata);
        if (!isMediaTypeAllowed(mediaType)) {
            throw new RuntimeException("configuration excludes media type [" + mediaType + "]");

        }
        BodyContentHandler handler = new BodyContentHandler(configuration.getWriteLimit());
        ParseContext parseContext = new ParseContext();

        if (!isExtractEmbedded) {
            parseContext.set(Parser.class, new EmptyParser());
        }
        var result = new ContentParseResult();
        result.setId(id);
        var future = Executors.newSingleThreadExecutor().submit(() -> {
            try {
                parser.parse(stream, handler, metadata, parseContext);
            } catch (SAXException | IOException | TikaException e) {
                if (!configuration.isAllowPartialExtraction() || !e.getClass().getName().contains(tika_write_limit_reached_ex_name)) {
                    return e;
                }
                result.setStatus(ContentParseResult.Status.PARTIAL_WRITE_LIMIT);
                logger.warn("partial text extracted for [{}] due to limitation - max content size [{}].", id, configuration.getWriteLimit());

            }
            return null;
        });
        Exception ex = null;
        try {
            ex = future.get(configuration.getTimeoutMs(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            future.cancel(true);
            String handlerStr = handler.toString();
            if (!configuration.isAllowPartialExtraction() || Utils.isStringNullOrWhitespaces(handlerStr)) {
                result.setStatus(ContentParseResult.Status.TIMEOUT);
                logger.error("no text extracted for [{}] due to limitation - timeout [{} ms].", id, configuration.getTimeoutMs());
                throw e;

            }
            result.setStatus(ContentParseResult.Status.PARTIAL_TIMEOUT);
            logger.warn("partial text extracted [{}] for [{}] due to limitation - timeout [{} ms].", handlerStr.length(), id, configuration.getTimeoutMs());


        }
        if (ex != null) {
            throw ex;
        }
        result.setContent(handler.toString());
        return result;
    }

    private ContentParseResult parseFromStream(InputStream stream, String id, boolean isExtractEmbedded) throws IOException, TikaException, SAXException {
        Metadata metadata = new Metadata();
        var mediaType = parser.getDetector().detect(stream, metadata);
        if (!isMediaTypeAllowed(mediaType)) {
            throw new RuntimeException("configuration excludes media type [" + mediaType + "]");

        }
        BodyContentHandler handler = new BodyContentHandler(configuration.getWriteLimit());
        ParseContext parseContext = new ParseContext();

        if (!isExtractEmbedded) {
            parseContext.set(Parser.class, new EmptyParser());
        }
        var result = new ContentParseResult();
        result.setId(id);
        try {
            parser.parse(stream, handler, metadata, parseContext);
        } catch (SAXException e) {
            if (!e.getClass().getName().contains(tika_write_limit_reached_ex_name)) {
                throw e;
            }
            result.setStatus(ContentParseResult.Status.PARTIAL_WRITE_LIMIT);
            logger.warn("partial text extracted for [{}] due to limitation - max content size [{}].", id, configuration.getWriteLimit());

        }
        result.setContent(handler.toString());
        return result;
    }

    private ContentParseResult parseWithTimeout(byte[] bytes, String fileId, FileProcessing metric,
                                                boolean isExtractEmbedded, long timeout) throws Exception {
        var future = Executors.newSingleThreadExecutor().submit(() -> parseFromBytes(bytes, fileId, metric, isExtractEmbedded));
        try {
            return future.get(timeout, TimeUnit.MILLISECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            logger.error("parsing timed out [" + timeout + " ms] for [" + fileId + "]");
            future.cancel(true);
            throw e;
        }
    }

    private ContentParseResult parseWithTimeout(Path path, FileProcessing metric, boolean isExtractEmbedded,
                                                long timeout) throws Exception {
        var future = Executors.newSingleThreadExecutor().submit(() -> parseFromPath(path, metric, isExtractEmbedded));
        try {
            return future.get(timeout, TimeUnit.MILLISECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            logger.error("parsing timed out [" + timeout + " ms] for [" + path.toString() + "]");
            future.cancel(true);
            throw e;
        }

    }

    private boolean isMediaTypeAllowed(MediaType mediaType) {
        if (configuration.getAllowedMediaTypes() != null && !configuration.getAllowedMediaTypes().isEmpty()) {
            return configuration.getAllowedMediaTypes().contains(mediaType.getType());
        }
        return true;
    }
}
