package com.proofpoint.ecd.parse;

public class ContentParseResult {
    public static class Status {
        public static final String COMPLETED = "parse:ok:completed";
        public static final String PARTIAL_WRITE_LIMIT = "parse:partial:write-limit";
        public static final String ERROR = "parse:error";
        public static final String PARTIAL_TIMEOUT = "parse:partial:timeout";
        public static final String TIMEOUT = "parse:no-data:timeout";


        private Status() {
        }
    }

    private String id;
    private String content;
    private String status = Status.COMPLETED;

    public ContentParseResult() {
    }

    public ContentParseResult(String content, String status) {
        this.content = content;
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
