package com.proofpoint.ecd.analyze.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DlpResult {
    private List<Detector> detectors;
    private List<Matches> matches;
    private List<ContentPartMetadata> contentParts;
    private Map<String, SnippetList> snippets;

    public static DlpResult empty(){
        return new DlpResult();
    }

    public List<Detector> getDetectors() {
        return detectors;
    }

    public void setDetectors(List<Detector> detectors) {
        this.detectors = detectors;
    }

    public List<Matches> getMatches() {
        return matches;
    }

    public void setMatches(List<Matches> matches) {
        this.matches = matches;
    }

    public List<ContentPartMetadata> getContentParts() {
        return contentParts;
    }

    public void setContentParts(List<ContentPartMetadata> contentParts) {
        this.contentParts = contentParts;
    }

    public Map<String, SnippetList> getSnippets() {
        return snippets;
    }

    public void setSnippets(Map<String, SnippetList> snippets) {
        this.snippets = snippets;
    }

    public static class SnippetReferenceAndContent {
        private int snippetOffset;
        private String content;
        private int offset;
        private int length;

        public int getSnippetOffset() {
            return snippetOffset;
        }

        public void setSnippetOffset(int snippetOffset) {
            this.snippetOffset = snippetOffset;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public int getOffset() {
            return offset;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }

        public int getLength() {
            return length;
        }

        public void setLength(int length) {
            this.length = length;
        }
    }

    public static class SnippetList {
        List<SnippetReferenceAndContent> referencesAndContent = new ArrayList<>();

        public List<SnippetReferenceAndContent> getReferencesAndContent() {
            return referencesAndContent;
        }

        public void setReferencesAndContent(List<SnippetReferenceAndContent> referencesAndContent) {
            this.referencesAndContent = referencesAndContent;
        }
    }

    public static class ContentPartMetadata {
        private String contentPartId;
        private String contentPartType;

        public String getContentPartId() {
            return contentPartId;
        }

        public void setContentPartId(String contentPartId) {
            this.contentPartId = contentPartId;
        }

        public String getContentPartType() {
            return contentPartType;
        }

        public void setContentPartType(String contentPartType) {
            this.contentPartType = contentPartType;
        }
    }

    public static class Matches {
        private String matcherId;
        private String matcherName;
        private String matchType;
        private int matchCount;
        private Map<String, MatchesSnippetList> matchSnippets;

        public String getMatcherId() {
            return matcherId;
        }

        public void setMatcherId(String matcherId) {
            this.matcherId = matcherId;
        }

        public String getMatcherName() {
            return matcherName;
        }

        public void setMatcherName(String matcherName) {
            this.matcherName = matcherName;
        }

        public String getMatchType() {
            return matchType;
        }

        public void setMatchType(String matchType) {
            this.matchType = matchType;
        }

        public int getMatchCount() {
            return matchCount;
        }

        public void setMatchCount(int matchCount) {
            this.matchCount = matchCount;
        }

        public Map<String, MatchesSnippetList> getMatchSnippets() {
            return matchSnippets;
        }

        public void setMatchSnippets(Map<String, MatchesSnippetList> matchSnippets) {
            this.matchSnippets = matchSnippets;
        }
    }

    public static class MatchesSnippetList {
        List<Integer> referencesToSnippets;

        public List<Integer> getReferencesToSnippets() {
            return referencesToSnippets;
        }

        public void setReferencesToSnippets(List<Integer> referencesToSnippets) {
            this.referencesToSnippets = referencesToSnippets;
        }
    }

    public static class Detector {
        private String detectorId;
        private String detectorName;
        private List<String> referencedMatchers;

        public String getDetectorId() {
            return detectorId;
        }

        public void setDetectorId(String detectorId) {
            this.detectorId = detectorId;
        }

        public String getDetectorName() {
            return detectorName;
        }

        public void setDetectorName(String detectorName) {
            this.detectorName = detectorName;
        }

        public List<String> getReferencedMatchers() {
            return referencedMatchers;
        }

        public void setReferencedMatchers(List<String> referencedMatchers) {
            this.referencedMatchers = referencedMatchers;
        }
    }

    public static DlpResult fromApiDlpResult(com.proofpoint.dlp.api.java.DlpResult apiDlpResult) {

        var dlpResult = new DlpResult();
        dlpResult.setDetectors(fromApiDetectors(apiDlpResult.getDetectors()));
        dlpResult.setMatches(fromApiMatches(apiDlpResult.getMatches()));
        dlpResult.setContentParts(fromApiContentParts(apiDlpResult.getContentParts()));
        dlpResult.setSnippets(fromApiSnippets(apiDlpResult.getSnippets()));
        return dlpResult;
    }

    private static List<Detector> fromApiDetectors(List<com.proofpoint.dlp.api.java.Detector> apiDetectors) {
        var detectors = new ArrayList<Detector>();
        for (var apiDetector : apiDetectors) {
            var detector = new Detector();
            detector.setDetectorId(apiDetector.getDetectorId());
            detector.setDetectorName(apiDetector.getDetectorName());
            detector.setReferencedMatchers(List.copyOf(apiDetector.getReferencedMatchers()));
            detectors.add(detector);
        }
        return detectors;
    }

    private static List<Matches> fromApiMatches(List<com.proofpoint.dlp.api.java.Matches> apiMatches) {
        var matches = new ArrayList<Matches>();
        for (var apiMatch : apiMatches) {
            var match = new Matches();
            match.setMatcherId(apiMatch.getMatcherId());
            match.setMatcherName(apiMatch.getMatcherName());
            match.setMatchType(apiMatch.getMatchType().toString());
            match.setMatchCount(apiMatch.getMatchCount());
            var snippetMap = new HashMap<String, MatchesSnippetList>();
            for (var apiSnippetKey : apiMatch.getSnippets().keySet()) {
                var snippets = new MatchesSnippetList();
                snippets.setReferencesToSnippets(apiMatch.getSnippets().get(apiSnippetKey));
                snippetMap.put(apiSnippetKey, snippets);
            }
            match.setMatchSnippets(snippetMap);
            matches.add(match);

        }
        return matches;
    }

    private static List<ContentPartMetadata> fromApiContentParts(List<com.proofpoint.dlp.api.java.ContentPartMetadata> apiContentParts) {
        var metadataList = new ArrayList<ContentPartMetadata>();
        for (var apiContentPart : apiContentParts) {
            var contentPart = new ContentPartMetadata();
            contentPart.setContentPartId(apiContentPart.getContentPartId());
            contentPart.setContentPartType(apiContentPart.getContentPartType().isPresent() ? apiContentPart.getContentPartType().get() : null);
            metadataList.add(contentPart);

        }
        return metadataList;
    }

    private static Map<String, SnippetList> fromApiSnippets(Map<String, List<com.proofpoint.dlp.api.java.Snippet>> apiSnippets) {
        var snippetsMap = new HashMap<String, SnippetList>();
        for (var snipKey : apiSnippets.keySet()) {
            var snippets = new SnippetList();
            for (var apiSnippet : apiSnippets.get(snipKey)) {
                var referencesAndContent = new SnippetReferenceAndContent();
                referencesAndContent.setSnippetOffset(apiSnippet.getSnippetOffset());
                referencesAndContent.setContent(apiSnippet.getContent());
                referencesAndContent.setLength(apiSnippet.getLength());
                referencesAndContent.setOffset(apiSnippet.getOffset());
                snippets.getReferencesAndContent().add(referencesAndContent);
            }
            snippetsMap.put(snipKey, snippets);
        }
        return snippetsMap;
    }
}
