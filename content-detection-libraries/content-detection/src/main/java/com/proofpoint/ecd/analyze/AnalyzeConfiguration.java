package com.proofpoint.ecd.analyze;

import com.proofpoint.ecd.commons.Utils;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValueFactory;

import java.time.Duration;

public class AnalyzeConfiguration {
    public static final int SNIPPET_PADDING_MIN = 0;
    public static final int SNIPPET_MATCH_LENGTH_MIN = 0;
    public static final int SNIPPET_COUNT_MIN = 0;
    public static final int TIMEOUT_MIN = 0;

    private int snippetPadding = 20;
    private int snippetMaxMatchLength = 500;
    private int snippetsMaxCount = 1000;
    private long timeoutMs = 20000;

    public Config toConfig() {
        return append(ConfigFactory.empty());
    }

    public Config append(Config config) {
       return config.withValue("snippet.padding", ConfigValueFactory.fromAnyRef(getSnippetPadding()))
                .withValue("snippet.max-match-length", ConfigValueFactory.fromAnyRef(getSnippetMaxMatchLength()))
                .withValue("snippets.max-count", ConfigValueFactory.fromAnyRef(getSnippetsMaxCount()))
                .withValue("dlp.analyze.timeout", ConfigValueFactory.fromAnyRef(Duration.ofMillis(getTimeoutMs())));
    }

    public int getSnippetPadding() {
        return snippetPadding;
    }

    public int getSnippetMaxMatchLength() {
        return snippetMaxMatchLength;
    }

    public int getSnippetsMaxCount() {
        return snippetsMaxCount;
    }

    public long getTimeoutMs() {
        return timeoutMs;
    }

    public void setSnippetsMaxCount(int snippetsMaxCount) {
        Utils.validateMinValue(snippetsMaxCount, SNIPPET_COUNT_MIN, "snippetsMaxCount");
        this.snippetsMaxCount = snippetsMaxCount;
    }

    public void setSnippetPadding(int snippetPadding) {
        Utils.validateMinValue(snippetPadding, SNIPPET_PADDING_MIN, "snippetPadding");
        this.snippetPadding = snippetPadding;
    }

    public void setSnippetMaxMatchLength(int snippetMaxMatchLength) {
        Utils.validateMinValue(snippetMaxMatchLength, SNIPPET_MATCH_LENGTH_MIN, "snippetMaxMatchLength");
        this.snippetMaxMatchLength = snippetMaxMatchLength;
    }

    public void setTimeoutMs(long timeoutMs) {
        Utils.validateMinValue(timeoutMs, TIMEOUT_MIN, "timeoutMs");
        this.timeoutMs = timeoutMs;
    }
}
