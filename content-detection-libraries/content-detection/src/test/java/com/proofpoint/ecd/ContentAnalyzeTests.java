package com.proofpoint.ecd;

import com.proofpoint.dlp.api.java.ContentPart;
import com.proofpoint.dlp.api.java.SimpleDlpConfigurationLoader;
import com.proofpoint.ecd.analyze.AnalyzeConfiguration;
import com.proofpoint.ecd.analyze.Analyzer;
import com.proofpoint.ecd.analyze.BadDlpConfigurationException;
import com.proofpoint.ecd.analyze.model.AnalyzeRequest;
import com.proofpoint.ecd.analyze.model.DlpCustomerConfiguration;
import com.proofpoint.ecd.analyze.model.FileProcessing;
import com.proofpoint.ecd.parse.ContentParseResult;
import com.proofpoint.ecd.parse.ContentParserConfiguration;
import com.proofpoint.ecd.parse.TikaParser;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.*;

public class ContentAnalyzeTests {

//    @Test
//    public void analyzeDuplicateDisabledDetectorsDlpConfigTest() throws ExecutionException, InterruptedException, TimeoutException {
//        String disabledDetector = "IPAddress_Single_SmartId_Detector";
//        var customerDetectors = new DlpCustomerConfiguration();
//        customerDetectors.setDetectorSet(new String[]{disabledDetector, disabledDetector});
//        analyzeDisabledDetectorsDlpConfigTest(customerDetectors);
//
//    }
//
//    @Test
//    public void analyzeDisabledDetectorsDlpConfigTest() throws ExecutionException, InterruptedException, TimeoutException {
//        String disabledDetector = "IPAddress_Single_SmartId_Detector";
//        var customerDetectors = new DlpCustomerConfiguration();
//        customerDetectors.setDetectorSet(new String[]{disabledDetector});
//        analyzeDisabledDetectorsDlpConfigTest(customerDetectors);
//
//    }
//
//    public void analyzeDisabledDetectorsDlpConfigTest(DlpCustomerConfiguration dlpCustomerDetectors) throws ExecutionException, InterruptedException, TimeoutException {
//
//        var analyzer = getAnalyzer();
//        analyzer.updateConfiguration(getAnalyzer().getAnalyzeConfiguration(), TestResources.getContent("test-dlp-config-new.json"));
//        var contentParseResult = new ContentParseResult();
//        contentParseResult.setContent(TestResources.getContent("test-dlp-config-new-resource.json"));
//
//        var map = Map.of("test", new ContentPart(contentParseResult.getContent()));
//        var res = analyzer.analyze(map, "test", new FileProcessing());
//        assertEquals("assert number of matches", 5, res.getMatches().size());
//
//    }

    @Test
    public void analyzeDlpConfigTest() throws ExecutionException, InterruptedException, TimeoutException {
        var analyzer = getAnalyzer();
        var customerConfig = new DlpCustomerConfiguration();
        customerConfig.setCustomer("analyzeCompressedFileTest");
        analyzer.updateConfiguration(getAnalyzer().getAnalyzeConfiguration(), TestResources.getContent(TestResources.ANALYZE_CONFIG_FILE_NAME), customerConfig);
        var contentParseResult = new ContentParseResult();
        contentParseResult.setContent(TestResources.getContent("test-dlp-config-new-resource.json"));

        var map = Map.of("test", new ContentPart(contentParseResult.getContent()));
        var res = analyzer.analyze(map, "test", new FileProcessing());
        assertEquals("assert number of matches", 5, res.getMatches().size());

    }

    @Test
    public void analyzeCompressedConfigurationTest() throws Exception {
        var analyzer = getAnalyzer();
        assertFalse(analyzer.hasCustomerConfiguration("JUNIT_1MB_DOC_profile"));
        assertFalse(analyzer.hasCustomerConfiguration("itm1"));
        analyzer.updateConfiguration(analyzer.getAnalyzeConfiguration(), TestResources.getBytes(TestResources.ANALYZE_CONFIG_GZ_FILE_NAME));
        assertTrue(analyzer.hasCustomerConfiguration("JUNIT_1MB_DOC_profile"));
        assertTrue(analyzer.hasCustomerConfiguration("itm1"));
    }

    @Test
    public void analyzeCompressedFileTest() throws URISyntaxException {
        var analyzer = getAnalyzer();
        var customerConfig = new DlpCustomerConfiguration();
        customerConfig.setCustomer("analyzeCompressedFileTest");
        analyzer.updateConfiguration(getAnalyzer().getAnalyzeConfiguration(), TestResources.getContent(TestResources.ANALYZE_CONFIG_FILE_NAME), customerConfig);
        var analyze = new ContentAnalyze(analyzer, new TikaParser());
        var req = getAnalyzeRequest();
        req.getDlpConfig().setPath(null);
        req.setPath(TestResources.getPath("test-compressed.zip"));
        try {
            var res = analyze.analyze(req);
            assertEquals("assert matchers count", 3, res.getDlpResult().getMatches().size());
            assertEquals("assert detectors count", 3, res.getDlpResult().getDetectors().size());


        } catch (Exception e) {
            throw new RuntimeException("analyze failed.", e);
        }
    }

    @Test
    public void analyzeTest() {

        var conf = new ContentParserConfiguration();
        conf.setMaxFileSizeMB(10);
        var analyze = new ContentAnalyze(getAnalyzer(), new TikaParser(conf));
        var req = getAnalyzeRequest();

        try {
            var res = analyze.analyze(req);

            assertNotEquals(0, res.getMetric().getParse().getDurationMs());
            assertNotEquals(0, res.getMetric().getAnalyze().getDurationMs());
            assertNotEquals(0, res.getMetric().getDurationMs());
            assertNotEquals(0, res.getMetric().getSize().getBytes());
            assertNotEquals(0, res.getMetric().getSize().getCharCount());
            assertEquals(req.getPath(), res.getMetric().getPath());
            res.setMetric(null);
            var expected = TestResources.getJsonAsMap(TestResources.ANALYZE_1_MB_DOC_RESULT_FILE_NAME);

            expected.put("metric", null);

            var t1 = TestResources.JSON_MAPPER.valueToTree(expected);
            var t2 = TestResources.JSON_MAPPER.valueToTree(res);

            assertEquals("assert analyze result.", t1, t2);

        } catch (Exception e) {
            throw new RuntimeException("analyze failed.", e);
        }
    }

    @Test
    public void badDlpConfigurationTest() {
        var analyzer = getAnalyzer();
        assertFalse(analyzer.hasCustomerConfiguration("JUNIT_1MB_DOC_profile"));
        assertFalse(analyzer.hasCustomerConfiguration("itm1"));
        RuntimeException expected = null;
        try {
            analyzer.updateConfiguration(analyzer.getAnalyzeConfiguration(), TestResources.getContent(TestResources.ANALYZE_BAD_CONFIG_FILE_NAME));

        } catch (RuntimeException re) {
            expected = re;
        }
        assertNotNull(expected);
        assertEquals(BadDlpConfigurationException.class, expected.getCause().getClass());
        assertNotNull(((BadDlpConfigurationException) expected.getCause()).getDlpConfigurationValidationMetric());
        assertEquals("configuration is invalid. errors : [invalid_detector]", expected.getCause().getMessage());
        assertFalse(analyzer.hasCustomerConfiguration("JUNIT_1MB_DOC_profile"));
        assertFalse(analyzer.hasCustomerConfiguration("itm1"));
    }

    @Test(expected = NoSuchFileException.class)
    public void analyzeBadFilePathTest() throws Exception {

        var analyze = new ContentAnalyze(getAnalyzer(), new TikaParser());
        var req = getAnalyzeRequest();
        req.setPath("bad-file-path");
        analyze.analyze(req);

    }

//    @Test(expected = IllegalArgumentException.class)
//    public void analyzeBadCostumerConfigTest() throws Exception {
//
//        var analyze = new ContentAnalyze(getAnalyzer(), new TikaParser());
//        var req = getAnalyzeRequest();
//        req.getDlpConfig().setCustomer("bad-customer-configuration");
//        analyze.analyze(req);
//
//    }

    @Test(expected = NoSuchFileException.class)
    public void analyzeBadDlpConfigPathTest() throws Exception {

        var analyze = new ContentAnalyze(getAnalyzer(), new TikaParser());
        var req = getAnalyzeRequest();
        req.getDlpConfig().setPath("bad-config-path");
        analyze.analyze(req);

    }

    @Test(expected = RuntimeException.class)
    public void parseFileSizeLimitTest() throws Exception {
        var conf = new ContentParserConfiguration();
        conf.setTimeoutMs(0);
        conf.setMaxFileSizeMB(1);
        var analyze = new ContentAnalyze(getAnalyzer(), new TikaParser(conf));
        var req = getAnalyzeRequest();
        analyze.analyze(req);
    }

    @Test(expected = TimeoutException.class)
    public void parseTimeoutLimitPartialNoDataTest() throws Exception {
        var conf = new ContentParserConfiguration();
        conf.setTimeoutMs(10);

        var analyze = new ContentAnalyze(getAnalyzer(), new TikaParser(conf));
        var req = getAnalyzeRequest();
        analyze.analyze(req);
    }

    @Test
    public void parseTimeoutLimitPartialTest() throws Exception {
        var conf = new ContentParserConfiguration();
        conf.setTimeoutMs(500);

        var analyze = new ContentAnalyze(getAnalyzer(), new TikaParser(conf));
        var req = getAnalyzeRequest();
        var res = analyze.analyze(req);
        assertEquals(ContentParseResult.Status.PARTIAL_TIMEOUT, res.getContentParseResult().getStatus());

    }

    @Test(expected = TimeoutException.class)
    public void analyzeTimeoutLimitTest() throws Exception {
        var conf = new AnalyzeConfiguration();
        conf.setTimeoutMs(10);
        var analyzer = getAnalyzer();
        analyzer.updateConfiguration(conf);
        var analyze = new ContentAnalyze(analyzer, new TikaParser(new ContentParserConfiguration()));
        var req = getAnalyzeRequest();
        analyze.analyze(req);
    }

    @Test
    public void parseContentSizeLimitTest() throws Exception {

        var req = getAnalyzeRequest();
        var conf = new ContentParserConfiguration();
        conf.setWriteLimit(1000000);
        var tika = new TikaParser(conf);
        var contentParseResult = tika.parse(Paths.get(req.getPath()), new FileProcessing(), true);
        assertEquals(conf.getWriteLimit(), contentParseResult.getContent().length());
        conf.setWriteLimit(2000000);
        var contentParseResult2 = tika.parse(Paths.get(req.getPath()), new FileProcessing(), true);
        assertEquals(conf.getWriteLimit(), contentParseResult2.getContent().length());
        conf.setWriteLimit(3000000);
        var contentParseResult3 = tika.parse(Paths.get(req.getPath()), new FileProcessing(), true);
        conf.setWriteLimit(-1);
        var contentParseResult4 = tika.parse(Paths.get(req.getPath()), new FileProcessing(), true);
        assertEquals(contentParseResult4.getContent().length(), contentParseResult3.getContent().length());

    }

    private static AnalyzeRequest getAnalyzeRequest() {
        try {
            var req = new AnalyzeRequest();
            req.setPath(TestResources.getPath(TestResources.ANALYZE_1_MB_DOC_FILE_NAME));
            req.getDlpConfig().setPath(TestResources.getPath(TestResources.ANALYZE_CONFIG_FILE_NAME));
            return req;
        } catch (URISyntaxException e) {
            throw new RuntimeException("failed to load analyze request.", e);
        }
    }

    private static Analyzer getAnalyzer() {
        String defaultConf = TestResources.getContent(TestResources.ANALYZE_DEFAULT_CONFIG_FILE_NAME);
        var dlpConfigurationLoader = new SimpleDlpConfigurationLoader(defaultConf);
        return new Analyzer(new AnalyzeConfiguration(), dlpConfigurationLoader);

    }
}
