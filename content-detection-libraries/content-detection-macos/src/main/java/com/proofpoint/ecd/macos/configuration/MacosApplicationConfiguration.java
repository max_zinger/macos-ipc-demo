package com.proofpoint.ecd.macos.configuration;

import com.proofpoint.ecd.commons.runtime.RuntimeArgument;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;

import java.util.List;

public class MacosApplicationConfiguration extends ApplicationConfiguration {

    public MacosApplicationConfiguration() {
        super();
    }

    public MacosApplicationConfiguration(List<String> args) {
        super(args);
        append(args);
    }

    @RuntimeArgument(arg = {"--service-name", "-sn"}, desc = "remote service name")
    private String serviceName = "com.proofpoint.ecd.srv";

    @RuntimeArgument(arg = {"--initial-timeout", "-it"}, minValue = 5000, desc = "initial connection timeout (ms)")
    private int initialConnectionTimeoutMs = 10000;

    @RuntimeArgument(arg = {"--msg-key", "-mk"}, desc = "xpc dictionary message key for context read")
    private String messageKey = "json";

    @RuntimeArgument(arg = {"--msg-connect-val", "-mcv"}, desc = "xpc dictionary message value for establishing connection")
    private String messageConnectValue = "connect";

    public String getServiceName() {
        return serviceName;
    }

    public int getInitialConnectionTimeoutMs() {
        return initialConnectionTimeoutMs;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public String getMessageConnectValue() {
        return messageConnectValue;
    }
}
