package com.proofpoint.ecd.macos;

import com.proofpoint.ecd.ContentAnalyze;
import com.proofpoint.ecd.commons.log.RootLoggerConfiguration;
import com.proofpoint.ecd.ipc.IPCMode;
import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.IPCState;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.AbstractContextHandler;
import com.proofpoint.ecd.ipc.context.ContextActionHandler;
import com.proofpoint.ecd.ipc.context.JsonContextHandler;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.platform.mac.xpc.XPCConnectionConfiguration;
import com.proofpoint.ecd.ipc.platform.mac.xpc.XPCPeerConnection;
import com.proofpoint.ecd.macos.configuration.MacosApplicationConfiguration;
import com.proofpoint.ecd.service.DependencyResource;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;
import com.proofpoint.ecd.service.event.ApplicationEventPublisher;
import com.proofpoint.ecd.service.event.GcAppEventListener;
import com.proofpoint.ecd.service.handler.*;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Application {
    private final static Logger logger = LoggerFactory.getLogger(Application.class);
    private static boolean isStarted = false;
    private static InterProcessCommunicable ipc;

    public static void start(MacosApplicationConfiguration configuration) throws IOException {
        synchronized (Application.class) {

            if (isStarted) {
                throw new RuntimeException("application already running.");
            }
            isStarted = true;
        }
        Runtime.getRuntime().addShutdownHook(new ApplicationCloseThread());
        DependencyResource.getInstance().configure(configuration);
        DependencyResource.getInstance().getEventPublisher().addListener(new GcAppEventListener(configuration));
        DependencyResource.getInstance().getEventPublisher().publish(ApplicationEventPublisher.Event.APP_STARTUP);
        logAppStart();

        ipc = createXPCConnection();
        //initial connection timeout
        try {
            DependencyResource.getInstance().getRwIPCExecutorService().invokeAny(Collections.singletonList(Executors.callable(ipc)), MacosDependencyResource.getConfiguration().getInitialConnectionTimeoutMs(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException | TimeoutException | ExecutionException e) {
            if (ipc.getIPCState() == IPCState.CREATED || ipc.getIPCState() == IPCState.STARTED) {
                logger.warn("{} connect timeout [{}] ms exceeded, application will exit.", ipc.nameToString(), MacosDependencyResource.getConfiguration().getInitialConnectionTimeoutMs());
                close();
                return;
            }
        }
        if (ipc.isClosed()) {
            close();
            return;
        }

//        DependencyResource.getInstance().getScheduledExecutorService().scheduleWithFixedDelay(() -> {
//                    try {
//                        DependencyResource.getInstance().getIpcCollection().closeAllIdle(DependencyResource.getInstance().getConfiguration().getIdleTimeout());
//                    } catch (Exception e) {
//                        logger.error("an error occurred while executing scheduled idle ipcs cleanup.", e);
//                    }
//                },
//                DependencyResource.getInstance().getConfiguration().getIdleTimeout() + DependencyResource.getInstance().getConfiguration().getIdleTimeoutIntervalMs(),
//                DependencyResource.getInstance().getConfiguration().getIdleTimeoutIntervalMs(),
//                TimeUnit.MILLISECONDS);
    }

    /**
     * create main service ipc - assign context and action handlers
     *
     * @return
     */
    private static InterProcessCommunicable createXPCConnection() {

        var xpcConnection = new XPCPeerConnection(fromApplicationConfiguration(MacosDependencyResource.getConfiguration()));
        var ipcContextHandler = new AbstractContextHandler() {
            @Override
            public boolean canHandleStateChange() {
                return true;
            }

            @Override
            public void handleStateChanged(IPCState oldState, IPCState newState) {
                if (newState == IPCState.CLOSED || newState == IPCState.ERROR_UNKNOWN || newState == IPCState.ERROR_CLOSED) {
                    logger.info("service connection closed.");
                    close();
                }
            }

        };
        ipcContextHandler.setIPCOperationsHandler(new JsonContextHandler());
        ipcContextHandler.setIPCMode(IPCMode.READ_WRITE);
        ipcContextHandler.addAction("terminate-process", new TerminationActionHandler());

        var analyzer = DependencyResource.getInstance().getAnalyzer();
        var parser = DependencyResource.getInstance().getParser();

        ipcContextHandler.addAction("analyze", new AnalyzeContextHandler.AnalyzeMessageHandler(new ContentAnalyze(analyzer, parser)));
        ipcContextHandler.addAction("update-dlp-conf", new UpdateDlpConfigurationHandler());
        ipcContextHandler.addAction("update-limitation", new UpdateLimitationsConfigurationHandler());
        ipcContextHandler.addAction("get-status", new MessageStatusQueryHandler());
        xpcConnection.setIPCOperationsHandler(ipcContextHandler);
        return xpcConnection;
    }

    private static XPCConnectionConfiguration fromApplicationConfiguration(MacosApplicationConfiguration configuration) {
        var xpcConfiguration = new XPCConnectionConfiguration();
        xpcConfiguration.setServiceName(configuration.getServiceName());
        xpcConfiguration.setMessageConnectValue(configuration.getMessageConnectValue());
        xpcConfiguration.setMessageKey(configuration.getMessageKey());
        xpcConfiguration.setConnectTimeoutMs(configuration.getInitialConnectionTimeoutMs());
        return xpcConfiguration;
    }

    public static void close() {
        if (ipc != null && !ipc.isClosed()) {
            try {
                ipc.close();
                ipc = null;

            } catch (Exception e) {
                logger.error("error", e);
            }
        }
        DependencyResource.getInstance().terminateResources();
        isStarted = false;
        logger.info("application closed.");
    }

    private static class TerminationActionHandler implements ContextActionHandler {
        @Override
        public Class<? extends IPCMessage> getRefType() {
            return IPCMessage.class;
        }

        @Override
        public void handle(InterProcessCommunicable ipc, IPCMessage msg, IPCOperationsHandler ipcOperationsHandler) {
            handle(msg);
        }

        @Override
        public void handle(InterProcessCommunicable ipc, IPCMessage msg) {
            handle(msg);
        }

        @Override
        public void handle(IPCMessage msg) {
            logger.info("terminate-process called.");
            close();
        }
    }

    private static class ApplicationCloseThread extends Thread {
        @SneakyThrows
        @Override
        public void run() {
            close();
            logger.info("application exit.");
        }
    }

    private static void logAppStart() {

        RootLoggerConfiguration.setRootLogLevel("info");
        logger.info("{} [{}] started.", ApplicationConfiguration.APP_NAME, ApplicationConfiguration.APP_VERSION);
        RootLoggerConfiguration.setRootLogLevel(MacosDependencyResource.getConfiguration().getLogLevel());
        if (logger.isDebugEnabled()) {
            logger.debug("application configuration [{}].", MacosDependencyResource.getConfiguration());
        }

    }

}
