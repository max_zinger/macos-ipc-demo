package com.proofpoint.ecd.macos;

import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.commons.log.RootLoggerConfiguration;
import com.proofpoint.ecd.commons.runtime.RuntimeConfiguration;
import com.proofpoint.ecd.macos.configuration.MacosApplicationConfiguration;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;
import com.proofpoint.ecd.service.model.SerializationMixins;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        try {
            if (args != null && args.length > 0 && args[0].equalsIgnoreCase("-h")) {
                printHelp();
                return;
            }
            ApplicationConfiguration.setApplication(Main.class);
            var configuration = new MacosApplicationConfiguration();
            configure(args, configuration);
            Application.start(configuration);
        } catch (Exception e) {
            LoggerFactory.getLogger(Main.class).error("error", e);
        }
    }

    private static void configure(String[] args, MacosApplicationConfiguration configuration) {
        SerializationMixins.dlpResultSuppressSnippetReferenceAndContent(Utils.JSON_FORMATTER_INDENT_OUTPUT);
        configuration.append(Arrays.asList(args));
        RootLoggerConfiguration.configure(configuration.getLogDir(), Utils.isStringNullOrWhitespaces(MacosApplicationConfiguration.APP_NAME) ? "content-detection-macos" : MacosApplicationConfiguration.APP_NAME);
        RootLoggerConfiguration.setRootLogLevel(configuration.getLogLevel());
    }
    public static void printHelp() {
        System.out.printf("%s %s\r\n", ApplicationConfiguration.APP_NAME, ApplicationConfiguration.APP_VERSION);
        System.out.println("---------------------------------------------");
        RuntimeConfiguration.help(new MacosApplicationConfiguration()).forEach(System.out::println);
    }
}
