package com.proofpoint.ecd.macos;

import com.proofpoint.ecd.macos.configuration.MacosApplicationConfiguration;
import com.proofpoint.ecd.service.DependencyResource;

public class MacosDependencyResource {
    public static DependencyResource getInstance() {
        return DependencyResource.getInstance();
    }

    public static MacosApplicationConfiguration getConfiguration() {
        return (MacosApplicationConfiguration) DependencyResource.getInstance().getConfiguration();
    }
}
