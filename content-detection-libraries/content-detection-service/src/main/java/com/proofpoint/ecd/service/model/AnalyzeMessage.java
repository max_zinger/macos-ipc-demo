package com.proofpoint.ecd.service.model;

import com.proofpoint.ecd.analyze.model.AnalyzeRequest;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;

public class AnalyzeMessage extends IPCMessage<AnalyzeRequest> {
}
