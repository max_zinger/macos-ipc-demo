package com.proofpoint.ecd.service.event;

import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class GcAppEventListener implements ApplicationEventPublisher.ApplicationEventListener {
    private static final Logger logger = LoggerFactory.getLogger(GcAppEventListener.class);

    private GcHelper gcHelper;
    private GcHelper.Configuration configuration;
    private static final String interval_seq_regex = "-|,";

    public GcAppEventListener() {
        gcHelper = new GcHelper();
    }

    public GcAppEventListener(ApplicationConfiguration configuration) {
        this.configuration = configure(configuration);
        gcHelper = new GcHelper(this.configuration);
    }

    private GcHelper.Configuration configure(ApplicationConfiguration configuration) {
        var conf = new GcHelper.Configuration();
        try {
            conf.initial_delay_sec = configuration.getGcInitialDelay();
            int[] intervals = Arrays.stream(configuration.getGcInterval().split(interval_seq_regex)).mapToInt(Integer::parseInt).filter(i -> i > 0).toArray();
            if (intervals != null && intervals.length > 1) {
                conf.startup_intervals = intervals;
            } else {
                logger.warn("invalid interval sequence");
            }
        } catch (Exception e) {
            logger.error("error", e);
        }
        return conf;
    }

    @Override
    public void onPublish(ApplicationEventPublisher.Event e) {
        switch (e) {
            case DLP_FINISH:
                gcHelper.tryGc(configuration.initial_delay_sec);
                return;
            case DLP_START:
                //gcHelper.abort();
                return;
            case APP_STARTUP:
                gcHelper.tryGc();
                return;
            default:
                return;

        }
    }

    @Override
    public void dispose() {
        gcHelper.dispose();
    }
}
