package com.proofpoint.ecd.service.handler.cache;

import com.proofpoint.ecd.analyze.model.AnalyzeResponse;

import java.nio.file.attribute.BasicFileAttributes;

public class AnalyzeCacheValue {
    private AnalyzeResponse response;
    private BasicFileAttributes fileAttributes;

    public AnalyzeResponse getResponse() {
        return response;
    }

    public AnalyzeCacheValue setResponse(AnalyzeResponse response) {
        this.response = response;
        return this;
    }

    public BasicFileAttributes getFileAttributes() {
        return fileAttributes;
    }

    public AnalyzeCacheValue setFileAttributes(BasicFileAttributes fileAttributes) {
        this.fileAttributes = fileAttributes;
        return this;
    }
}
