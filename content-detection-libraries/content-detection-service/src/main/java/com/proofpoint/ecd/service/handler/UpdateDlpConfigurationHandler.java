package com.proofpoint.ecd.service.handler;

import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.ContextActionHandler;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.context.model.IPCMessageStatus;
import com.proofpoint.ecd.ipc.context.model.MessageStatus;
import com.proofpoint.ecd.service.DependencyResource;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;
import com.proofpoint.ecd.service.model.DlpConfigurationUpdate;
import com.proofpoint.ecd.commons.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Base64;
import java.util.concurrent.locks.ReentrantLock;

/**
 * dlp configuration message operations handling
 * <p>
 * update is valid for current analyzer configuration, therefore will affect all analyze actions
 */
public class UpdateDlpConfigurationHandler implements ContextActionHandler<DlpConfigurationUpdate.Message> {
    private final static Logger logger = LoggerFactory.getLogger(UpdateDlpConfigurationHandler.class);
    private static final ReentrantLock lock = new ReentrantLock();
    public final static String UPDATE_OK = "update dlp configuration success";

    @Override
    public Class<? extends IPCMessage> getRefType() {
        return DlpConfigurationUpdate.Message.class;
    }

    @Override
    public void handle(InterProcessCommunicable ipc, DlpConfigurationUpdate.Message msg) {
        handle(ipc, msg, null);
    }

    /**
     * async configuration update, if cache in use - will invalidate any cached analyze results
     *
     * @param ipc
     * @param msg
     */
    @Override
    public void handle(InterProcessCommunicable ipc, DlpConfigurationUpdate.Message msg, IPCOperationsHandler ipcOperationsHandler) {
        try {
            ipcOperationsHandler = IPCOperationsHandler.getIPCOperationsHandler(ipc, ipcOperationsHandler);
            var messageStatus = IPCOperationsHandler.getMessageStatus(ipcOperationsHandler, msg);
            messageStatus.addEvent(IPCMessageStatus.Defaults.MSG_ENQUEUE);

            DependencyResource.getInstance().getIoExecutorService().submit(() -> {
                lock.lock();
                try {
                    messageStatus.addEvent(IPCMessageStatus.Defaults.MSG_PROCESSING);
                    IPCOperationsHandler.addInProcess(ipc);
                    var response = new IPCMessage<String>();
                    DlpConfigurationUpdate.Data data = null;
                    try {
                        if (logger.isDebugEnabled()) {
                            logger.debug("analyze request message [{}]", Utils.JSON_FORMATTER_INDENT_OUTPUT.writeValueAsString(msg));
                        }
                        response.setContext(msg.getContext());
                        data = msg.getData();
                        if (data == null || data.getConfig() == null || data.getConfig().isEmpty()) {
                            throw new IllegalArgumentException("dlp configuration data is null");
                        }
                        updateLimitations(data.getLimitations());
                        updateDlpConfiguration(data);

                        response.setData(UPDATE_OK);
                        logger.info(UPDATE_OK);

                        invalidateCache();


                    } catch (Exception e) {
                        handleError(response, e, messageStatus, data);
                    } finally {
                        IPCOperationsHandler.removeInProcess(ipc);
                    }
                    if (ipc != null) {
                        ipc.write(response);
                    }
                    if (logger.isDebugEnabled()) {
                        logger.debug("update dlp config response message [{}]", Utils.JSON_FORMATTER_INDENT_OUTPUT.writeValueAsString(response));
                    }
                    messageStatus.addEvent(IPCMessageStatus.Defaults.MSG_COMPLETED);
                } catch (Exception e) {
                    logger.error("error during write.", e);
                } finally {
                    lock.unlock();
                }
            });
        } catch (Exception e) {
            logger.error("error during write.", e);
        }
    }

    private void handleError(IPCMessage<String> response, Exception e, MessageStatus messageStatus, DlpConfigurationUpdate.Data data) {
        response.getContext().setIncidentId(java.util.UUID.randomUUID().toString());
        response.setError(e.getMessage());
        response.set_status(IPCMessageStatus.fromException(e));
        messageStatus.addEvent(response.get_status());
        messageStatus.setIncidentId(response.getContext().getIncidentId());
        logger.error(String.format("failed to update dlp config. message context : [%s].", response.getContext()), e);
        if (data != null) {
            logger.error("dlp configuration : \n{}", data);
        }
    }

    private void updateDlpConfiguration(DlpConfigurationUpdate.Data data) throws Exception {
        var analyzer = DependencyResource.getInstance().getAnalyzer();
        var analyzeConf = DependencyResource.getInstance().getConfiguration().getParseAnalyzeConfiguration().getAnalyze();

        if (data.getConfig().isGzip()) {
            analyzer.updateConfiguration(analyzeConf, data.getConfig().getConfigAsBytes(), data.getCustomerConfiguration());

        } else if (data.getConfig().isBase64()) {
            analyzer.updateConfiguration(analyzeConf, new String(Base64.getDecoder().decode((String) data.getConfig().getValue())), data.getCustomerConfiguration());

        } else if (data.getConfig().isJson()) {
            analyzer.updateConfiguration(analyzeConf, (String) data.getConfig().getValue(), data.getCustomerConfiguration());

        } else {
            throw new IllegalArgumentException("unsupported data type for update dlp configuration");
        }
    }

    private void invalidateCache() {
        if (DependencyResource.getInstance().getConfiguration().isUseCaching() && DependencyResource.getInstance().getCache() != null) {
            DependencyResource.getInstance().getCache().invalidateAll();
            logger.info("analyze cache invalidated.");
        }
    }

    private void updateLimitations(ApplicationConfiguration.ParseAnalyzeConfiguration limitations) {
        if (limitations != null) {
            DependencyResource.getInstance().getConfiguration().setParseAnalyzeConfiguration(limitations);
            DependencyResource.getInstance().getParser().setConfiguration(limitations.getParse());
        }

    }

    @Override
    public void handle(DlpConfigurationUpdate.Message msg) {
        handle(null, msg);
    }
}
