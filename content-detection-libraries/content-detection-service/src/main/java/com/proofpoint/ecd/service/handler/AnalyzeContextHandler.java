package com.proofpoint.ecd.service.handler;

import com.google.common.cache.Cache;
import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.ContentAnalyze;
import com.proofpoint.ecd.analyze.model.AnalyzeResponse;
import com.proofpoint.ecd.analyze.model.DlpConfig;
import com.proofpoint.ecd.analyze.model.FileIdentifier;
import com.proofpoint.ecd.ipc.context.model.MessageStatus;
import com.proofpoint.ecd.service.DependencyResource;
import com.proofpoint.ecd.service.event.ApplicationEventPublisher;
import com.proofpoint.ecd.service.handler.cache.AnalyzeCacheValue;
import com.proofpoint.ecd.service.model.AnalyzeMessage;
import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.IPCState;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.AbstractContextHandler;
import com.proofpoint.ecd.ipc.context.ContextActionHandler;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.context.model.IPCMessageStatus;
import com.proofpoint.ecd.parse.ContentParseResult;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

/**
 * analyze message operations handling
 */
@SuppressWarnings("rawtypes")
public class AnalyzeContextHandler extends AbstractContextHandler {
    private static final Logger logger = LoggerFactory.getLogger(AnalyzeContextHandler.class);
    InterProcessCommunicable ipc;

    public AnalyzeContextHandler(InterProcessCommunicable ipc) {
        this.ipc = ipc;
    }

    @Override
    public boolean canHandleStateChange() {
        return true;
    }

    @Override
    public void handleStateChanged(IPCState oldState, IPCState newState) {
        if (ipc != null && (newState == IPCState.CLOSED || newState == IPCState.ERROR_UNKNOWN)) {
            try {
                DependencyResource.getInstance().getIpcCollection().close(ipc);
            } catch (Exception e) {
                logger.error("error during write ipc close.", e);
            }
        }
    }

    public static class AnalyzeMessageHandler implements ContextActionHandler<AnalyzeMessage> {
        private static final Logger logger = LoggerFactory.getLogger(AnalyzeMessageHandler.class);
        private final ExecutorService executors;
        private final ContentAnalyze analyze;
        private final Cache<String, AnalyzeCacheValue> cache;
        private final ApplicationConfiguration configuration;

        public AnalyzeMessageHandler(ContentAnalyze analyze) {
            this.analyze = analyze;
            this.executors = DependencyResource.getInstance().getIoExecutorService();
            this.configuration = DependencyResource.getInstance().getConfiguration();
            this.cache = DependencyResource.getInstance().getCache();
        }

        @Override
        public void handle(InterProcessCommunicable writer, AnalyzeMessage msg) {
            handle(writer, msg, null);
        }

        /**
         * async handling of analyze message
         *
         * @param ipc
         * @param msg
         */
        @Override
        public void handle(InterProcessCommunicable ipc, AnalyzeMessage msg, IPCOperationsHandler ipcOperationsHandler) {
            try {
                ipcOperationsHandler = IPCOperationsHandler.getIPCOperationsHandler(ipc, ipcOperationsHandler);
                var messageStatus = IPCOperationsHandler.getMessageStatus(ipcOperationsHandler, msg);
                messageStatus.addEvent(IPCMessageStatus.Defaults.MSG_ENQUEUE);
                executors.submit(() -> {
                    try {
                        if (ipc.isClosed() || ipc.isClosePending()) {
                            logger.warn("[{}] invalidated. terminating message [{}]", ipc == null ? "ipc" : ipc.getName(), Utils.JSON_FORMATTER_INDENT_OUTPUT.writeValueAsString(msg.getContext()));
                            return;
                        }
                        messageStatus.addEvent(IPCMessageStatus.Defaults.MSG_PROCESSING);
                        IPCOperationsHandler.addInProcess(ipc);
                        if (logger.isDebugEnabled()) {
                            logger.debug("analyze request message [{}]", Utils.JSON_FORMATTER_INDENT_OUTPUT.writeValueAsString(msg));
                        }
                        DependencyResource.getInstance().getEventPublisher().publish(ApplicationEventPublisher.Event.DLP_START);
                        var response = new IPCMessage<AnalyzeResponse>();
                        response.setContext(msg.getContext());
                        try {
                            if (msg.getData().getDlpConfig() == null) {
                                msg.getData().setDlpConfig(new DlpConfig());
                            }
                            var dlpRes = getDlpResult(msg);
                            response.set_status(fromAnalyzeResponse(dlpRes));
                            response.setData(dlpRes);

                        } catch (Exception e) {
                            handleError(e, response, messageStatus);
                            logger.error(String.format("error analyzing file. message context : [%s].", response.getContext()), e);
                        } finally {
                            IPCOperationsHandler.removeInProcess(ipc);
                        }
                        ipc.write(response);

                        if (logger.isDebugEnabled()) {
                            logger.debug("analyze response message [{}]", Utils.JSON_FORMATTER_INDENT_OUTPUT.writer().writeValueAsString(response));
                        }
                        messageStatus.addEvent(IPCMessageStatus.Defaults.MSG_COMPLETED);
                    } catch (Exception e) {
                        logger.error("error during write.", e);
                    }
                    DependencyResource.getInstance().getEventPublisher().publish(ApplicationEventPublisher.Event.DLP_FINISH);

                });
            } catch (Exception e) {
                logger.error("error during write.", e);
            }
        }

        private void handleError(Exception e, IPCMessage msg, MessageStatus msgStat) {
            msg.set_status(IPCMessageStatus.fromException(e));
            msg.getContext().setIncidentId(java.util.UUID.randomUUID().toString());
            msg.setError(e.getMessage());
            msgStat.addEvent(msg.get_status());
            msgStat.setIncidentId(msg.getContext().getIncidentId());
        }

        /**
         * iterate provided analyze file paths, analyze a single valid path
         *
         * @param msg
         * @return
         * @throws Exception
         */
        private AnalyzeResponse getDlpResult(AnalyzeMessage msg) throws Exception {
            var paths = Arrays.stream(new String[]{msg.getData().getPath(), msg.getData().getPathEx().getSource(), msg.getData().getPathEx().getTarget()})
                    .filter(p -> !Utils.isStringNullOrWhitespaces(p)).distinct().collect(Collectors.toList());
            logger.info("analyze path priority list : {}", paths);
            Exception lastError = null;
            for (String path : paths) {

                logger.debug("trying to analyze file [{}].", path);

                msg.getData().setPath(path);
                try {
                    return getAnalyzeCachedResult(msg);
                } catch (Exception e) {
                    String em = e.getMessage();
                    if (Utils.isStringNullOrWhitespaces(em)) {
                        em = e.getClass().getName();
                    }
                    logger.warn("[{}] {}", path, em);
                    lastError = e;
                }

            }
            if (lastError != null) {
                throw lastError;
            }
            throw new IllegalArgumentException("unable to process requested paths " + paths);
        }

        /**
         * get the analyze result by processing or from cache
         *
         * @param msg
         * @return
         * @throws Exception
         */
        private AnalyzeResponse getAnalyzeCachedResult(AnalyzeMessage msg) throws Exception {
            if (!configuration.isUseCaching() || cache == null) {
                return analyze.analyze(msg.getData());
            }
            var fileId = new FileIdentifier(msg.getData().getPath());
            AnalyzeCacheValue cachedRes;

            if ((cachedRes = cache.getIfPresent(fileId.getKey())) != null && fileId.getAttributes().lastModifiedTime().equals(cachedRes.getFileAttributes().lastModifiedTime())) {
                logger.debug("cached content scan result for file [{}] is valid.", fileId.getAbsolutePath());
                return cachedRes.getResponse();

            }
            var analyzeRes = analyze.analyze(msg.getData());
            cachedRes = new AnalyzeCacheValue()
                    .setResponse(analyzeRes)
                    .setFileAttributes(fileId.getAttributes());
            logger.debug("caching content scan result for file [{}].", fileId.getAbsolutePath());
            cache.put(fileId.getKey(), cachedRes);
            return analyzeRes;
        }

        private static IPCMessageStatus fromAnalyzeResponse(AnalyzeResponse res) {
            if (res.getContentParseResult() != null) {
                switch (res.getContentParseResult().getStatus()) {
                    case ContentParseResult.Status.PARTIAL_WRITE_LIMIT:
                        return IPCMessageStatus.Defaults.PARTIAL_SUCCESS_WRITE_LIMIT;
                    case ContentParseResult.Status.PARTIAL_TIMEOUT:
                        return IPCMessageStatus.Defaults.PARTIAL_SUCCESS_TIMEOUT;
                }
            }

            return IPCMessageStatus.Defaults.OK;
        }

        @Override
        public void handle(AnalyzeMessage msg) {

        }

        @Override
        public Class<? extends IPCMessage> getRefType() {
            return AnalyzeMessage.class;
        }


    }

}