package com.proofpoint.ecd.service.handler;

import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.ContextActionHandler;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContextErrorHandler implements ContextActionHandler {
    private static final Logger logger = LoggerFactory.getLogger(ContextErrorHandler.class);

    @Override
    public Class<? extends IPCMessage> getRefType() {
        return IPCMessage.class;
    }

    @Override
    public void handle(InterProcessCommunicable writer, IPCMessage msg, IPCOperationsHandler ipcOperationsHandler) {
        handle(msg);
    }

    @Override
    public void handle(InterProcessCommunicable writer, IPCMessage msg) {
        handle(msg);
    }

    @Override
    public void handle(IPCMessage msg) {
        logger.warn(msg.getError());
    }
}
