package com.proofpoint.ecd.service.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.proofpoint.ecd.analyze.model.DlpCustomerConfiguration;
import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;

import java.util.Base64;
import java.util.List;

public class DlpConfigurationUpdate {
    public static class Config<T> {
        private static final Base64.Decoder decoder = Base64.getDecoder();

        private String compression;
        private String encoding;

        private T value;

        public String getCompression() {
            return compression;
        }

        public void setCompression(String compression) {
            this.compression = compression;
        }

        public String getEncoding() {
            return encoding;
        }

        public void setEncoding(String encoding) {
            this.encoding = encoding;
        }

        public T getValue() {
            return value;
        }

        public void setValue(T value) {
            this.value = value;
        }

        @JsonIgnore
        public boolean isEmpty() {
            return Utils.isStringNullOrWhitespaces(compression) || value == null;
        }

        @JsonIgnore
        public boolean isGzip() {
            return Compression.JSON_GZIP.equalsIgnoreCase(compression);
        }

        @JsonIgnore
        public boolean isJson() {
            return Encoding.NONE.equalsIgnoreCase(encoding) || Encoding.PLAIN.equalsIgnoreCase(encoding) || Encoding.JSON.equalsIgnoreCase(encoding);
        }

        @JsonIgnore
        public boolean isBase64() {
            return Encoding.JSON_BASE64.equalsIgnoreCase(encoding);
        }

        @JsonIgnore
        public byte[] getConfigAsBytes() {
            byte[] config = null;
            if (this.value instanceof String) {
                config = decoder.decode((String) this.value);

            } else if (this.value instanceof byte[]) {
                config = (byte[]) this.value;

            } else if (this.value instanceof List) {
                config = toByteArray(((List<Integer>) value));

            }
            if (config == null) {
                throw new NullPointerException("dlp configuration is null");
            }
            return config;
        }

        private static byte[] toByteArray(List<Integer> bytes) {
            var byteArray = new byte[bytes.size()];
            for (int i = 0; i < byteArray.length; i++) {
                byteArray[i] = bytes.get(i).byteValue();
            }
            return byteArray;
        }
    }

    public static class Compression {
        public final static String NONE = "none";
        public final static String JSON_GZIP = "gzip";

        private Compression() {
        }
    }

    public static class Encoding {
        public final static String NONE = "none";
        public final static String PLAIN = "plain";
        public final static String JSON = "json";
        public final static String JSON_BASE64 = "base64";

        private Encoding() {
        }
    }

    public static <T> Message getMessage(T conf, String encoding, String compression) {
        var config = new Config<>();
        config.setCompression(compression);
        config.setEncoding(encoding);
        config.setValue(conf);
        var msgData = new Data<>();
        msgData.setConfig(config);
        var msg = new Message();
        msg.setData(msgData);
        return msg;
    }


    public static class Data<T> {
        private Config<T> config;
        private DlpCustomerConfiguration customerConfiguration;
        private ApplicationConfiguration.ParseAnalyzeConfiguration limitations;

        public Config<T> getConfig() {
            return config;
        }

        public void setConfig(Config<T> config) {
            this.config = config;
        }

        public DlpCustomerConfiguration getCustomerConfiguration() {
            return customerConfiguration;
        }

        public void setCustomerConfiguration(DlpCustomerConfiguration customerConfiguration) {
            this.customerConfiguration = customerConfiguration;
        }

        public ApplicationConfiguration.ParseAnalyzeConfiguration getLimitations() {
            return limitations;
        }

        public void setLimitations(ApplicationConfiguration.ParseAnalyzeConfiguration limitations) {
            this.limitations = limitations;
        }

        @Override
        public String toString() {
            var obj = Utils.JSON_FORMATTER_INDENT_OUTPUT.createObjectNode();
            ObjectNode confObj = null;
            if (config != null) {
                confObj = Utils.JSON_FORMATTER_INDENT_OUTPUT.valueToTree(config);
                confObj.remove("value");
                confObj.set("valueIsEmpty", BooleanNode.valueOf(config.isEmpty()));
            }
            obj.set("config", confObj);
            obj.set("customerConfiguration", customerConfiguration != null ? Utils.JSON_FORMATTER_INDENT_OUTPUT.valueToTree(customerConfiguration) : null);
            obj.set("limitations", limitations != null ? Utils.JSON_FORMATTER_INDENT_OUTPUT.valueToTree(limitations) : null);
            return obj.toPrettyString();
        }
    }

    public static class Message extends IPCMessage<Data> {

    }
}

