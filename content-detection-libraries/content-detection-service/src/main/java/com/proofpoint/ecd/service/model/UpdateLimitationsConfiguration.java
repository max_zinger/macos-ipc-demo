package com.proofpoint.ecd.service.model;


import com.proofpoint.ecd.analyze.AnalyzeConfiguration;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.context.model.IPCMessageContext;
import com.proofpoint.ecd.parse.ContentParserConfiguration;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;

public class UpdateLimitationsConfiguration {

    public static Message getMessage(ContentParserConfiguration parse, AnalyzeConfiguration analyze){
        var msg = new Message();
        msg.setContext(new IPCMessageContext());
        var conf = new ApplicationConfiguration.ParseAnalyzeConfiguration();
        conf.setAnalyze(analyze);
        conf.setParse(parse);
        msg.setData(conf);

        return msg;
    }
    public static class Message extends IPCMessage<ApplicationConfiguration.ParseAnalyzeConfiguration> {
    }
}
