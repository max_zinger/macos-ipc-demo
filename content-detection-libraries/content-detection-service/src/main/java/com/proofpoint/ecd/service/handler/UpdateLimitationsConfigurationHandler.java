package com.proofpoint.ecd.service.handler;

import com.proofpoint.ecd.service.DependencyResource;
import com.proofpoint.ecd.service.model.UpdateLimitationsConfiguration;
import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.AbstractContextHandler;
import com.proofpoint.ecd.ipc.context.ContextActionHandler;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.context.model.IPCMessageStatus;
import com.proofpoint.ecd.commons.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * content parse and analyze limitations configuration message operations handling
 * <p>
 * update is valid for current configurations, therefore will affect all parse and analyze actions
 */
public class UpdateLimitationsConfigurationHandler implements ContextActionHandler<UpdateLimitationsConfiguration.Message> {
    private final static Logger logger = LoggerFactory.getLogger(UpdateLimitationsConfigurationHandler.class);
    public final static String UPDATE_OK = "update limitations configuration success";

    @Override
    public Class<? extends IPCMessage> getRefType() {
        return UpdateLimitationsConfiguration.Message.class;
    }

    @Override
    public void handle(InterProcessCommunicable writer, UpdateLimitationsConfiguration.Message msg) {
        handle(writer, msg, null);
    }

    /**
     * handle configuration update, if cache in use - will invalidate any cached analyze results
     *
     * @param ipc
     * @param msg
     */
    @Override
    public void handle(InterProcessCommunicable ipc, UpdateLimitationsConfiguration.Message msg, IPCOperationsHandler ipcOperationsHandler) {

        try {
            ipcOperationsHandler = IPCOperationsHandler.getIPCOperationsHandler(ipc, ipcOperationsHandler);
            var messageStatus = IPCOperationsHandler.getMessageStatus(ipcOperationsHandler, msg);
            messageStatus.addEvent(IPCMessageStatus.Defaults.MSG_PROCESSING);
            if (logger.isDebugEnabled()) {
                logger.debug("update parse analyze config request message [{}]", Utils.JSON_FORMATTER_INDENT_OUTPUT.writeValueAsString(msg));
            }
            var res = new IPCMessage<String>();
            res.setContext(msg.getContext());
            try {
                var conf = msg.getData();
                DependencyResource.getInstance().getConfiguration().setParseAnalyzeConfiguration(conf);
                DependencyResource.getInstance().getAnalyzer().updateConfiguration(conf.getAnalyze());
                DependencyResource.getInstance().getParser().setConfiguration(conf.getParse());

                res.setData(UPDATE_OK);
                if (DependencyResource.getInstance().getConfiguration().isUseCaching() && DependencyResource.getInstance().getCache() != null) {
                    DependencyResource.getInstance().getCache().invalidateAll();
                    logger.debug("analyze cache invalidated.");
                }

            } catch (Exception e) {
                res.getContext().setIncidentId(java.util.UUID.randomUUID().toString());
                res.setError(e.getMessage());
                res.set_status(IPCMessageStatus.Defaults.BAD_ARGUMENTS);
                messageStatus.addEvent(res.get_status());
                messageStatus.setIncidentId(res.getContext().getIncidentId());
                logger.error(String.format("failed to update parse analyze config [%s]. message context : [%s].", Utils.JSON_FORMATTER_INDENT_OUTPUT.writeValueAsString(msg.getData()), res.getContext()), e);
            }

            if (ipc != null) {
                ipc.write(res);
            }
            messageStatus.addEvent(IPCMessageStatus.Defaults.MSG_COMPLETED);
            logger.info(UPDATE_OK);
            if (logger.isDebugEnabled()) {
                logger.debug("update parse analyze config response message [{}]", Utils.JSON_FORMATTER_INDENT_OUTPUT.writeValueAsString(res));
            }

        } catch (Exception e) {
            logger.error("error during write.", e);
        }
    }

    @Override
    public void handle(UpdateLimitationsConfiguration.Message msg) {
        handle(null, msg);
    }
}
