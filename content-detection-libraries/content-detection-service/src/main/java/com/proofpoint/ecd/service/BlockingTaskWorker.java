package com.proofpoint.ecd.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * Worker thread that syncs queued blocking tasks executions on the main thread. terminates on user thread exit
 */
public class BlockingTaskWorker extends Thread {
    private static final Logger logger = LoggerFactory.getLogger(BlockingTaskWorker.class);

    private static final int await_complete_interval_ms = 500;
    private static final long await_complete_timeout_ms = 10000;
    private static final int exec_err_max_count = 5;
    private LinkedBlockingQueue<BlockingTask> linkedBlockingQueue;
    private static BlockingTaskWorker blockingTaskWorker;
    private boolean isRunning = false;

    public interface BlockingTask {
        BlockingTaskResult exec();
        String getName();
    }

    public static class BlockingTaskResult {
        Object result;
        boolean terminate;

        public BlockingTaskResult(Object result, boolean terminate) {
            this.result = result;
            this.terminate = terminate;
        }

        public BlockingTaskResult(Object result) {
            this(result, false);
        }
    }

    private BlockingTaskWorker() {
        linkedBlockingQueue = new LinkedBlockingQueue<>();
        this.setDaemon(true);
        this.start();
    }

    public static BlockingTaskWorker getInstance() {
        if (blockingTaskWorker != null) {
            return blockingTaskWorker;
        }
        synchronized (BlockingTaskWorker.class) {
            if (blockingTaskWorker == null) {
                blockingTaskWorker = new BlockingTaskWorker();
            }
        }

        return blockingTaskWorker;
    }

    public void put(BlockingTask task) throws InterruptedException {
        linkedBlockingQueue.put(task);
        logger.debug("task [{}] was added to blocking queue", task.getName());
    }

    public boolean hasTasks() {
        return isRunning && !linkedBlockingQueue.isEmpty();
    }

    public void awaitComplete(long timeoutMs) {
        long total = 0;
        try {
            while (hasTasks()) {

                if (total >= timeoutMs) {
                    this.interrupt();
                    return;
                }
                Thread.sleep(await_complete_interval_ms);
                total += await_complete_interval_ms;

            }
        } catch (Exception e) {
            logger.warn("error", e);
        }

    }

    public void awaitComplete() {
        awaitComplete(await_complete_timeout_ms);

    }

    @Override
    public void run() {
        isRunning = true;
        logger.info("blocking task worker thread started");
        int errCount = 0;
        try {
            while (true) {
                try {
                    if (linkedBlockingQueue.take().exec().terminate) {
                        logger.info("termination task executed - existing worker thread");
                        break;
                    }
                } catch (Exception e) {
                    logger.error("error", e);
                    if (errCount++ >= exec_err_max_count) {
                        logger.warn("max error count [{}]", errCount);
                        break;
                    }
                }

            }
        } finally {
            isRunning = false;
        }
        logger.info("blocking task worker thread terminated");
    }
}
