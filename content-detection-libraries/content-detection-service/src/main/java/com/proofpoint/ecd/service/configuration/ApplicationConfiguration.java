package com.proofpoint.ecd.service.configuration;

import ch.qos.logback.classic.Level;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.analyze.AnalyzeConfiguration;
import com.proofpoint.ecd.analyze.Analyzer;
import com.proofpoint.ecd.commons.runtime.RuntimeArgument;
import com.proofpoint.ecd.commons.runtime.RuntimeConfiguration;
import com.proofpoint.ecd.parse.ContentParserConfiguration;


import java.util.List;

/**
 * runtime application configurations
 */
public class ApplicationConfiguration extends RuntimeConfiguration {

    public static String APP_PATH;
    public static String APP_VERSION;
    public static String APP_NAME;

    private static String default_tmpdir = System.getProperty("java.io.tmpdir");

    public static void setApplication(Class<?> c) {
        APP_PATH = Utils.getAppPath(c);
        APP_VERSION = c.getPackage().getImplementationVersion();
        APP_NAME = c.getPackage().getImplementationTitle();
        default_tmpdir = APP_PATH + "/java_temp";
        System.setProperty("java.io.tmpdir", default_tmpdir);
        System.setProperty("jna.tmpdir", default_tmpdir + "/jna");
    }

    @RuntimeArgument(arg = {"--idle-timeout", "-idt"}, minValue = 10000, desc = "inactivity timeout for open connections")
    private int idleTimeout = 10000;

    @RuntimeArgument(arg = {"--idle-timeout-interval-ms", "-idti"}, ignore = true, minValue = 5000, desc = "")
    private int idleTimeoutIntervalMs = 5000;

    @RuntimeArgument(arg = {"--concurrent-connections", "-cc"}, minValue = 2, desc = "max number of concurrent read and write ipc instances")
    private int concurrentConnections = 2;

    @RuntimeArgument(arg = {"--concurrent-workers", "-cw"}, minValue = 5, desc = "number of read and write worker threads")
    private int concurrentWorkers = 10;

    @RuntimeArgument(arg = {"--log-level", "-ll"}, desc = "log level - off,error,warn,info,debug,trace,all")
    private String logLevel = "error";
    @RuntimeArgument(arg = {"--log-dir", "-ld"}, desc = "logging directory path")
    private String logDir = APP_PATH + "/logs";

    @RuntimeArgument(arg = {"--dlp-conf-path", "-dcp"}, desc = "path to default dlp configuration file")
    private String dlpConfPath = Analyzer.DEFAULT_DLP_CONF_PATH;

    @RuntimeArgument(arg = {"--tmpdir", "-jtd"}, desc = "java io temp dir location (e.g. c:/apps/itc-content-detection/java_temp, ../java_temp/jna)")
    private String tempDir = default_tmpdir;

    @RuntimeArgument(arg = {"--cache", "-c"}, desc = "cache dlp results")
    private boolean useCaching = false;
    @RuntimeArgument(arg = {"--cache-exp", "-cexp"}, minValue = 1, desc = "cached dlp result entry will expire after access period (seconds)")
    private int cacheEntryExpirationSec = 1;
    @RuntimeArgument(arg = {"--gc-delay", "-gcd"}, minValue = 5, desc = "gc initial start delay (seconds)")
    private int gcInitialDelay = 15;
    @RuntimeArgument(arg = {"--gc-interval", "-gci"}, desc = "gc interval (seconds sequence)")
    private String gcInterval = "2-3-3-5-7-10";


    private ParseAnalyzeConfiguration parseAnalyzeConfiguration = new ParseAnalyzeConfiguration();

    public ParseAnalyzeConfiguration getParseAnalyzeConfiguration() {
        return parseAnalyzeConfiguration;
    }

    public void setParseAnalyzeConfiguration(ParseAnalyzeConfiguration parseAnalyzeConfiguration) {
        this.parseAnalyzeConfiguration = parseAnalyzeConfiguration;
    }

    public ApplicationConfiguration() {
    }

    public ApplicationConfiguration(List<String> args) {
        append(args);
    }

    @Override
    public void apply() {
        System.setProperty("java.io.tmpdir", tempDir);
        System.setProperty("jna.tmpdir", tempDir + "/jna");
        logLevel = Level.toLevel(logLevel, Level.INFO).levelStr.toLowerCase();
    }

    public String getTempDir() {
        return tempDir;
    }

    public String getDlpConfPath() {
        return dlpConfPath;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(String level) {
        logLevel = level;
    }


    public int getIdleTimeoutIntervalMs() {
        return idleTimeoutIntervalMs;
    }

    public int getIdleTimeout() {
        return idleTimeout;
    }

    public int getConcurrentConnections() {
        return concurrentConnections;
    }

    public int getConcurrentWorkers() {
        return concurrentWorkers;
    }


    public String getLogDir() {
        return logDir;
    }

    public boolean isUseCaching() {
        return useCaching;
    }

    public int getCacheEntryExpirationSec() {
        return cacheEntryExpirationSec;
    }

    public int getGcInitialDelay() {
        return gcInitialDelay;
    }

    public String getGcInterval() {
        return gcInterval;
    }

    @Override
    public String toString() {
        final var mapper = new ObjectMapper();
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "failed to serialize application configuration - " + e.getMessage();
        }

    }

    public static class ParseAnalyzeConfiguration {
        private AnalyzeConfiguration analyze = new AnalyzeConfiguration();
        private ContentParserConfiguration parse = new ContentParserConfiguration();

        public AnalyzeConfiguration getAnalyze() {
            return analyze;
        }

        public ContentParserConfiguration getParse() {
            return parse;
        }

        public void setAnalyze(AnalyzeConfiguration analyze) {
            this.analyze = analyze;
        }

        public void setParse(ContentParserConfiguration parse) {
            this.parse = parse;
        }
    }


}
