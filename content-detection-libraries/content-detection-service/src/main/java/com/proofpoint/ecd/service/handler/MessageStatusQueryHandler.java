package com.proofpoint.ecd.service.handler;

import com.proofpoint.ecd.ipc.IPCOperationsHandler;
import com.proofpoint.ecd.ipc.InterProcessCommunicable;
import com.proofpoint.ecd.ipc.context.AbstractContextHandler;
import com.proofpoint.ecd.ipc.context.ContextActionHandler;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.context.model.IPCMessageStatus;
import com.proofpoint.ecd.ipc.context.model.MessageStatus;
import com.proofpoint.ecd.ipc.context.model.MessageStatusQuery;
import com.proofpoint.ecd.commons.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * handle message status requests
 */
public class MessageStatusQueryHandler implements ContextActionHandler<MessageStatusQuery.Message> {
    private final static Logger logger = LoggerFactory.getLogger(MessageStatusQueryHandler.class);

    @Override
    public Class<? extends IPCMessage> getRefType() {
        return MessageStatusQuery.Message.class;
    }

    @Override
    public void handle(InterProcessCommunicable ipc, MessageStatusQuery.Message msg) {
        handle(ipc, msg, null);
    }


    @Override
    public void handle(InterProcessCommunicable ipc, MessageStatusQuery.Message msg, IPCOperationsHandler ipcOperationsHandler) {

        try {
            ipcOperationsHandler = IPCOperationsHandler.getIPCOperationsHandler(ipc, ipcOperationsHandler);
            var messageStatus = IPCOperationsHandler.getMessageStatus(ipcOperationsHandler, msg);
            messageStatus.addEvent(IPCMessageStatus.Defaults.MSG_PROCESSING);
            if (logger.isDebugEnabled()) {
                logger.debug("context message list request [{}]", Utils.JSON_FORMATTER_INDENT_OUTPUT.writeValueAsString(msg));
            }
            var response = new IPCMessage<ArrayList<MessageStatus>>();
            response.setContext(msg.getContext());
            try {
                if (ipcOperationsHandler == null) {
                    throw new RuntimeException("ipc operation handler is undefined");
                }
                var msgStatList = new ArrayList<MessageStatus>();
                var msgStatMap = ipcOperationsHandler.getMessageStatusMap();
                for (String id : Arrays.stream(msg.getData().getTransactionIdSet()).distinct().collect(Collectors.toList())) {
                    try {
                        if (!Utils.isStringNullOrWhitespaces(id) && msgStatMap.containsKey(id)) {
                            msgStatList.add(msgStatMap.get(id));
                        } else {
                            logger.warn("message with id [{}] not found", id);
                        }
                    } catch (Exception e) {
                        logger.error("error", e);
                    }

                }
                msgStatList.sort(Comparator.comparing(MessageStatus::getCreatedAt));
                response.setData(msgStatList);

            } catch (Exception e) {
                response.getContext().setIncidentId(UUID.randomUUID().toString());
                response.setError(e.getMessage());
                response.set_status(IPCMessageStatus.fromException(e));
                messageStatus.addEvent(response.get_status());
                logger.error(String.format("error : [%s].", Utils.JSON_FORMATTER_INDENT_OUTPUT.writeValueAsString(msg.getData()), response.getContext()), e);
            }

            if (ipc != null) {
                ipc.write(response);
            }
            messageStatus.addEvent(IPCMessageStatus.Defaults.MSG_COMPLETED);
            if (logger.isDebugEnabled()) {
                logger.debug("update parse analyze config response message [{}]", Utils.JSON_FORMATTER_INDENT_OUTPUT.writeValueAsString(response));
            }

        } catch (Exception e) {
            logger.error("error during write.", e);
        }
    }

    @Override
    public void handle(MessageStatusQuery.Message msg) {
        handle(null, msg, null);
    }
}
