package com.proofpoint.ecd.service.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class GcHelper {
    private static final Logger logger = LoggerFactory.getLogger(GcHelper.class);

    public static class Configuration {
        public int[] startup_intervals = {2, 3, 3, 5, 7, 10};
        public int initial_delay_sec = 15;
    }

    public GcHelper() {
        configuration = new Configuration();
    }

    public GcHelper(Configuration configuration) {
        this.configuration = configuration;
    }

    private Configuration configuration;
    private ScheduledExecutorService gcExecutorService = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> gcFuture;
    private GcHelperThread gcThread;
    private static final ReentrantLock gc_lock = new ReentrantLock();

    public boolean isGcRunning() {
        return (gcThread != null && gcThread.running);
    }

    public void abort() {
        if (gcThread != null) {
            if (!gcThread.finished) {
                gcThread.abort = true;
            }
        }
        if (gcFuture != null) {
            gcFuture.cancel(true);
        }
    }
    public boolean tryGc(){
        return tryGc(0);
    }
    public boolean tryGc(int initialDelay) {

        if (!gc_lock.tryLock()) {
            return false;
        }
        try {
            abort();
            gcThread = new GcHelperThread(this.configuration);
            gcFuture = gcExecutorService.schedule(gcThread, initialDelay, TimeUnit.SECONDS);
            logger.debug("gc helper scheduled to start in [{}] [{}]", initialDelay, TimeUnit.SECONDS.name());

        } finally {
            gc_lock.unlock();
        }
        return true;
    }
    private class GcHelperThread implements Runnable {
        boolean abort = false;
        boolean finished = false;
        boolean running = false;
        Configuration configuration;

        GcHelperThread(Configuration configuration) {
            this.configuration = configuration;
        }

        @Override
        public void run() {
            int length = configuration.startup_intervals.length;
            for (int i = 0; i < length; i++) {

                if (abort) {
                    break;
                }
                try {
                    running = true;
                    logger.debug("gc helper - call [{}] of [{}] started", i + 1, length);
                    System.gc();
                    logger.debug("gc helper - call [{}] of [{}] finished", i + 1, length);
                    if (abort || (i + 1 >= length)) {

                        break;
                    }
                    Thread.sleep(configuration.startup_intervals[i] * 1000);
                } catch (InterruptedException ie) {
                    logger.info(ie.getMessage());
                } catch (Exception e) {
                    logger.error("error", e);
                }
            }
            running = false;
            finished = true;
            logger.debug("gc helper finished");
        }
    }

    public void dispose() {
        abort();
        if (gcExecutorService != null) {
            gcExecutorService.shutdownNow();
        }
    }


}
