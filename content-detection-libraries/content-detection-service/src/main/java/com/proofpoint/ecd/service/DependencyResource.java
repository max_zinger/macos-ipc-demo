package com.proofpoint.ecd.service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.analyze.Analyzer;
import com.proofpoint.ecd.service.event.ApplicationEventPublisher;
import com.proofpoint.ecd.service.handler.cache.AnalyzeCacheValue;
import com.proofpoint.ecd.ipc.IPCCollection;
import com.proofpoint.ecd.parse.ContentParser;
import com.proofpoint.ecd.parse.TikaParser;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * application dependencies
 */
public class DependencyResource {
    private Analyzer analyzer;
    private ContentParser parser;
    private ExecutorService rwIPCExecutorService;
    private ExecutorService ioExecutorService;
    private ScheduledExecutorService scheduledExecutorService;
    private ApplicationConfiguration configuration;
    private IPCCollection ipcCollection;
    private Cache<String, AnalyzeCacheValue> cache;
    private ApplicationEventPublisher eventPublisher;
    private static DependencyResource resource;

    private DependencyResource() {

    }

    public void configure() throws IOException {
        configure(new ApplicationConfiguration());
    }

    public void configure(ApplicationConfiguration configuration) throws IOException {
        this.configuration = configuration;
        this.analyzer = configureAnalyzer(configuration);
        this.parser = new TikaParser();
        this.rwIPCExecutorService = Executors.newFixedThreadPool((configuration.getConcurrentConnections() * 2) + 1);
        this.ioExecutorService = Executors.newFixedThreadPool(configuration.getConcurrentWorkers());
        this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        this.ipcCollection = new IPCCollection();
        if (configuration.isUseCaching()) {
            this.cache = CacheBuilder.newBuilder()
                    .expireAfterAccess(configuration.getCacheEntryExpirationSec(), TimeUnit.SECONDS)
                    //.expireAfterWrite(5,TimeUnit.MINUTES)
                    .build();
        }
        this.eventPublisher = new ApplicationEventPublisher();


    }

    private static Analyzer configureAnalyzer(ApplicationConfiguration configuration) throws IOException {
        if (Files.exists(Paths.get(configuration.getDlpConfPath()))) {
            return new Analyzer(configuration.getParseAnalyzeConfiguration().getAnalyze(), Paths.get(configuration.getDlpConfPath()));
        }
        return new Analyzer(configuration.getParseAnalyzeConfiguration().getAnalyze(), Utils.readStringFromResource(DependencyResource.class, Analyzer.DEFAULT_DLP_CONF_CLASS_PATH));
    }

    public static DependencyResource getInstance() {
        if (resource != null) {
            return resource;
        }
        synchronized (DependencyResource.class) {
            if (resource == null) {
                resource = new DependencyResource();
            }
        }

        return resource;
    }


    public Analyzer getAnalyzer() {
        return analyzer;
    }

    public ContentParser getParser() {
        return parser;
    }

    /**
     * get executor for ipc creation and read triggers
     *
     * @return
     */
    public ExecutorService getRwIPCExecutorService() {
        return rwIPCExecutorService;
    }

    /**
     * get executor for ipc message processing
     *
     * @return
     */
    public ExecutorService getIoExecutorService() {
        return ioExecutorService;
    }

    /**
     * get current application configuration
     *
     * @return
     */
    public ApplicationConfiguration getConfiguration() {
        return configuration;
    }

    /**
     * get executor for scheduling
     *
     * @return
     */
    public ScheduledExecutorService getScheduledExecutorService() {
        return scheduledExecutorService;
    }

    /**
     * get ipc open references
     *
     * @return
     */
    public IPCCollection getIpcCollection() {
        return ipcCollection;
    }

    public Cache<String, AnalyzeCacheValue> getCache() {
        return cache;
    }

    public ApplicationEventPublisher getEventPublisher() {
        return eventPublisher;
    }

    /**
     * dispose executors, cache and close current open ipc connections.
     */
    public void terminateResources() {
        if (ipcCollection != null) {
            ipcCollection.closeAll();
            ipcCollection = null;

        }
        if (cache != null) {
            cache.invalidateAll();
            cache = null;
        }
        if (eventPublisher != null) {
            eventPublisher.dispose();
        }


        shutdownNow(getScheduledExecutorService());
        shutdownNow(getRwIPCExecutorService());
        shutdownNow(getIoExecutorService());


    }

    private static void shutdownNow(ExecutorService executorService) {
        if (executorService != null) {
            executorService.shutdownNow();
        }
    }
}
