package com.proofpoint.ecd.service.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.concurrent.Executors;

public class ApplicationEventPublisher {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationEventPublisher.class);

    public enum Event {
        APP_STARTUP,
        APP_SHUTDOWN,
        APP_CONF,
        DLP_FINISH,
        DLP_START,
        DLP_CONF,


    }

    public interface ApplicationEventListener {
        void onPublish(Event e);

        void dispose();
    }

    private ArrayList<ApplicationEventListener> listeners = new ArrayList<>();

    public void addListener(ApplicationEventListener listener) {
        listeners.add(listener);
    }

    public void publish(Event e) {
        logger.debug("publishing event [{}]", e.name());
        for (var listener : listeners) {
            try {
                listener.onPublish(e);
            } catch (Exception ex) {
                logger.error("error publishing event " + e.name(), ex);
            }

        }
    }

    public void publishAsync(Event e) {
        Executors.newSingleThreadExecutor().submit(() -> publish(e));
    }

    public void dispose() {
        for (var listener : listeners) {
            try {
                listener.dispose();
            } catch (Exception e) {
                logger.error("error", e);
            }

        }
    }
}
