package com.proofpoint.ecd.service.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class SerializationMixins {

    public abstract class DlpResult{
        public abstract class SnippetReferenceAndContent{
            @JsonIgnore
            public String getContent(){
                return "";
            }
        }
    }
    public static void dlpResultSuppressSnippetReferenceAndContent(ObjectMapper mapper){
        mapper.addMixIn(com.proofpoint.ecd.analyze.model.DlpResult.SnippetReferenceAndContent.class, SerializationMixins.DlpResult.SnippetReferenceAndContent.class);
    }
}
