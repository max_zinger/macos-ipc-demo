package com.proofpoint.ecd.service.handler;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.analyze.AnalyzeConfiguration;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.platform.MockIPC;
import com.proofpoint.ecd.parse.ContentParserConfiguration;
import com.proofpoint.ecd.service.DependencyResource;
import com.proofpoint.ecd.service.TestResources;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;
import com.proofpoint.ecd.service.model.UpdateLimitationsConfiguration;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class UpdateLimitationsConfigurationHandlerTests {
    private static final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    @Test
    public void updateParseAnalyzeConfigurationHandlerTest() throws URISyntaxException, IOException {
        initResources();

        var parseConf = new ContentParserConfiguration();
        parseConf.setMaxFileSizeMB(100);
        parseConf.setWriteLimit(50);
        parseConf.setTimeoutMs(25);


        var analyzeConf = new AnalyzeConfiguration();
        analyzeConf.setSnippetPadding(20);
        analyzeConf.setSnippetsMaxCount(10);
        analyzeConf.setSnippetMaxMatchLength(5);
        analyzeConf.setTimeoutMs(40);

        var updateMsg = UpdateLimitationsConfiguration.getMessage(parseConf, analyzeConf);
        var updateHandler = new UpdateLimitationsConfigurationHandler();
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(updateMsg));
        updateMsg = (UpdateLimitationsConfiguration.Message) mapper.readValue(mapper.writeValueAsBytes(updateMsg), new UpdateLimitationsConfigurationHandler().getRefType());

        var mockIPC = MockIPC.getPlatformIPC();
        updateHandler.handle(mockIPC, updateMsg);

        assertNotNull(mockIPC.getResult());
        var res = (IPCMessage<String>) mockIPC.getResult();

        assertEquals(UpdateLimitationsConfigurationHandler.UPDATE_OK, res.getData());
        assertEquals(parseConf.getMaxFileSizeMB(), DependencyResource.getInstance().getParser().getConfiguration().getMaxFileSizeMB());
        assertEquals(parseConf.getWriteLimit(), DependencyResource.getInstance().getParser().getConfiguration().getWriteLimit());
        assertEquals(parseConf.getTimeoutMs(), DependencyResource.getInstance().getParser().getConfiguration().getTimeoutMs());


        assertEquals(analyzeConf.getSnippetPadding(), DependencyResource.getInstance().getAnalyzer().getAnalyzeConfiguration().getSnippetPadding());
        assertEquals(analyzeConf.getSnippetMaxMatchLength(), DependencyResource.getInstance().getAnalyzer().getAnalyzeConfiguration().getSnippetMaxMatchLength());
        assertEquals(analyzeConf.getTimeoutMs(), DependencyResource.getInstance().getAnalyzer().getAnalyzeConfiguration().getTimeoutMs());
        assertEquals(analyzeConf.getSnippetsMaxCount(), DependencyResource.getInstance().getAnalyzer().getAnalyzeConfiguration().getSnippetsMaxCount());


        terminateResources();


    }

    private void initResources() throws URISyntaxException, IOException {
        var args = new ArrayList<String>();
        args.add("--dlp-conf-path");
        args.add(TestResources.getPath(TestResources.ANALYZE_DEFAULT_CONFIG_FILE_NAME));
        ApplicationConfiguration configuration = new ApplicationConfiguration(args);
        DependencyResource.getInstance().configure(configuration);
    }

    private void terminateResources() {
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 10, TimeUnit.SECONDS);
        DependencyResource.getInstance().terminateResources();
    }

}
