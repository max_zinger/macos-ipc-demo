package com.proofpoint.ecd.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.proofpoint.ecd.analyze.model.DlpCustomerConfiguration;
import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.service.model.AnalyzeMessage;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Calendar;
import java.util.Map;

public class TestResources {

    public final static ObjectMapper JSON_MAPPER = new ObjectMapper();
    static{
        JSON_MAPPER.setTimeZone(Calendar.getInstance().getTimeZone());
    }
    public static final String ANALYZE_DEFAULT_CONFIG_FILE_NAME = "dlp-config.json";
    public static final String ANALYZE_1_MB_DOC_FILE_NAME = "test-1mb-doc.docx";
    public static final String ANALYZE_MSG_FILE_NAME = "test-analyze-file-message.json";
    public static final String ANALYZE_MSG_RESULT_FILE_NAME = "test-analyze-file-result.json";
    public static final String ANALYZE_CONFIG_FILE_NAME = "test-dlp-config.json";
    public static final String ANALYZE_CONFIG_GZ_FILE_NAME = "test-dlp-config.json.gz";

    public static final String ANALYZE_1_MB_DOC_CUSTOMER_CONFIG_NAME = "JUNIT_1MB_DOC_profile";
    public static final String ANALYZE_ITM_1_CUSTOMER_CONFIG_NAME = "itm1";
    public static final String ANALYZE_DEFAULT_CUSTOMER_CONFIG_NAME = "Customer-Conf-Default";

    public static final String ANALYZE_CONFIG_UPDATE_DATA_TOSTRING_FILE_NAME = "dlpConfigurationUpdateDataToStringTest_resource.json";

    public static String getPath(String filename) throws URISyntaxException {
        return getResourceFilePath(TestResources.class, filename).toString();
    }

    public static byte[] getBytes(String filename) {
        return Utils.readFromResource(TestResources.class, filename);
    }

    public static String getContent(String filename) {
        return Utils.readStringFromResource(TestResources.class, filename);
    }

    public static Map getJsonAsMap(String filename) {
        return Utils.readJsonFromResource(TestResources.class, filename, Map.class);
    }

    public static <T> T getJson(String filename, Class<T> type) {
        return Utils.readJsonFromResource(TestResources.class, filename, type);
    }

    public static <T> T getJson(String filename, TypeReference<T> type) {
        return Utils.readJsonFromResource(TestResources.class, filename, type);
    }

    public static Path getResourceFilePath(Class<?> c, String filename) {
        ClassLoader classLoader = c.getClassLoader();
        URL resource = classLoader.getResource(filename);
        if (resource == null) {
            throw new IllegalArgumentException("file not found! " + filename);
        }
        Path path = null;
        try {
            path = new File(resource.getPath()).toPath();
        } catch (Exception e1) {
            try {
                path = new File(resource.toURI().getPath()).toPath();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return path;
    }
    public static JsonNode removeMetricData(JsonNode msg) {
        var metric = (ObjectNode) msg.get("data").get("metric");
        metric.remove("startedAt");
        metric.remove("endedAt");
        metric.remove("parse");
        metric.remove("analyze");
        metric.remove("contentCached");
        metric.remove("durationMs");
        return msg;
    }
    public static AnalyzeMessage getAnalyzeMessage() throws URISyntaxException {
        var analyzeMsg = TestResources.getJson(TestResources.ANALYZE_MSG_FILE_NAME,AnalyzeMessage.class);
        analyzeMsg.getData().setPath(TestResources.getPath(TestResources.ANALYZE_1_MB_DOC_FILE_NAME));
        var customerConfiguration = new DlpCustomerConfiguration();
        customerConfiguration.setCustomer(TestResources.ANALYZE_1_MB_DOC_CUSTOMER_CONFIG_NAME);
        analyzeMsg.getData().getDlpConfig().setDlpCustomerConfiguration(customerConfiguration);
        analyzeMsg.getData().getDlpConfig().setPath(TestResources.getPath(TestResources.ANALYZE_CONFIG_FILE_NAME));
        return analyzeMsg;
    }
    public static AnalyzeMessage getBadAnalyzeMessage() throws URISyntaxException {
        var msg = TestResources.getAnalyzeMessage();
        msg.getData().setPath("bad-path");
        return msg;
    }


}
