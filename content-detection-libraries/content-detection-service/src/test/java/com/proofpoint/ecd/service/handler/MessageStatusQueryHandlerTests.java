package com.proofpoint.ecd.service.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.proofpoint.ecd.ContentAnalyze;
import com.proofpoint.ecd.analyze.model.DlpCustomerConfiguration;
import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.commons.log.RootLoggerConfiguration;
import com.proofpoint.ecd.analyze.model.AnalyzeRequest;
import com.proofpoint.ecd.ipc.context.JsonContextHandler;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.context.model.IPCMessageStatus;
import com.proofpoint.ecd.ipc.context.model.MessageStatus;
import com.proofpoint.ecd.ipc.context.model.MessageStatusQuery;
import com.proofpoint.ecd.ipc.platform.MockIPC;
import com.proofpoint.ecd.service.DependencyResource;
import com.proofpoint.ecd.service.TestResources;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class MessageStatusQueryHandlerTests {

    private static String get_status_action = "get-status";
    private static String analyze_action = "analyze";

    @Test
    public void analyzeMessageStatusTest() throws IOException {
        RootLoggerConfiguration.setRootLogLevel("error");
        DependencyResource.getInstance().configure();
        var mockIPC = MockIPC.getPlatformIPC();
        var handler = new JsonContextHandler();
        mockIPC.setIPCOperationsHandler(handler);
        var parser = DependencyResource.getInstance().getParser();
        var analyzer = DependencyResource.getInstance().getAnalyzer();
        handler.addAction(analyze_action, new AnalyzeContextHandler.AnalyzeMessageHandler(new ContentAnalyze(analyzer, parser)));
        handler.addAction(get_status_action, new MessageStatusQueryHandler());

        var analyzeReq = getAnalyzeRequestMessage();
        analyzeReq.getContext().setTransactionId("analyze-1");

        handler.handleRead(mockIPC, getJsonBytes(analyzeReq));
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 1, TimeUnit.MINUTES);

        var msgStatQuery = getMessageStatusQuery();
        msgStatQuery.getContext().setTransactionId("get-status-1");
        msgStatQuery.getData().setTransactionIdSet(new String[]{"analyze-1"});
        handler.handleRead(mockIPC, getJsonBytes(msgStatQuery));

        var query1 = (IPCMessage<List<MessageStatus>>) mockIPC.getResult();
        assertEquals(1, query1.getData().size());
        assertEquals(IPCMessageStatus.Defaults.MSG_RECEIVED.getStatus(), query1.getData().get(0).getEvents().get(0).getName());
        assertEquals(IPCMessageStatus.Defaults.MSG_ENQUEUE.getStatus(), query1.getData().get(0).getEvents().get(1).getName());
        assertEquals(IPCMessageStatus.Defaults.MSG_PROCESSING.getStatus(), query1.getData().get(0).getEvents().get(2).getName());
        assertEquals(IPCMessageStatus.Defaults.MSG_COMPLETED.getStatus(), query1.getData().get(0).getEvents().get(3).getName());
        assertEquals(analyzeReq.getContext().getTransactionId(), query1.getData().get(0).getTransactionId());

        msgStatQuery.getContext().setTransactionId("get-status-2");
        msgStatQuery.getData().setTransactionIdSet(new String[]{"analyze-1", "get-status-1"});
        handler.handleRead(mockIPC, getJsonBytes(msgStatQuery));
        var query2 = (IPCMessage<List<MessageStatus>>) mockIPC.getResult();
        assertEquals(2, query2.getData().size());
        assertEquals(IPCMessageStatus.Defaults.MSG_RECEIVED.getStatus(), query2.getData().get(1).getEvents().get(0).getName());
        assertEquals(IPCMessageStatus.Defaults.MSG_PROCESSING.getStatus(), query2.getData().get(1).getEvents().get(1).getName());
        assertEquals(IPCMessageStatus.Defaults.MSG_COMPLETED.getStatus(), query2.getData().get(1).getEvents().get(2).getName());
        assertTrue(query2.getData().get(1).getIsCompleted());
        assertFalse(query2.getData().get(1).getHasError());
        assertEquals(MessageStatus.Status.COMPLETED, query1.getData().get(0).getStatus());

        System.out.println(TestResources.JSON_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(msgStatQuery));
        System.out.println(TestResources.JSON_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(mockIPC.getResult()));

    }

    @Test
    public void analyzeBadMessageStatusTest() throws IOException {
        RootLoggerConfiguration.setRootLogLevel("error");
        DependencyResource.getInstance().configure();
        var mockIPC = MockIPC.getPlatformIPC();
        var handler = new JsonContextHandler();
        mockIPC.setIPCOperationsHandler(handler);
        var parser = DependencyResource.getInstance().getParser();
        var analyzer = DependencyResource.getInstance().getAnalyzer();
        handler.addAction(analyze_action, new AnalyzeContextHandler.AnalyzeMessageHandler(new ContentAnalyze(analyzer, parser)));
        handler.addAction(get_status_action, new MessageStatusQueryHandler());

        var analyzeReq = getAnalyzeRequestMessage();
        analyzeReq.getData().setPath("bad-path");
        analyzeReq.getContext().setTransactionId("analyze-1");
        analyzeReq.getContext().setCorrelationId("correlation-1");

        handler.handleRead(mockIPC, getJsonBytes(analyzeReq));
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 1, TimeUnit.MINUTES);

        var msgStatQuery = getMessageStatusQuery();
        msgStatQuery.getContext().setTransactionId("get-status-1");
        msgStatQuery.getData().setTransactionIdSet(new String[]{"analyze-1"});
        handler.handleRead(mockIPC, getJsonBytes(msgStatQuery));
        System.out.println(TestResources.JSON_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(mockIPC.getResult()));
        var query1 = (IPCMessage<List<MessageStatus>>) mockIPC.getResult();
        assertEquals(1, query1.getData().size());
        assertEquals(IPCMessageStatus.Defaults.MSG_RECEIVED.getStatus(), query1.getData().get(0).getEvents().get(0).getName());
        assertEquals(IPCMessageStatus.Defaults.MSG_ENQUEUE.getStatus(), query1.getData().get(0).getEvents().get(1).getName());
        assertEquals(IPCMessageStatus.Defaults.MSG_PROCESSING.getStatus(), query1.getData().get(0).getEvents().get(2).getName());
        assertEquals(IPCMessageStatus.Defaults.FILE_NOT_FOUND.getStatus(), query1.getData().get(0).getEvents().get(3).getName());
        assertEquals(IPCMessageStatus.Defaults.MSG_COMPLETED.getStatus(), query1.getData().get(0).getEvents().get(4).getName());
        assertTrue(query1.getData().get(0).getIsCompleted());
        assertTrue(query1.getData().get(0).getHasError());
        assertEquals(MessageStatus.Status.ERROR, query1.getData().get(0).getStatus());
        assertNotNull(query1.getData().get(0).getIncidentId());
        assertEquals(analyzeReq.getContext().getCorrelationId(), query1.getData().get(0).getCorrelationId());
        assertEquals(analyzeReq.getContext().getTransactionId(), query1.getData().get(0).getTransactionId());
        System.out.println(TestResources.JSON_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(msgStatQuery));


    }

    private static MessageStatusQuery.Message getMessageStatusQuery() {
        MessageStatusQuery.Message msg = new MessageStatusQuery.Message();
        msg.getContext().setAction(get_status_action);
        return msg;
    }

    private static IPCMessage<AnalyzeRequest> getAnalyzeRequestMessage() {
        try {
            IPCMessage msg = new IPCMessage();
            var req = new AnalyzeRequest();
            req.setPath(TestResources.getPath(TestResources.ANALYZE_1_MB_DOC_FILE_NAME));
            req.getDlpConfig().setPath(TestResources.getPath(TestResources.ANALYZE_CONFIG_FILE_NAME));
            var customerConfiguration = new DlpCustomerConfiguration();
            customerConfiguration.setCustomer(TestResources.ANALYZE_1_MB_DOC_CUSTOMER_CONFIG_NAME);
            req.getDlpConfig().setDlpCustomerConfiguration(customerConfiguration);
            msg.getContext().setAction(analyze_action);
            msg.setData(req);
            return msg;
        } catch (URISyntaxException e) {
            throw new RuntimeException("failed to load analyze request.", e);
        }
    }

    private static byte[] getJsonBytes(Object obj) throws JsonProcessingException {
        return TestResources.JSON_MAPPER.writeValueAsBytes(obj);
    }
}
