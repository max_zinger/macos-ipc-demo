package com.proofpoint.ecd.service.event;

import com.proofpoint.ecd.commons.log.RootLoggerConfiguration;
import org.junit.Test;

import static org.junit.Assert.*;

public class GcHelperTests {

    @Test
    public void gcTest() throws InterruptedException {
        RootLoggerConfiguration.setRootLogLevel("debug");
        var conf = new GcHelper.Configuration();
        conf.initial_delay_sec = 2;
        conf.startup_intervals = new int[]{3, 3, 3};
        var gcHelper = new GcHelper(conf);
        assertTrue(gcHelper.tryGc(conf.initial_delay_sec));
        assertFalse(gcHelper.isGcRunning());
        Thread.sleep(conf.initial_delay_sec * 1500);
        assertTrue(gcHelper.isGcRunning());

        Thread.sleep(conf.initial_delay_sec * conf.startup_intervals.length * 1500);
        assertFalse(gcHelper.isGcRunning());


    }

    @Test
    public void gcEventAbortTest() throws InterruptedException {
        RootLoggerConfiguration.setRootLogLevel("debug");
        var conf = new GcHelper.Configuration();
        conf.initial_delay_sec = 2;
        conf.startup_intervals = new int[]{3, 3, 3};
        var gcHelper = new GcHelper(conf);
        assertTrue(gcHelper.tryGc(conf.initial_delay_sec));
        assertFalse(gcHelper.isGcRunning());
        Thread.sleep(conf.initial_delay_sec * 1500);
        assertTrue(gcHelper.isGcRunning());
        gcHelper.abort();
        Thread.sleep(conf.startup_intervals.length * 1000);
        assertFalse(gcHelper.isGcRunning());


    }

    @Test
    public void multipleGcEventTest() throws InterruptedException {
        RootLoggerConfiguration.setRootLogLevel("debug");
        var conf = new GcHelper.Configuration();
        conf.initial_delay_sec = 2;
        conf.startup_intervals = new int[]{3, 3, 3};
        var gcHelper = new GcHelper(conf);
        assertTrue(gcHelper.tryGc(conf.initial_delay_sec));
        assertFalse(gcHelper.isGcRunning());
        Thread.sleep(conf.initial_delay_sec * 1500);
        assertTrue(gcHelper.isGcRunning());
        assertTrue(gcHelper.tryGc(conf.initial_delay_sec));
        Thread.sleep(conf.initial_delay_sec * 1000);
        gcHelper.abort();


    }
}
