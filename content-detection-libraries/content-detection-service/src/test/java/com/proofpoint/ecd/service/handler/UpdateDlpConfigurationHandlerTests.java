package com.proofpoint.ecd.service.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.proofpoint.ecd.ContentAnalyze;
import com.proofpoint.ecd.analyze.model.DlpCustomerConfiguration;
import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.analyze.model.AnalyzeRequest;
import com.proofpoint.ecd.analyze.model.AnalyzeResponse;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.context.model.IPCMessageStatus;
import com.proofpoint.ecd.ipc.platform.MockIPC;
import com.proofpoint.ecd.service.DependencyResource;
import com.proofpoint.ecd.service.TestResources;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;
import com.proofpoint.ecd.service.model.DlpConfigurationUpdate;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class UpdateDlpConfigurationHandlerTests {
    private static final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    @Test
    public void updateDlpConfigurationHandlerTest() throws URISyntaxException, IOException {
        var msg = DlpConfigurationUpdate.getMessage(TestResources.getContent(TestResources.ANALYZE_CONFIG_FILE_NAME), DlpConfigurationUpdate.Encoding.JSON, DlpConfigurationUpdate.Compression.NONE);
        updateDlpConfigurationHandlerTest(msg);
    }

    @Test
    public void updateDlpConfigurationHandlerCompressedTest() throws URISyntaxException, IOException {
        var msg = DlpConfigurationUpdate.getMessage(TestResources.getBytes(TestResources.ANALYZE_CONFIG_GZ_FILE_NAME), DlpConfigurationUpdate.Encoding.JSON, DlpConfigurationUpdate.Compression.JSON_GZIP);
        updateDlpConfigurationHandlerTest(msg);
    }

    private void updateDlpConfigurationHandlerTest(DlpConfigurationUpdate.Message updateMsg) throws URISyntaxException, IOException {
        initResources();

        var analyze = new ContentAnalyze(DependencyResource.getInstance().getAnalyzer(), DependencyResource.getInstance().getParser());
        var req = getAnalyzeRequest();
        AnalyzeResponse res;

        var updateDlpConfHandler = new UpdateDlpConfigurationHandler();
        updateMsg = (DlpConfigurationUpdate.Message) mapper.readValue(mapper.writeValueAsBytes(updateMsg), new UpdateDlpConfigurationHandler().getRefType());
        var badKindMockIPC = MockIPC.getPlatformIPC();
        updateDlpConfHandler.handle(badKindMockIPC, updateMsg);
        terminateResources();
        assertEquals(UpdateDlpConfigurationHandler.UPDATE_OK, ((IPCMessage) badKindMockIPC.getResult()).getData());
        assertTrue("assert target customer configuration.", DependencyResource.getInstance().getAnalyzer().hasCustomerConfiguration(TestResources.ANALYZE_ITM_1_CUSTOMER_CONFIG_NAME));
        analyze = new ContentAnalyze(DependencyResource.getInstance().getAnalyzer(), DependencyResource.getInstance().getParser());

        try {
            res = analyze.analyze(req);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        assertNotNull("assert result for valid costumer configuration after updating dlp configuration.", res);
        assertEquals("assert detectors size.", 1, res.getDlpResult().getDetectors().size());
        assertEquals("assert matches size.", 1, res.getDlpResult().getMatches().size());
        assertEquals("assert content parts size.", 1, res.getDlpResult().getContentParts().size());
        assertEquals("assert snippets size.", 1, res.getDlpResult().getSnippets().size());
    }

    @Test
    public void updateDlpConfigurationHandlerBadConfigurationTest() throws URISyntaxException, IOException {
        initResources();

        var updateDlpConfHandler = new UpdateDlpConfigurationHandler();
        var badData = new Object[]{"xyz", "{}", "{ \"hello\":\"world\"}", new String[]{"hello", "world"}, null, 123};
        for (var data : badData) {
            var msg = DlpConfigurationUpdate.getMessage(data, DlpConfigurationUpdate.Encoding.JSON, DlpConfigurationUpdate.Compression.NONE);
            updateDlpConfHandler.handle(msg);
        }

        terminateResources();

        assertTrue("assert customer configuration unchanged.", DependencyResource.getInstance().getAnalyzer().hasCustomerConfiguration(TestResources.ANALYZE_DEFAULT_CUSTOMER_CONFIG_NAME));
    }

    @Test
    public void updateDlpConfigurationHandlerBadConfigurationKindTest() throws URISyntaxException, IOException {
        initResources();

        var updateDlpConfHandler = new UpdateDlpConfigurationHandler();
        var badKindMessage = DlpConfigurationUpdate.getMessage(TestResources.getBytes(TestResources.ANALYZE_CONFIG_GZ_FILE_NAME), "bad-kind", "bad-kind");
        var badKindMockIPC0 = MockIPC.getPlatformIPC();
        updateDlpConfHandler.handle(badKindMockIPC0, badKindMessage);
        var badKindMessage2 = DlpConfigurationUpdate.getMessage(TestResources.getBytes(TestResources.ANALYZE_CONFIG_GZ_FILE_NAME), null, null);
        var badKindMockIPC1 = MockIPC.getPlatformIPC();
        updateDlpConfHandler.handle(badKindMockIPC1, badKindMessage2);

        terminateResources();

        assertTrue("assert customer configuration unchanged.", DependencyResource.getInstance().getAnalyzer().hasCustomerConfiguration(TestResources.ANALYZE_DEFAULT_CUSTOMER_CONFIG_NAME));
        assertNotNull("assert result not null", badKindMockIPC0.getResult());
        var badKindResult0 = (IPCMessage) badKindMockIPC0.getResult();
        assertNotNull("assert result contains incident id", badKindResult0.getContext().getIncidentId());
        assertEquals("assert message status", IPCMessageStatus.Defaults.BAD_ARGUMENTS, badKindResult0.get_status());
        assertEquals("unsupported data type for update dlp configuration", badKindResult0.getError());
        assertNotNull("assert result not null", badKindMockIPC1.getResult());
        var badKindResult1 = (IPCMessage) badKindMockIPC1.getResult();
        assertEquals("assert message status", IPCMessageStatus.Defaults.BAD_ARGUMENTS, badKindResult1.get_status());

        assertNotNull("assert result contains incident id", badKindResult1.getContext().getIncidentId());
        assertEquals("dlp configuration data is null", badKindResult1.getError());
    }

    @Test
    public void DlpConfigurationUpdateDataToStringTest() throws JsonProcessingException {
        var data = new DlpConfigurationUpdate.Data();
        var config = new DlpConfigurationUpdate.Config<String>();
        config.setCompression(DlpConfigurationUpdate.Compression.NONE);
        config.setEncoding(DlpConfigurationUpdate.Encoding.NONE);
        config.setValue("some-value");
        data.setConfig(config);
        var customerConfig = new DlpCustomerConfiguration();
        customerConfig.setCustomer("some-customer");
        customerConfig.setDetectorSet(new HashSet<>(List.of("x", "y")));
        data.setCustomerConfiguration(customerConfig);
        data.setLimitations(new ApplicationConfiguration.ParseAnalyzeConfiguration());
        var expected = TestResources.getJsonAsMap(TestResources.ANALYZE_CONFIG_UPDATE_DATA_TOSTRING_FILE_NAME);

        var t1 = TestResources.JSON_MAPPER.valueToTree(expected);
        var t2 = TestResources.JSON_MAPPER.readTree(data.toString());

        assertEquals("assert to string result.", t1, t2);
    }

    private void initResources() throws URISyntaxException, IOException {
        var args = new ArrayList<String>();
        args.add("--dlp-conf-path");
        args.add(TestResources.getPath(TestResources.ANALYZE_DEFAULT_CONFIG_FILE_NAME));
        ApplicationConfiguration configuration = new ApplicationConfiguration(args);
        DependencyResource.getInstance().configure(configuration);
    }

    private void terminateResources() {
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 10, TimeUnit.SECONDS);
        DependencyResource.getInstance().terminateResources();
    }

    private AnalyzeRequest getAnalyzeRequest() throws URISyntaxException {
        var analyzeReq = new AnalyzeRequest();
        analyzeReq.setPath(TestResources.getPath(TestResources.ANALYZE_1_MB_DOC_FILE_NAME));
        var customerConfiguration = new DlpCustomerConfiguration();
        customerConfiguration.setCustomer(TestResources.ANALYZE_DEFAULT_CUSTOMER_CONFIG_NAME);
        analyzeReq.getDlpConfig().setDlpCustomerConfiguration(customerConfiguration);
        analyzeReq.getDlpConfig().getDlpCustomerConfiguration().setCustomer(TestResources.ANALYZE_1_MB_DOC_CUSTOMER_CONFIG_NAME);
        return analyzeReq;
    }
}
