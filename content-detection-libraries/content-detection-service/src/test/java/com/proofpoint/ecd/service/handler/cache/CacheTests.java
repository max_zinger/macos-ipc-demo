package com.proofpoint.ecd.service.handler.cache;

import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.analyze.model.AnalyzeResponse;
import com.proofpoint.ecd.commons.runtime.RuntimeConfiguration;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.platform.MockIPC;
import com.proofpoint.ecd.service.DependencyResource;

import com.proofpoint.ecd.service.TestResources;
import com.proofpoint.ecd.service.configuration.ApplicationConfiguration;
import com.proofpoint.ecd.service.handler.AnalyzeContextHandler;
import com.proofpoint.ecd.service.handler.AnalyzeContextHandlerTests;
import com.proofpoint.ecd.service.handler.UpdateDlpConfigurationHandler;
import com.proofpoint.ecd.service.model.AnalyzeMessage;
import com.proofpoint.ecd.service.model.DlpConfigurationUpdate;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class CacheTests {
    @Test
    public void CacheTest() throws URISyntaxException, InterruptedException {
        try {
            DependencyResource.getInstance().terminateResources();
            var conf = new ApplicationConfiguration();
            List<String> args = new ArrayList<>();
            args.add(RuntimeConfiguration.getArgumentByFieldName(conf, "useCaching").arg()[0]);
            args.add("true");
            args.add(RuntimeConfiguration.getArgumentByFieldName(conf, "cacheEntryExpirationSec").arg()[0]);
            args.add("60");
            conf.append(args);
            DependencyResource.getInstance().configure(conf);
        } catch (IOException e) {
            throw new RuntimeException("failed to configure resource.", e);
        }
        var analyze = new AnalyzeContextHandlerTests.AnalyzeMock();
        var analyzeHandler = new AnalyzeContextHandler.AnalyzeMessageHandler(analyze);
        var writer = MockIPC.getPlatformIPC();
        var msg = TestResources.getJson(TestResources.ANALYZE_MSG_FILE_NAME, AnalyzeMessage.class);
        msg.getData().setPath(TestResources.getPath(TestResources.ANALYZE_1_MB_DOC_FILE_NAME));
        analyzeHandler.handle(writer, msg);
        Thread.sleep(250);
        analyzeHandler.handle(writer, msg);
        Thread.sleep(250);
        analyzeHandler.handle(writer, msg);
        Thread.sleep(250);
        var result = (IPCMessage<AnalyzeResponse>) writer.getResult();
        assertEquals("assert context correlation.", msg.getContext().getCorrelationId(), result.getContext().getCorrelationId());
        assertEquals("assert context incident.", msg.getContext().getIncidentId(), result.getContext().getIncidentId());
        assertEquals("assert context transaction.", msg.getContext().getTransactionId(), result.getContext().getTransactionId());
        assertEquals("assert context action name.", msg.getContext().getAction(), result.getContext().getAction());
        assertEquals("assert file path.", msg.getData().getPath(), result.getData().getMetric().getPath());
        assertEquals("assert dlp analyze request count.", 1, analyze.requestCount);
        assertEquals("assert cached dlp results.", 1, DependencyResource.getInstance().getCache().size());

        msg.getData().setPath(TestResources.getPath(TestResources.ANALYZE_DEFAULT_CONFIG_FILE_NAME));
        analyzeHandler.handle(writer, msg);
        Thread.sleep(250);
        analyzeHandler.handle(writer, msg);
        Thread.sleep(250);
        analyzeHandler.handle(writer, msg);
        Thread.sleep(250);
        assertEquals("assert dlp analyze request count.", 2, analyze.requestCount);
        assertEquals("assert cached dlp results.", 2, DependencyResource.getInstance().getCache().size());

        var updateDlpConfHandler = new UpdateDlpConfigurationHandler();
        var dlpUpdateMsg = DlpConfigurationUpdate.getMessage(TestResources.getContent(TestResources.ANALYZE_CONFIG_FILE_NAME), DlpConfigurationUpdate.Encoding.NONE, DlpConfigurationUpdate.Compression.NONE);
        updateDlpConfHandler.handle(dlpUpdateMsg);
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 10, TimeUnit.SECONDS);

        assertEquals("assert cached dlp results after dlp conf update.", 0, DependencyResource.getInstance().getCache().size());
        DependencyResource.getInstance().terminateResources();
    }

    @Test
    public void CacheOffTest() throws URISyntaxException, InterruptedException {
        try {
            DependencyResource.getInstance().terminateResources();
            var conf = new ApplicationConfiguration();
            List<String> args = new ArrayList<>();
            args.add(RuntimeConfiguration.getArgumentByFieldName(conf, "useCaching").arg()[0]);
            args.add("false");
            conf.append(args);
            DependencyResource.getInstance().configure(conf);
        } catch (IOException e) {
            throw new RuntimeException("failed to configure resource.", e);
        }
        var analyze = new AnalyzeContextHandlerTests.AnalyzeMock();
        var analyzeHandler = new AnalyzeContextHandler.AnalyzeMessageHandler(analyze);
        var writer = MockIPC.getPlatformIPC();
        var msg = TestResources.getJson(TestResources.ANALYZE_MSG_FILE_NAME, AnalyzeMessage.class);
        msg.getData().setPath(TestResources.getPath(TestResources.ANALYZE_1_MB_DOC_FILE_NAME));
        analyzeHandler.handle(writer, msg);
        Thread.sleep(250);
        analyzeHandler.handle(writer, msg);
        Thread.sleep(250);
        analyzeHandler.handle(writer, msg);
        Thread.sleep(250);
        var result = (IPCMessage<AnalyzeResponse>) writer.getResult();
        assertEquals("assert context correlation.", msg.getContext().getCorrelationId(), result.getContext().getCorrelationId());
        assertEquals("assert context incident.", msg.getContext().getIncidentId(), result.getContext().getIncidentId());
        assertEquals("assert context transaction.", msg.getContext().getTransactionId(), result.getContext().getTransactionId());
        assertEquals("assert context action name.", msg.getContext().getAction(), result.getContext().getAction());
        assertEquals("assert file path.", msg.getData().getPath(), result.getData().getMetric().getPath());
        assertEquals("assert dlp analyze request count.", 3, analyze.requestCount);
        assertNull("assert cached cache null.", DependencyResource.getInstance().getCache());

        msg.getData().setPath(TestResources.getPath(TestResources.ANALYZE_DEFAULT_CONFIG_FILE_NAME));
        analyzeHandler.handle(writer, msg);
        Thread.sleep(250);
        analyzeHandler.handle(writer, msg);
        Thread.sleep(250);
        analyzeHandler.handle(writer, msg);
        Thread.sleep(250);
        assertEquals("assert dlp analyze request count.", 6, analyze.requestCount);
        assertNull("assert cached cache null.", DependencyResource.getInstance().getCache());

        var updateDlpConfHandler = new UpdateDlpConfigurationHandler();
        var dlpUpdateMsg = DlpConfigurationUpdate.getMessage(TestResources.getContent(TestResources.ANALYZE_CONFIG_FILE_NAME), DlpConfigurationUpdate.Encoding.JSON, DlpConfigurationUpdate.Compression.NONE);
        updateDlpConfHandler.handle(dlpUpdateMsg);
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 10, TimeUnit.SECONDS);
        DependencyResource.getInstance().terminateResources();
        assertNull("assert cached cache null.", DependencyResource.getInstance().getCache());
    }
}
