package com.proofpoint.ecd.service.configuration;

import ch.qos.logback.classic.Level;
import com.proofpoint.ecd.commons.log.RootLoggerConfiguration;
import com.proofpoint.ecd.commons.runtime.RuntimeConfiguration;
import com.proofpoint.ecd.service.TestResources;
import org.junit.AfterClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ApplicationConfigurationTests {
    final static String javaIoTmpDir = System.getProperty("java.io.tmpdir");

    @Test
    public void testArgs() {

        String rwMaxConnections = "10";
        String IOWorkers = "20";
        String logLevel = "debug";
        String dlpConfPath = "path-to-conf";
        String tempDir = System.getProperty("user.dir") + "/java_test_path";

        List<String> args = new ArrayList<>();
        var conf = new ApplicationConfiguration();

        args.add(RuntimeConfiguration.getArgumentByFieldName(conf, "concurrentConnections").arg()[0]);
        args.add(rwMaxConnections);
        args.add(RuntimeConfiguration.getArgumentByFieldName(conf, "concurrentWorkers").arg()[0]);
        args.add(IOWorkers);
        args.add(RuntimeConfiguration.getArgumentByFieldName(conf, "logLevel").arg()[0]);
        args.add(logLevel);
        args.add(RuntimeConfiguration.getArgumentByFieldName(conf, "dlpConfPath").arg()[0]);
        args.add(dlpConfPath);
        args.add(RuntimeConfiguration.getArgumentByFieldName(conf, "tempDir").arg()[0]);
        args.add(tempDir);

        conf.append(args);

        assertEquals(rwMaxConnections, Integer.toString(conf.getConcurrentConnections()));
        assertEquals(IOWorkers, Integer.toString(conf.getConcurrentWorkers()));
        assertEquals(Level.DEBUG.levelStr.toLowerCase(), conf.getLogLevel());
        assertEquals(dlpConfPath, conf.getDlpConfPath());
        assertEquals(System.getProperty("java.io.tmpdir"), conf.getTempDir());
        assertEquals(System.getProperty("jna.tmpdir"), conf.getTempDir() + "/jna");


    }

    @Test
    public void testInvalidArgs() {

        String rwMaxConnections = "1";
        String IOWorkers = "3";
        String logLevel = "bad-arg";
        String dlpConfPath = "";

        List<String> args = new ArrayList<>();
        var conf = new ApplicationConfiguration();

        args.add(RuntimeConfiguration.getArgumentByFieldName(conf, "concurrentConnections").arg()[0]);
        args.add(rwMaxConnections);
        args.add(RuntimeConfiguration.getArgumentByFieldName(conf, "concurrentWorkers").arg()[0]);
        args.add(IOWorkers);
        args.add(RuntimeConfiguration.getArgumentByFieldName(conf, "logLevel").arg()[0]);
        args.add(logLevel);
        args.add(RuntimeConfiguration.getArgumentByFieldName(conf, "dlpConfPath").arg()[0]);
        args.add(dlpConfPath);

        conf.append(args);

        List<String> defaultArgs = new ArrayList<>();
        defaultArgs.add(RuntimeConfiguration.getArgumentByFieldName(conf, "logLevel").arg()[0]);
        defaultArgs.add(RootLoggerConfiguration.getDefaultLogLevel().levelStr.toLowerCase());
        defaultArgs.add(RuntimeConfiguration.getArgumentByFieldName(conf, "concurrentWorkers").arg()[0]);
        defaultArgs.add(String.valueOf(RuntimeConfiguration.getArgumentByFieldName(conf, "concurrentWorkers").minValue()));
        var defaultConf = new ApplicationConfiguration(defaultArgs);
        var t1 = TestResources.JSON_MAPPER.valueToTree(defaultConf);
        var t2 = TestResources.JSON_MAPPER.valueToTree(conf);

        assertEquals("assert application configuration args set to min default.", t1, t2);


    }

    @AfterClass
    public static void after() {
        System.setProperty("java.io.tmpdir", javaIoTmpDir);
    }
}
