package com.proofpoint.ecd.service.handler;

import com.fasterxml.jackson.core.type.TypeReference;
import com.proofpoint.ecd.ContentAnalyze;
import com.proofpoint.ecd.commons.Utils;
import com.proofpoint.ecd.analyze.model.AnalyzeRequest;
import com.proofpoint.ecd.analyze.model.AnalyzeResponse;
import com.proofpoint.ecd.ipc.context.JsonContextHandler;
import com.proofpoint.ecd.ipc.context.model.IPCMessage;
import com.proofpoint.ecd.ipc.context.model.IPCMessageStatus;
import com.proofpoint.ecd.ipc.platform.MockIPC;
import com.proofpoint.ecd.parse.ContentParseResult;
import com.proofpoint.ecd.service.DependencyResource;
import com.proofpoint.ecd.service.TestResources;
import com.proofpoint.ecd.service.model.AnalyzeMessage;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class AnalyzeContextHandlerTests {

    @Test
    public void analyzeInProcessCounterTest() throws URISyntaxException, InterruptedException {
        try {
            DependencyResource.getInstance().configure();
        } catch (IOException e) {
            throw new RuntimeException("failed to configure resource.", e);
        }
        var analyze = new AnalyzeMock();
        analyze.shouldSleep = true;
        var analyzeHandler = new AnalyzeContextHandler.AnalyzeMessageHandler(analyze);
        var writer = MockIPC.getPlatformIPC();
        writer.setIPCOperationsHandler(new JsonContextHandler());
        var msg = TestResources.getJson(TestResources.ANALYZE_MSG_FILE_NAME, AnalyzeMessage.class);
        msg.getData().setPath(TestResources.getPath(TestResources.ANALYZE_1_MB_DOC_FILE_NAME));
        analyzeHandler.handle(writer, msg);
        analyzeHandler.handle(writer, msg);
        Thread.sleep(1500);

        assertTrue("assert context is set to in-process", writer.getIPCOperationsHandler().isInProcess());
        assertEquals("assert count of in-process threads", 2, writer.getIPCOperationsHandler().getInProcess());
        analyze.shouldSleep = false;
        Thread.sleep(1500);
        analyze.throwError = true;
        analyzeHandler.handle(writer, msg);
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 5, TimeUnit.SECONDS);
        assertFalse("assert context is set to in-process", writer.getIPCOperationsHandler().isInProcess());
        assertEquals("assert count of in-process threads", 0, writer.getIPCOperationsHandler().getInProcess());
    }

    @Test
    public void badAnalyzeDlpMessageHandlerTest() throws URISyntaxException {
        try {
            DependencyResource.getInstance().configure();
        } catch (IOException e) {
            throw new RuntimeException("failed to configure resource.", e);
        }
        var analyzer = DependencyResource.getInstance().getAnalyzer();
        var parser = DependencyResource.getInstance().getParser();
        var mockIPC = MockIPC.getPlatformIPC();
        var handler = new AnalyzeContextHandler.AnalyzeMessageHandler(new ContentAnalyze(analyzer, parser));
        var msg = TestResources.getBadAnalyzeMessage();
        msg.getContext().setTransactionId("file-not-found");
        handler.handle(mockIPC, msg);
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 5, TimeUnit.SECONDS);
        var result = (IPCMessage<String>) mockIPC.getResult();
        assertTrue(result.getError().contains("path must be a file"));
        assertNotNull("error should generate an incident id", result.getContext().getIncidentId());
        assertEquals("assert message status", IPCMessageStatus.Defaults.FILE_NOT_FOUND, result.get_status());

    }

    @Test
    public void analyzeDlpMessageHandlerTest() throws URISyntaxException {
        try {
            DependencyResource.getInstance().configure();
        } catch (IOException e) {
            throw new RuntimeException("failed to configure resource.", e);
        }
        var analyzer = DependencyResource.getInstance().getAnalyzer();
        var parser = DependencyResource.getInstance().getParser();
        var mockIPC = MockIPC.getPlatformIPC();
        var handler = new AnalyzeContextHandler.AnalyzeMessageHandler(new ContentAnalyze(analyzer, parser));
        handler.handle(mockIPC, TestResources.getAnalyzeMessage());
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 5, TimeUnit.SECONDS);
        var expectedResultOk = TestResources.getJson(TestResources.ANALYZE_MSG_RESULT_FILE_NAME, new TypeReference<AnalyzeResponseMessageTest>() {
        });

        try {
            expectedResultOk.getData().getMetric().setPath(TestResources.getPath(TestResources.ANALYZE_1_MB_DOC_FILE_NAME));
        } catch (URISyntaxException e) {
            throw new RuntimeException("error", e);
        }
        var t1 = TestResources.removeMetricData(TestResources.JSON_MAPPER.valueToTree(expectedResultOk));
        var t2 = TestResources.removeMetricData(TestResources.JSON_MAPPER.valueToTree(mockIPC.getResult()));
        assertEquals("assert analyze message result.", t1.toString(), t2.toString());

    }

    @Test
    public void analyzeDlpMessageHandlerDefaultCustomerTest() throws Exception {
        try {
            DependencyResource.getInstance().configure();
        } catch (IOException e) {
            throw new RuntimeException("failed to configure resource.", e);
        }
        var analyzer = DependencyResource.getInstance().getAnalyzer();
        var parser = DependencyResource.getInstance().getParser();
        var ipc = MockIPC.getPlatformIPC();
        var contentAnalyze = new ContentAnalyze(analyzer, parser);
        var handler = new AnalyzeContextHandler.AnalyzeMessageHandler(contentAnalyze);
        var msg = TestResources.getAnalyzeMessage();
        msg.getData().getDlpConfig().getDlpCustomerConfiguration().setCustomer(null);
        handler.handle(ipc, msg);
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 30, TimeUnit.SECONDS);
        var expectedResultOk = TestResources.getJson(TestResources.ANALYZE_MSG_RESULT_FILE_NAME, new TypeReference<AnalyzeResponseMessageTest>() {
        });

        try {
            expectedResultOk.getData().getMetric().setPath(TestResources.getPath(TestResources.ANALYZE_1_MB_DOC_FILE_NAME));
        } catch (URISyntaxException e) {
            throw new RuntimeException("error", e);
        }
        var t1 = TestResources.removeMetricData(TestResources.JSON_MAPPER.valueToTree(expectedResultOk));
        var t2 = TestResources.removeMetricData(TestResources.JSON_MAPPER.valueToTree(ipc.getResult()));
        assertEquals("assert analyze message result.", t1.toString(), t2.toString());

    }

    @Test
    public void analyzeDlpMessageHandlerNullExtensiveFilePathsTest() throws URISyntaxException {
        var msg = TestResources.getAnalyzeMessage();
        msg.getContext().setTransactionId("result-analyze-error");
        msg.getData().getPathEx().setSource("");
        msg.getData().setPath(null);
        msg.getData().getPathEx().setTarget(null);

        assertExtensiveFilePathsResult(msg, IPCMessageStatus.Defaults.BAD_ARGUMENTS);
    }

    @Test
    public void analyzeDlpMessageHandlerBadExtensiveFilePathsTest() throws URISyntaxException {
        var msg = TestResources.getAnalyzeMessage();
        msg.getContext().setTransactionId("result-analyze-error");
        msg.getData().getPathEx().setSource("bad-path-1");
        msg.getData().setPath(null);
        msg.getData().getPathEx().setTarget("bad-path-2");

        assertExtensiveFilePathsResult(msg, IPCMessageStatus.Defaults.FILE_NOT_FOUND);

    }

    private void assertExtensiveFilePathsResult(AnalyzeMessage msg, IPCMessageStatus expectedStatus) {
        try {
            DependencyResource.getInstance().configure();
        } catch (IOException e) {
            throw new RuntimeException("failed to configure resource.", e);
        }
        var analyzer = DependencyResource.getInstance().getAnalyzer();
        var parser = DependencyResource.getInstance().getParser();
        var mockIPC = MockIPC.getPlatformIPC();
        var handler = new AnalyzeContextHandler.AnalyzeMessageHandler(new ContentAnalyze(analyzer, parser));
        msg.getContext().setTransactionId("result-analyze-error");
        handler.handle(mockIPC, msg);
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 5, TimeUnit.SECONDS);
        var res = (IPCMessage) mockIPC.getResult();
        assertEquals("assert analyze message result.", expectedStatus, res.get_status());
    }

    @Test
    public void analyzeDlpMessageHandlerExtensiveFilePathsTest() throws URISyntaxException {
        try {
            DependencyResource.getInstance().configure();
        } catch (IOException e) {
            throw new RuntimeException("failed to configure resource.", e);
        }
        var analyzer = DependencyResource.getInstance().getAnalyzer();
        var parser = DependencyResource.getInstance().getParser();
        var mockIPC = MockIPC.getPlatformIPC();
        var handler = new AnalyzeContextHandler.AnalyzeMessageHandler(new ContentAnalyze(analyzer, parser));
        var msg = TestResources.getAnalyzeMessage();
        msg.getData().getPathEx().setSource(msg.getData().getPath());
        msg.getData().setPath(null);
        msg.getData().getPathEx().setTarget("bad-path-2");
        handler.handle(mockIPC, msg);
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 5, TimeUnit.SECONDS);
        var expectedResultOk = TestResources.getJson(TestResources.ANALYZE_MSG_RESULT_FILE_NAME, new TypeReference<AnalyzeResponseMessageTest>() {
        });

        try {
            expectedResultOk.getData().getMetric().setPath(TestResources.getPath(TestResources.ANALYZE_1_MB_DOC_FILE_NAME));
        } catch (URISyntaxException e) {
            throw new RuntimeException("error", e);
        }
        var t1 = TestResources.removeMetricData(TestResources.JSON_MAPPER.valueToTree(expectedResultOk));
        var t2 = TestResources.removeMetricData(TestResources.JSON_MAPPER.valueToTree(mockIPC.getResult()));
        assertEquals("assert analyze message result.", t1.toString(), t2.toString());

    }

    @Test
    public void badAnalyzeMessageHandlerTest() throws URISyntaxException {
        try {
            DependencyResource.getInstance().configure();
        } catch (IOException e) {
            throw new RuntimeException("failed to configure resource.", e);
        }
        var analyze = new AnalyzeMock();
        analyze.throwError = true;
        var analyzeHandler = new AnalyzeContextHandler.AnalyzeMessageHandler(analyze);
        var writer = MockIPC.getPlatformIPC();
        var msg = TestResources.getJson(TestResources.ANALYZE_MSG_FILE_NAME, AnalyzeMessage.class);
        msg.getData().setPath(TestResources.getPath(TestResources.ANALYZE_1_MB_DOC_FILE_NAME));
        analyzeHandler.handle(writer, msg);
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 5, TimeUnit.SECONDS);
        var result = (IPCMessage<AnalyzeResponse>) writer.getResult();

        assertEquals("assert message status", IPCMessageStatus.Defaults.INTERNAL_ERROR, result.get_status());
        assertEquals("assert context correlation.", msg.getContext().getCorrelationId(), result.getContext().getCorrelationId());
        assertNotNull("assert context incident.", result.getContext().getIncidentId());
        assertEquals("assert context transaction.", msg.getContext().getTransactionId(), result.getContext().getTransactionId());
        assertEquals("assert context action name.", msg.getContext().getAction(), result.getContext().getAction());
        assertEquals("expected-test-error", result.getError());

    }

    @Test
    public void analyzeMessageHandlerTest() throws URISyntaxException {
        try {
            DependencyResource.getInstance().configure();
        } catch (IOException e) {
            throw new RuntimeException("failed to configure resource.", e);
        }
        var analyze = new AnalyzeMock();
        var analyzeHandler = new AnalyzeContextHandler.AnalyzeMessageHandler(analyze);
        var writer = MockIPC.getPlatformIPC();
        var msg = TestResources.getJson(TestResources.ANALYZE_MSG_FILE_NAME, AnalyzeMessage.class);
        msg.getData().setPath(TestResources.getPath(TestResources.ANALYZE_1_MB_DOC_FILE_NAME));
        analyzeHandler.handle(writer, msg);
        Utils.awaitTermination(DependencyResource.getInstance().getIoExecutorService(), 5, TimeUnit.SECONDS);
        var result = (IPCMessage<AnalyzeResponse>) writer.getResult();
        assertEquals("assert message status", IPCMessageStatus.Defaults.OK, result.get_status());

        assertEquals("assert context correlation.", msg.getContext().getCorrelationId(), result.getContext().getCorrelationId());
        assertEquals("assert context incident.", msg.getContext().getIncidentId(), result.getContext().getIncidentId());
        assertEquals("assert context transaction.", msg.getContext().getTransactionId(), result.getContext().getTransactionId());
        assertEquals("assert context action name.", msg.getContext().getAction(), result.getContext().getAction());
        assertEquals("assert file path.", msg.getData().getPath(), result.getData().getMetric().getPath());

    }

    public static class AnalyzeResponseMessageTest extends IPCMessage<AnalyzeResponse> {
    }

    public static class AnalyzeMock extends ContentAnalyze {
        public boolean throwError = false;
        public int requestCount;
        public boolean shouldSleep = false;

        public AnalyzeMock() {
            super(null, null);
        }

        @Override
        public AnalyzeResponse analyze(AnalyzeRequest analyzeRequest) {
            while (shouldSleep) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (throwError) {
                throw new RuntimeException("expected-test-error");
            }
            var res = new AnalyzeResponse();
            res.getMetric().setPath(analyzeRequest.getPath());
            var contentResult = new ContentParseResult();
            contentResult.setStatus(ContentParseResult.Status.COMPLETED);
            res.setContentParseResult(contentResult);
            requestCount++;
            return res;
        }
    }

}
