package com.proofpoint.ecd.service.event;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class ApplicationEventPublisherTests {
    @Test
    public void test() {
        var publisher = new ApplicationEventPublisher();
        var listener0 = new TestListener();
        var listener1 = new TestListener();
        publisher.addListener(listener0);
        publisher.addListener(listener1);
        publisher.publish(ApplicationEventPublisher.Event.APP_CONF);
        publisher.publish(ApplicationEventPublisher.Event.APP_SHUTDOWN);
        assertEquals(ApplicationEventPublisher.Event.APP_CONF, listener0.events.get(0));
        assertEquals(ApplicationEventPublisher.Event.APP_SHUTDOWN, listener0.events.get(1));
        assertEquals(2, listener1.events.size());
        publisher.dispose();
        assertEquals(1, listener0.disposed);
        assertEquals(1, listener1.disposed);

    }

    class TestListener implements ApplicationEventPublisher.ApplicationEventListener {

        ArrayList<ApplicationEventPublisher.Event> events = new ArrayList<>();
        int disposed;

        @Override
        public void onPublish(ApplicationEventPublisher.Event e) {
            events.add(e);
        }

        @Override
        public void dispose() {
            disposed++;
        }
    }

}
