#! /bin/bash

declare -a arr=("commons" "content-detection" "ipc-context" "content-detection-service" "content-detection-macos")
for i in "${arr[@]}"; do
   echo "mvn install /$i"
   cd $i
   mvn install
   cd ..
done
