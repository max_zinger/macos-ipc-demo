#include <iostream>
#include <string>
#include <thread>
#include <fstream>
#include <iterator>
#include <vector>

#include <xpc/xpc.h>
#include <xpc/connection.h>
#include <uuid/uuid.h>

#include "../include/json.hpp"

using namespace nlohmann;

struct connection_info
{
  const char *srv;
  const char *msg_key;
  const char *msg_connect;
  xpc_connection_t current_client;
  std::vector<std::string> transactionIds;
  void current_client_set(xpc_connection_t new_client)
  {
    current_client = new_client;
    transactionIds = {};
  }
  void transaction_id_add(std::string id)
  {
    transactionIds.push_back(id);
  }
};

inline void
trim_left(std::string &s)
{
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch)
                                  { return !std::isspace(ch); }));
}
inline void trim_right(std::string &s)
{
  s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch)
                       { return !std::isspace(ch); })
              .base(),
          s.end());
}
inline void trim(std::string &s)
{
  trim_left(s);
  trim_right(s);
}
inline std::vector<std::string> get_args(std::string &s)
{
  trim(s);
  std::string delimiter = " ";
  std::string action;
  size_t pos = 0;
  if ((pos = s.find(delimiter)))
  {
    action = s.substr(0, pos);
    trim(action);
    if (pos != std::string::npos)
    {
      s.erase(0, pos + delimiter.length());
      trim(s);
    }
  }
  std::vector<std::string> args = {action, s};
  return args;
}
inline std::string new_guid_str()
{
  uuid_t uuid;
  uuid_generate(uuid);
  char guid[128];
  uuid_unparse_lower(uuid, guid);
  return std::string(guid);
}
std::vector<char> read_file(std::string path)
{
  std::ifstream input(path, std::ios::binary);

  std::vector<char> bytes(
      (std::istreambuf_iterator<char>(input)),
      (std::istreambuf_iterator<char>()));

  input.close();
  return bytes;
}
json get_message(connection_info *connectionInfo, std::string &line, std::string &correlationId, std::string &transactionId)
{
  std::vector<std::string> args = get_args(line);
  json msg = {
      {"context",
       {{"action", args[0]},
        {"correlationId", correlationId},
        {"transactionId", transactionId}}}};

  if (args[0].compare("analyze") == 0)
  {
    msg["data"]["path"] = args[1];
  }
  else if (args[0].compare("get-status") == 0)
  {
    msg["data"]["transactionIdSet"] = connectionInfo->transactionIds;
  }
  else if (args[0].compare("update-dlp-conf") == 0)
  {
    msg["data"]["config"]["encoding"] = "none";
    msg["data"]["config"]["compression"] = "gzip";
    msg["data"]["config"]["value"] = read_file(args[1]);
  }

  return msg;
}

void start_xpcj_test_server(connection_info *connectionInfo)
{
  xpc_connection_t conn = xpc_connection_create_mach_service(connectionInfo->srv, dispatch_get_main_queue(), XPC_CONNECTION_MACH_SERVICE_LISTENER);
  xpc_connection_set_event_handler(conn, ^(xpc_object_t client) {
    xpc_type_t type = xpc_get_type(client);
    if (XPC_TYPE_ERROR == type)
    {
      std::cout << "an error occured [" << xpc_copy_description(client) << "]\n";
      return;
    }
    xpc_connection_set_event_handler((xpc_connection_t)client, ^(xpc_object_t message) {
      xpc_type_t type = xpc_get_type(message);
      if (XPC_TYPE_ERROR == type)
      {
        const char *err = xpc_dictionary_get_string(message, XPC_ERROR_KEY_DESCRIPTION);
        std::cout << "an error occured [" << err << "]\n";
        const struct _xpc_dictionary_s *derr = (struct _xpc_dictionary_s *)message;

        if (message == XPC_ERROR_CONNECTION_INVALID)
        {
          printf("XPC_ERROR_CONNECTION_INVALID\n");
        }

        if (message == XPC_ERROR_CONNECTION_INTERRUPTED)
        {
          printf("XPC_ERROR_CONNECTION_INTERRUPTED\n");
        }
        else if (message == XPC_ERROR_TERMINATION_IMMINENT)
        {
          printf("XPC_ERROR_CONNECTION_TERMINATION_IMMINENT\n");
        }
        std::cout << "invalidating current client\n";
        connectionInfo->current_client_set(NULL);
      }
      else if (XPC_TYPE_DICTIONARY == type)
      {
        std::string req = xpc_dictionary_get_string(message, connectionInfo->msg_key);
        if (req.empty())
        {
          return;
        }
        //initial connection message
        if (req.compare(connectionInfo->msg_connect) == 0)
        {
          connectionInfo->current_client_set((xpc_connection_t)client);
          std::cout << "client connected \n";
          xpc_object_t ores = xpc_dictionary_create(NULL, NULL, 0);
          xpc_dictionary_set_string(ores, connectionInfo->msg_key, connectionInfo->msg_connect);
          xpc_connection_send_message((xpc_connection_t)client, ores);
          return;
        }
        //assume json message
        try
        {
          json res = json::parse(req);
          std::cout << "message received : \n"
                    << res.dump(4) << "\n"
                    << "transactionId : " << res["context"]["transactionId"] << "\n";
          connectionInfo->transaction_id_add(res["context"]["transactionId"]);
        }
        catch (std::exception &e)
        {
          std::cout << "error " << e.what() << "\n";
        }
      }
    });

    xpc_connection_activate((xpc_connection_t)client);
  });
  xpc_connection_activate(conn);
}
void read_stdin(connection_info *connectionInfo)
{
  std::string correlationId = new_guid_str();

  while (true)
  {
    for (std::string line; std::getline(std::cin, line);)
    {
      try
      {

        if (connectionInfo->current_client)
        {
          std::string transactionId = new_guid_str();
          json req = get_message(connectionInfo, line, correlationId, transactionId);

          xpc_object_t msg = xpc_dictionary_create(NULL, NULL, 0);
          xpc_dictionary_set_string(msg, connectionInfo->msg_key, req.dump().c_str());
          xpc_connection_send_message(connectionInfo->current_client, msg);

          std::cout << "message sent : \n"
                    << req.dump(4) << "\n";
        }
      }
      catch (...)
      {
      }
    }
  }

  std::cout << "read line exit\n";
}
int main()
{
  struct connection_info connectionInfo;
  connectionInfo.srv = "com.proofpoint.ecd.srv";
  connectionInfo.msg_connect = "connect";
  connectionInfo.msg_key = "json";
  //async
  start_xpcj_test_server(&connectionInfo);
  std::thread t(read_stdin, &connectionInfo);
  dispatch_main();
  return 0;
}