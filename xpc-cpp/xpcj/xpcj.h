
/*
 * jna c interface for xpc service api    
*/
//#define FMT_HEADER_ONLY

#include <iostream>
#include <stdio.h>
#include <xpc/xpc.h>

#define GET_NAME(var) (#var)

extern "C"
{
    //connect/create and obtain the xpc connection object
    xpc_connection_t connection_create_mach_service(const char *srvname, u_int64_t flags);
    //set client callback message event handlers
    int set_peer_event_handler(xpc_connection_t *conn, void (*msgCallback)(xpc_object_t, int));
    //set server callback message event handlers
    int set_listener_event_handler(xpc_connection_t *conn, void (*msgCallback)(xpc_connection_t, xpc_object_t, int));
    //create an empty xpc dictionary object
    xpc_object_t dictionary_create();
    //set a dictionray string value entry
    int dictionary_set_string(xpc_object_t *msg, const char *key, const char *str);
    //get a dictionray string value entry
    const char *dictionary_get_string(xpc_object_t *msg, const char *key);
    //send the xpc object
    int connection_send_message(xpc_connection_t *conn, xpc_object_t *msg);
    //activate the connection - on init
    int connection_activate(xpc_connection_t *conn);
    //suspend the xpc connection
    int connection_suspend(xpc_connection_t *conn);
    //cancel the xpc connection
    int connection_cancel(xpc_connection_t *conn);
    //resume
    int connection_resume(xpc_connection_t *conn);
    //get message description
    const char *copy_description(xpc_object_t *msg);
    //iterate dictionary keys
    int dictionary_apply(xpc_object_t *msg, void (*callback)(const char *, xpc_object_t value));
    //get name of type
    const char *type_get_name(xpc_object_t *value);

    //set a dictionray raw data value entry
    // int set_msg_bytes(xpc_object_t *msg, const char *key, const uint8_t *bytes, size_t size);
    // //get a dictionray raw data value entry
    // const uint8_t *get_msg_bytes(xpc_object_t *msg, const char *key);
}
//handle message event
void handle_msg(xpc_connection_t peer, xpc_object_t dmsg, void (*msgCallback)(xpc_object_t, int), void (*msgPeerCallback)(xpc_connection_t, xpc_object_t, int));
//handle error message event
//void handle_error(xpc_object_t dmsg, void (*msgCallback)(xpc_object_t), void (*errCallback)(const char *, const int c));
